NAME = registry.dev.bcgdv.xyz/myadvisor/web-app
BRANCH = $(shell git symbolic-ref HEAD | sed -e 's,.*/\(.*\),\1,')
BUILD = 1
VERSION = 1.0.$(BUILD)
TAG = $(VERSION)-$(BRANCH)

.PHONY: all bump_version tag_version prepare build tag_latest release

all: build

tag_version:
  #$(shell sed -i '' -e '6s/.*/  "$(TAG)"/' app/scripts/services.coffee )

prepare: tag_version
	gulp build

build: prepare
	docker build -t $(NAME):$(TAG) --rm .

tag_latest:
	docker tag $(NAME):$(TAG) $(NAME):latest-$(BRANCH)

release: tag_latest
	@if ! docker images $(NAME) | awk '{ print $$2 }' | grep -q -F $(TAG); then echo "$(NAME) version $(TAG) is not yet built. Please run 'make build'"; false; fi
	docker push $(NAME)