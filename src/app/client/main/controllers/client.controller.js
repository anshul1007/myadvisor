export class ClientController {
  constructor(stringConstants, $scope, $state, helpers) {
    'ngInject';

    this.stringConstants = stringConstants;
    this.$state = $state;
    this.helpers = helpers;
    $scope.getAutomationId = (elmName, elmType, elmIndex) => this.getAutomationId(elmName, elmType, elmIndex);
  }

  getAutomationId(elmName, elmType, elmIndex = null) {
    const elementObject = {
      elementState: this.$state.current.name,
      elementName: elmName,
      elementType: elmType,
      elementIndex: elmIndex
    };
    return this.helpers.generateAutomationID(elementObject);
  }
}
