describe('Describing AdvisorController and its configuration functions', function() {
  let scope;
  let controller;
  let rootScope;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, stringConstants) => {
    scope = $rootScope.$new();
    rootScope = $rootScope;
    controller = $controller('ClientController', {
      $scope: scope
    });
  }));

});
