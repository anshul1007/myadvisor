export class OnboardingService {
  constructor(Restangular) {
    'ngInject';
    this.onBoardingRest = Restangular.one('onboarding');
  }

  getContractSignStatus() {
    return this.onBoardingRest.all('contract').get('status');
  }

  setContractSignStatus(data) {
    return this.onBoardingRest.all('contract').post(data);
  }

  getClientEmailAddress() {
    return "example@rbc.com";
  }
  setOnBoardingStatus(data) {
    return this.onBoardingRest.all('contract').post(data);
  }
  getAdvisorDetails() {
    return {
      firstName: 'Johnathan',
      lastName: 'John',
      photoUrl: '',
      primaryHours: 'Mornning And Afternoon',
      bio: 'Aenean sed efficitur nulla, eget commodo ante. Vestibulum mollis vitae orci quis tempus. Sed sagittis justo ut urna gravida efficitur in non nibh. Aenean ac posuere risus, quis facilisis mauris. Mauris viverra congue ante a viverra. Sed eleifend justo quis consequat malesuada. Suspendisse vel massa elementum est scelerisque auctor. Donec sed finibus ipsum. Phasellus dignissim felis ut vehicula pulvinar. Duis dictum erat id varius fermentum. Duis finibus magna ut nunc volutpat fringilla.'
    };
  }
  getAnotherAdvisorDetails() {
    return [{
      firstName: 'John',
      lastName: 'Greg',
      photoUrl: '',
      primaryHours: 'Mornning And Afternoon',
      bio: 'Aenean sed efficitur nulla, eget commodo ante. Vestibulum mollis vitae orci quis tempus. Sed sagittis justo ut urna gravida efficitur in non nibh. Aenean ac posuere risus, quis facilisis mauris. Mauris viverra congue ante a viverra. Sed eleifend justo quis consequat malesuada. Suspendisse vel massa elementum est scelerisque auctor. Donec sed finibus ipsum. Phasellus dignissim felis ut vehicula pulvinar. Duis dictum erat id varius fermentum. Duis finibus magna ut nunc volutpat fringilla.'
     },
    {
      firstName: 'Johnathan',
      lastName: 'John',
      photoUrl: '',
      primaryHours: 'Mornning And Afternoon',
      bio: 'Aenean sed efficitur nulla, eget commodo ante. Vestibulum mollis vitae orci quis tempus. Sed sagittis justo ut urna gravida efficitur in non nibh. Aenean ac posuere risus, quis facilisis mauris. Mauris viverra congue ante a viverra. Sed eleifend justo quis consequat malesuada. Suspendisse vel massa elementum est scelerisque auctor. Donec sed finibus ipsum. Phasellus dignissim felis ut vehicula pulvinar. Duis dictum erat id varius fermentum. Duis finibus magna ut nunc volutpat fringilla.'
    }]; 
  }
}
