 class ModalInstanceCtrl {
  constructor($uibModalInstance, $scope, config) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
    $scope.config = config;
    $scope.onPlayerReady = function(API) {
      this.API = API;
      this.API.setVolume(100);
    };
  }
  ok() {
    this.$uibModalInstance.close();
  }
  cancel() {
    this.$uibModalInstance.dismiss();
  }

}

 export class OnBoardingController {
  constructor($scope, $uibModal, $state, $sce, OnboardingService, moment, $timeout, $anchorScroll, $location) {
    'ngInject';
    this.$state = $state;
    this.$anchorScroll = $anchorScroll;
    this.$location = $location;
      //Client Contract
    this.onboardingService = OnboardingService;
    this.isContractSigned = false;
    this.user = {};
    this.$scope = $scope;
    this.$uibModal = $uibModal;
    this.user.emailTrackAgree = false;
    this.user.legalAgreement = false;
    this.showMore = false;
    this.moment = moment;
    this.getClientEmailAddress();
    this.setOnBoardingStatus();
    //video module
    this.API = null;
    this.$sce = $sce;
    this.videoConfig();
    // Match Advisor
    this.isContractSigned = false;
    this.$timeout = $timeout;
    this.showSubmittig = true;
    this.sendMsgScreen = false;
    this.advisorDetails = this.onboardingService.getAdvisorDetails();
    this.advisors = this.onboardingService.getAnotherAdvisorDetails();
    this.$timeout(()=> {
      this.showSubmittig = false;
    }, 3000);
    // Retirement Goal
    this.employmentStatus = ['Employed', 'Unemployed', 'Student', 'Retired'];
    this.retirementAge =  _.range(50,81);
    this.annualIncome = 0;
    this.clothingAndPersonalCare = 0;
    this.entertainment = 0; 
    this.foodAndGroceries = 0;
    this.housing = 0;
    this.transportation = 0;
    this.utilities = 0;
    this.other = 0;
    this.monthlyTotal = 0;
    this.$scope.$watch(() => {
        return (this.clothingAndPersonalCare + this.entertainment + this.foodAndGroceries + this.housing  +this.transportation + this.utilities + this.other);
    }, () => {
        this.monthlyTotal = parseFloat(this.clothingAndPersonalCare) + parseFloat(this.entertainment) + parseFloat(this.foodAndGroceries) 
            + parseFloat(this.housing)+ parseFloat(this.transportation) + parseFloat(this.utilities) + parseFloat(this.other);
    });
  }
  //Video module for onborading
  videoConfig() {
    this.config = {
      sources: [
        { src: this.$sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogular.mp4'), type: 'video/mp4' },
        { src: this.$sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogular.webm'), type: 'video/webm' },
        { src: this.$sce.trustAsResourceUrl('http://static.videogular.com/assets/videos/videogular.ogg'), type: 'video/ogg' }
      ],
      theme: {
        url: 'http://www.videogular.com/styles/themes/default/latest/videogular.css'
      }
    };
  }
  onComplete() {
    this.API.play();
  }
  onPlayerReady(API) {
    this.API = API;
    this.API.setVolume(0);
  }
  optimizeWeatlth() {
    this.$state.go('client.onboarding.contract');
  }
  playVideo() {
    const resObj = {
      config: function () {
        return {
          preload: 'none',
          sources: [
            { src: 'http://static.videogular.com/assets/videos/videogular.mp4', type: 'video/mp4' },
            { src: 'http://static.videogular.com/assets/videos/videogular.webm', type: 'video/webm' },
            { src: 'http://static.videogular.com/assets/videos/videogular.ogg', type: 'video/ogg' }
          ],
          tracks: [
            {
              src: 'pale-blue-dot.vtt',
              kind: 'subtitles',
              srclang: 'en',
              label: 'English',
              default: ''
            }],
          theme: {
            url: 'http://www.videogular.com/styles/themes/default/latest/videogular.css'
          }
        };
      }
    };
    this.dialogOpen('_video', 'videoFullScreen', resObj);
  }
  //Contract Onbording module
  getClientEmailAddress() {
    this.user.email = this.onboardingService.getClientEmailAddress
();
  }

  setOnBoardingStatus() {
    const statusObj = {
      OnBoardingStatus: 'incomplete'
    };
    this.onboardingService.setOnBoardingStatus(statusObj).then(() => {
    });
  }

  toggleShowMore() {
    this.showMore = !this.showMore;
  }
  getClientEmailAddress() {
    this.user.email = this.onboardingService.getClientEmailAddress();
  }

  submitLegalAgreement() {
    if (this.user.emailTrackAgree && this.user.legalAgreement)
    {
      const signData ={
        accountStatus: 'Activated',
        activationDate: this.moment(),
        emailAgreementAccepted: this.user.emailTrackAgree,
        clickWrapAccepted: this.user.legalAgreement
      };
      this.onboardingService.setContractSignStatus(signData).then((result) => {
        if (result) {
          this.isContractSigned = true;
        }
      });
      this.$state.go('client.onboarding.match-advisor');
    }
    else
    {
      const resObj = {
        config: function() {
          return '';
        }
      };
      this.dialogOpen('_contract-alert', '', resObj);
    }
  }

  noThanksClick() {
    const resObj = {
      config: function() {
        return '';
      }
    };
    this.dialogOpen('_accept-alert', '', resObj);
  }

  setOnBoardingStatus() {
    const statusObj = {
      OnBoardingStatus: 'incomplete'
    };
    this.onboardingService.setOnBoardingStatus(statusObj).then(() => {
    });
  }

  checkContractSignStatus() {
    this.onboardingService.getContractSignStatus().then((result) => {
      if (result) {
        this.isContractSigned = true;
      }
    });
    return this.isContractSigned;
  }

  //Match advisor onboarding module
  chooseAnotherAdvisor() {
    const resObj = {
      config: function() {
        return '';
      }
    };
    this.dialogOpen('_choose-advisor', 'advisorFullScreen', resObj);
  }

  confirmAdvisor() {
    this.showSubmittig = false;
    this.$state.go('client.onboarding.select-advisor');
  }

  setupRetirementGoal(){
      this.$state.go('client.onboarding.setup-retirement-goal');
  }

  returnToAdvisorSelection() {
    this.$state.go('client.onboarding.match-advisor');
  }
  gotoAnchor (newHash){
      if (this.$location.hash() !== newHash) {
          // set the $location.hash to `newHash` and
          // $anchorScroll will automatically scroll to it
          this.$location.hash(newHash);
      } else {
          // call $anchorScroll() explicitly,
          // since $location.hash hasn't changed
          this.$anchorScroll();
      }
  }
  //to open dialog box on client onboarding
  dialogOpen(Url, classToAdd, resobj) {
    this.$uibModal.open({
      animation: true,
      templateUrl: 'app/client/onboarding/views/'+Url+'.html',
      scope: this.$scope,
      controller: ModalInstanceCtrl,
      controllerAs: 'mCtrl',
      bindToController: true,
      windowClass: classToAdd,
      backdrop: 'static',
      keyboard: false,
      resolve: resobj
    });
  }

}


