/**
 * @todo Complete the test
 * This example is not perfect.
 * Test should check if MomentJS have been called
 */
describe('controller value propagation', function() {
  let OnboardingService;
  let deferred;
  let vm;
  let scope;
  let httpBackend;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, _OnboardingService_, $q, $httpBackend) => {
    deferred = $q.defer();
    scope = $rootScope.$new();
    OnboardingService = _OnboardingService_;
    httpBackend = $httpBackend;
    httpBackend.whenGET('/api/v1/onboarding/contract/status').respond({});

    vm = $controller('ValuePropController', {
      $scope: scope
    });

  }));
  /*

  it('should be false checkContractSignStatus', () => {
    deferred.resolve(false);
    spyOn(OnboardingService, 'getContractSignStatus').and.returnValue(deferred.promise);

    vm.checkContractSignStatus();
    scope.$digest();
  });

  it('should be true checkContractSignStatus', () => {
    deferred.resolve(true);
    spyOn(OnboardingService, 'getContractSignStatus').and.returnValue(deferred.promise);

    vm.checkContractSignStatus();
    scope.$digest();
  });
  */
});
