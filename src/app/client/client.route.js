export function routerConfig(stringConstants, $stateProvider, $urlRouterProvider, $locationProvider) {
  'ngInject';
  $stateProvider
    .state('client', {
      title: stringConstants.client.pageTitle,
      description: stringConstants.client.description,
      keywords: stringConstants.client.keywords,
      url: '/client',
      templateUrl: 'app/client/main/views/index.html',
      controller: 'ClientController',
      controllerAs: 'ClientCtrl'
    })
    .state('client.dashboard', {
      title: stringConstants.dashboard.pageTitle,
      description: stringConstants.dashboard.description,
      keywords: stringConstants.dashboard.keywords,
      url: '/dashboard',
      templateUrl: 'app/client/dashboard/views/dashboard.html',
      controller: 'DashboardController',
      controllerAs: 'DashboardCtrl'
    })
    .state('client.appointment', {
      title: stringConstants.appointment.pageTitle,
      description: stringConstants.appointment.description,
      keywords: stringConstants.appointment.keywords,
      url: '/appointment',
      templateUrl: 'app/client/appointment/views/_appointment.html'
    })
    .state('client.appointment.upcoming-appointments', {
      title: stringConstants.appointment.confirmappointment.pageTitle,
      description: stringConstants.appointment.confirmappointment.description,
      keywords: stringConstants.appointment.confirmappointment.keywords,
      url: '/upcoming-appointments',
      templateUrl: 'app/client/appointment/views/_appointments.html',
      controller: 'AppointmentsController',
      controllerAs: 'appointmentsCtrl'
    })
    .state('client.appointment.schedule', {
      title: stringConstants.appointment.pageTitle,
      description: stringConstants.appointment.description,
      keywords: stringConstants.appointment.keywords,
      url: '/schedule',
      templateUrl: 'app/client/appointment/views/_schedule.html'
    })
    .state('client.appointment.schedule.session-type', {
      title: stringConstants.appointment.choosetopic.pageTitle,
      description: stringConstants.appointment.choosetopic.description,
      keywords: stringConstants.appointment.choosetopic.keywords,
      url: '/session-type',
      templateUrl: 'app/client/appointment/views/_session-type.html',
      controller: 'SessionTypeController',
      controllerAs: 'sessionTypeCtrl'
    })
    .state('client.appointment.schedule.choose-topic', {
      title: stringConstants.appointment.choosetopic.pageTitle,
      description: stringConstants.appointment.choosetopic.description,
      keywords: stringConstants.appointment.choosetopic.keywords,
      url: '/choose-topic',
      templateUrl: 'app/client/appointment/views/_choose-topic.html',
      controller: 'ChooseTopicController',
      controllerAs: 'chooseTopicCtrl'
    })
    .state('client.appointment.schedule.available-time', {
      title: stringConstants.appointment.availabletime.pageTitle,
      description: stringConstants.appointment.availabletime.description,
      keywords: stringConstants.appointment.availabletime.keywords,
      url: '/available-time',
      templateUrl: 'app/client/appointment/views/_available-time.html',
      controller: 'AvailableTimeController',
      controllerAs: 'availableTimeCtrl'
    })
    .state('client.appointment.schedule.choose-date', {
      title: stringConstants.appointment.choosedate.pageTitle,
      description: stringConstants.appointment.choosedate.description,
      keywords: stringConstants.appointment.choosedate.keywords,
      url: '/choose-date',
      templateUrl: 'app/client/appointment/views/_choose-date.html',
      controller: 'ChooseDateController',
      controllerAs: 'chooseDateCtrl'
    })
    .state('client.appointment.schedule.available-times-by-date', {
      title: stringConstants.appointment.availabletimesbydate.pageTitle,
      description: stringConstants.appointment.availabletimesbydate.description,
      keywords: stringConstants.appointment.availabletimesbydate.keywords,
      url: '/available-times-by-date',
      templateUrl: 'app/client/appointment/views/_available-times-by-date.html',
      controller: 'AvailableTimesByDateController',
      controllerAs: 'availableTimesByDateCtrl'
    })
    .state('client.appointment.schedule.confirm-phone', {
      title: stringConstants.appointment.confirmphone.pageTitle,
      description: stringConstants.appointment.confirmphone.description,
      keywords: stringConstants.appointment.confirmphone.keywords,
      url: '/confirm-phone',
      templateUrl: 'app/client/appointment/views/_confirm-phone.html',
      controller: 'ConfirmPhoneController',
      controllerAs: 'confirmPhoneCtrl'
    })
    .state('client.appointment.schedule.confirm-appointment', {
      title: stringConstants.appointment.confirmappointment.pageTitle,
      description: stringConstants.appointment.confirmappointment.description,
      keywords: stringConstants.appointment.confirmappointment.keywords,
      url: '/confirm-appointment',
      templateUrl: 'app/client/appointment/views/_confirm-appointment.html',
      controller: 'ConfirmAppointmentController',
      controllerAs: 'confirmAppointmentCtrl'
    })
    .state('client.onboarding', {
      title: stringConstants.onboarding.pageTitle,
      description: stringConstants.onboarding.description,
      keywords: stringConstants.onboarding.keywords,
      url: '/onboarding',
      templateUrl: 'app/client/onboarding/views/onboarding.html'
    })
    .state('client.onboarding.valueprop', {
      title: stringConstants.onboarding.pageTitle,
      description: stringConstants.onboarding.description,
      keywords: stringConstants.onboarding.keywords,
      url: '/valueprop',
      templateUrl: 'app/client/onboarding/views/valueprop.html',
      controller: 'OnBoardingController',
      controllerAs: 'onBoardCtrl'
    })
    .state('client.onboarding.contract', {
      title: stringConstants.onboarding.contract.pageTitle,
      description: stringConstants.onboarding.contract.description,
      keywords: stringConstants.onboarding.contract.keywords,
      url: '/contract',
      templateUrl: 'app/client/onboarding/views/contract.html',
      controller: 'OnBoardingController',
      controllerAs: 'onBoardCtrl'
    })
    .state('client.onboarding.match-advisor', {
      title: stringConstants.onboarding.contract.pageTitle,
      description: stringConstants.onboarding.contract.description,
      keywords: stringConstants.onboarding.contract.keywords,
      url: '/match-advisor',
      templateUrl: 'app/client/onboarding/views/matchadvisor.html',
      controller: 'OnBoardingController',
      controllerAs: 'onBoardCtrl'
    })
    .state('client.onboarding.select-advisor', {
      title: stringConstants.onboarding.contract.pageTitle,
      description: stringConstants.onboarding.contract.description,
      keywords: stringConstants.onboarding.contract.keywords,
      url: '/select-advisor',
      templateUrl: 'app/client/onboarding/views/select-advisor.html',
      controller: 'OnBoardingController',
      controllerAs: 'onBoardCtrl'
    })
    .state('client.onboarding.setup-retirement-goal', {
        title: stringConstants.onboarding.contract.pageTitle,
        description: stringConstants.onboarding.contract.description,
        keywords: stringConstants.onboarding.contract.keywords,
        url: '/setup-retirement-goal',
        templateUrl: 'app/client/onboarding/views/setup-retirement-goal.html',
        controller: 'OnBoardingController',
        controllerAs: 'onBoardCtrl'
    })
    .state('client.onboarding.investment-profile', {
        title: stringConstants.onboarding.contract.pageTitle,
        description: stringConstants.onboarding.contract.description,
        keywords: stringConstants.onboarding.contract.keywords,
        url: '/investment-profile',
        templateUrl: 'app/client/onboarding/views/investment-profile.html',
        controller: 'OnBoardingController',
        controllerAs: 'onBoardCtrl'
    })
    .state('pagenotfound', {
      title: stringConstants.commonerrors.pageNotFound.pageTitle,
      description: stringConstants.commonerrors.pageNotFound.description,
      keywords: stringConstants.commonerrors.pageNotFound.keywords,
      url: '/pagenotfound',
      templateUrl: '/404.html'
    })
    .state('add-goal', {
      url: '/add-goal',
      onEnter: ['$uibModal', '$state', function($uibModal, $state) {
        $uibModal.open({
          templateUrl: 'app/client/goals/views/_goal-types.html',
          controller: 'GoalTypesController',
          controllerAs: 'goalTypesCtrl',
          backdrop: false,
          windowTopClass: 'full-screen'
        }).result.finally(function() {
          $state.go('client.dashboard');
        });
      }]
    })

    .state('add-goal.major-expense-goal', {
      url: '/major-expense-goal',
      parent: 'add-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_major-expense-goal.html',
          controller: 'ExpenseTypeController',
          controllerAs: 'expenseTypeCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.expense-investor-profile', {
      url: '/expense-investor-profile',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_expense-investor-profile.html',
          controller: 'InvestorProfileController',
          controllerAs: 'investorProfileCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.goal-confirmation', {
      url: '/goal-confirmation',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_goal-confirmation.html',
          controller: 'GoalConfirmationController',
          controllerAs: 'goalConfirmationCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.risk-tolerance', {
      url: '/risk-tolerance',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_risk-tolerance.html',
          controller: 'RiskToleranceController',
          controllerAs: 'riskToleranceCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.investment-objective', {
      url: '/investment-objective',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_investment-objective.html',
          controller: 'InvestorObjectiveController',
          controllerAs: 'investmentObjectiveCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.select-account', {
      url: '/select-account',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_select-account.html'
        }
      }
    })
    .state('add-goal.major-expense-goal.how-important', {
      url: '/how-important',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_how-important.html',
          controller: 'GoalImportanceController',
          controllerAs: 'goalImportanceCtrl'
        }
      }
    })
    .state('add-goal.major-expense-goal.goal-information', {
      url: '/goal-information',
      parent: 'add-goal.major-expense-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_goal-information.html',
          controller: 'GoalInformationController',
          controllerAs: 'goalInformationCtrl'
        }
      }
    })
    .state('add-goal.education-goal', {
      url: '/education-goal',
      parent: 'add-goal',
      views: {
        'modal@': {
          templateUrl: 'app/client/goals/views/_education-goal.html'
        }
      }
    });

  $urlRouterProvider.otherwise('/client/dashboard');
  $locationProvider.html5Mode(true);
}
