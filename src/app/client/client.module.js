/* global moment:false */

import { config } from './client.config';
import { routerConfig } from './client.route';
import { runBlock } from './client.run';
import { DashboardController } from './dashboard/controllers/dashboard.controller';
import { DashboardService } from './dashboard/services/dashboard.service';
import { ClientController } from './main/controllers/client.controller';
import { SessionTypeController } from './appointment/controllers/sessiontype.controller';
import { ChooseTopicController } from './appointment/controllers/choosetopic.controller';
import { AvailableTimeController } from './appointment/controllers/availabletime.controller';
import { GoalTypesController } from './goals/controllers/goaltypes.controller';
import { ExpenseTypeController } from './goals/controllers/expensetype.controller';
import { InvestorProfileController } from './goals/controllers/investorprofile.controller';
import { RiskToleranceController } from './goals/controllers/risktolerance.controller';
import { GoalImportanceController } from './goals/controllers/goalImportance.controller';
import { GoalInformationController } from './goals/controllers/goalinformation.controller';
import { GoalConfirmationController } from './goals/controllers/goalconfirmation.controller';
import { InvestorObjectiveController } from './goals/controllers/investorobjective.controller';
import WhenScrollEndsDirective from './appointment/directives/whenscrollends.directive';
import CalendarDirective from './appointment/directives/calendar.directive';
import { ChooseDateController } from './appointment/controllers/choosedate.controller';
import { AvailableTimesByDateController } from './appointment/controllers/availabletimesbydate.controller';
import { ConfirmPhoneController } from './appointment/controllers/confirmphone.controller';
import { ConfirmAppointmentController } from './appointment/controllers/confirmappointment.controller';
import { AppointmentsController } from './appointment/controllers/appointments.controller';
import { AppointmentService } from './shared/services/appointment.service';
import { CashFlowController } from './dashboard/controllers/cashflow.controller';
import { NetWorthController } from './dashboard/controllers/networth.controller';
import { SandBoxController } from './dashboard/controllers/sandbox.controller';
import { GoalsController } from './dashboard/controllers/goals.controller';
import { EditGoalController } from './dashboard/controllers/editgoal.controller';
import { ClientConferenceController } from './dashboard/controllers/conference.controller';
import { GoalsService } from './dashboard/services/goals.service';
import { OnBoardingController } from './onboarding/controllers/onBoarding.controller';
import { OnboardingService } from './onboarding/services/onboarding.service';
angular.module('myClient', ['toastr', 'ui.router', 'constants', 'ngIdle'])
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('ClientController', ClientController)
  .controller('SessionTypeController', SessionTypeController)
  .controller('ChooseTopicController', ChooseTopicController)
  .controller('AvailableTimeController', AvailableTimeController)
  .controller('GoalTypesController', GoalTypesController)
  .controller('ExpenseTypeController', ExpenseTypeController)
  .controller('InvestorProfileController', InvestorProfileController)
  .controller('RiskToleranceController', RiskToleranceController)
  .controller('GoalImportanceController', GoalImportanceController)
  .controller('GoalInformationController', GoalInformationController)
  .controller('GoalConfirmationController', GoalConfirmationController)
  .controller('InvestorObjectiveController', InvestorObjectiveController)
  .directive('calendar', () => new CalendarDirective())
  .directive('whenScrollEnds', () => new WhenScrollEndsDirective())
  .controller('ChooseDateController', ChooseDateController)
  .controller('AvailableTimesByDateController', AvailableTimesByDateController)
  .controller('ConfirmPhoneController', ConfirmPhoneController)
  .controller('ConfirmAppointmentController', ConfirmAppointmentController)
  .controller('OnBoardingController', OnBoardingController)
  .controller('DashboardController', DashboardController)
  .controller('CashFlowController', CashFlowController)
  .controller('NetWorthController', NetWorthController)
  .controller('SandBoxController', SandBoxController)
  .controller('GoalsController', GoalsController)
  .controller('EditGoalController', EditGoalController)
  .controller('ClientConferenceController', ClientConferenceController)
  .service('GoalsService', GoalsService)
  .controller('AppointmentsController', AppointmentsController)
  .service('OnboardingService', OnboardingService)
  .service('DashboardService', DashboardService)
  .service('AppointmentService', AppointmentService);
