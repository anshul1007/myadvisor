export default class WhenScrollEndsDirective {
  constructor() {
    this.restrict = 'A';
  }

  link(scope, element, attrs) {
    const visibleHeight = element.height();

    element.scroll(function() {
      let scrollableHeight = element.prop('scrollHeight');
      let hiddenContentHeight = scrollableHeight - visibleHeight;

      if (hiddenContentHeight - element.scrollTop() <= attrs.threshold) {
        scope.$apply(attrs.whenScrollEnds);
      }
    });
  }
}
