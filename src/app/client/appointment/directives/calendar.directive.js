/*
Source: https://www.codementor.io/angularjs/tutorial/angularjs-calendar-directives-less-cess-moment-font-awesome
*/

class CalendarDirectiveController {
  constructor(moment, _) {
    'ngInject';
    this.moment = moment;
    this._ = _;
  }
}

export default class CalendarDirective {
  constructor() {
    this.restrict = 'E';
    this.templateUrl = 'app/client/appointment/views/_calendar.html';
    this.scope = {
      month: '=',
      availableSlots: '=',
      selectedDate: '=',
      onDateSelect: '&',
      automationId: '&',
      dayPart: '='
    };
    this.controller = CalendarDirectiveController;
    this.controllerAs = 'calendarCtrl';
  }

  link(scope, element, attrs, calendarCtrl) {
    let start;

    function removeTime(date) {
      return date.hour(0).minute(0).second(0).millisecond(0);
    }

    function buildWeek(date, month) {
      let days = [];
      let i = 0;
      let newdate = date;
      let disableDate = true;
      let numberOfSlots = 0;
      for (i = 0; i < 7; i++) {
        for (let counter = 0; counter < calendarCtrl.slots.length; counter++) {
          let removeTimeTempDate = removeTime(calendarCtrl.moment(calendarCtrl.slots[counter].slotDate));
          if (removeTimeTempDate.isSame(newdate)) {
            numberOfSlots = (calendarCtrl.slots[counter].numberOfSlots > 4) ? 4 : calendarCtrl.slots[counter].numberOfSlots;
            if (numberOfSlots) {
              disableDate = false;
            }
          }
        }
        days.push({
          numberOfSlots: numberOfSlots,
          name: newdate.format('dd').substring(0, 1),
          number: newdate.date(),
          isSelected: newdate.isSame(scope.selectedDate, 'day'),
          isDisable: disableDate || newdate.month() !== month.month() || newdate.day() === 0 || newdate.day() === 6 || newdate.isBefore(calendarCtrl.moment().subtract(1, 'day')),
          isDifferentMonth: newdate.month() !== month.month(),
          date: newdate
        });
        newdate = newdate.clone();
        newdate.add(1, 'd');
        numberOfSlots = 0;
        disableDate = true;
      }
      return days;
    }

    function buildMonth(startMonth, month) {
      let done = false;
      let date = startMonth.clone();
      let monthIndex = date.month();
      let count = 0;

      scope.weeks = [];
      while (!done) {
        scope.weeks.push({ days: buildWeek(date.clone(), month) });
        date.add(1, 'w');
        done = count++ > 2 && monthIndex !== date.month();
        monthIndex = date.month();
      }
    }

    function buildCalendar() {
      scope.month = removeTime(scope.month || calendarCtrl.moment());
      scope.month = scope.month.clone();
      start = scope.month.clone();
      start.date(1);
      removeTime(start.day(0));
      buildMonth(start, scope.month);

      scope.select = function(day) {
        if (!day.isDisable) {
          scope.month = day.date;
          scope.onDateSelect({ selectedDate: scope.month });
        }
      };
    }
    //for reference https://gist.github.com/CMCDragonkai/6282750
    scope.$watch('availableSlots', function(value) {
      if (value) {
        calendarCtrl.slots = value;
        buildCalendar();
      }
    });
    scope.getTimes = function(n) {
      return new Array(n);
    }
  }
}
