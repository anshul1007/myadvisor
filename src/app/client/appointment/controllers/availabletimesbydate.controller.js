export class AvailableTimesByDateController {
  constructor(AppointmentService, $state, moment, $analytics) {
    'ngInject';
    $analytics.eventTrack('viewed appointment time', { category: 'Scheduling', label: 'Appointment Time Page Load' });
    this.appointmentService = AppointmentService;
    this.moment = moment;
    this.state = $state;
    this.getAvailableTimesByDayPart();
    this.dayPartName = this.appointmentService.dayPartName;
    this.selectedDate = this.moment(this.appointmentService.appointmentDate).format('dddd') + ', ' + this.moment(this.appointmentService.appointmentDate).format('LL');
    angular.element('.js-steps-bar').removeClass('fixed-at-top');
  }
  getTimeFromUTCDateTime(utcDateTime) {
    return this.moment(utcDateTime).format('LT');
  }
  goNext(timeStart, timeEnd) {
    this.appointmentService.appointmentStartTime = timeStart;
    this.appointmentService.appointmentEndTime = timeEnd;
    this.state.go('client.appointment.schedule.confirm-phone');
  }
  getAvailableTimesByDayPart() {
    let timeSlots;
    const params = { startDate: this.appointmentService.appointmentDate, endDate: this.appointmentService.appointmentDate, appointmentReasonId: this.appointmentService.appointmentTypeId, timeZone: this.appointmentService.localTimezone };
    this.appointmentService.getAvailableAppointmentsByMonth(params).then((response) => {
      for (let slot of response.days) {
        switch (this.appointmentService.dayPartName) {
          case 'morning':
            if (angular.isDefined(slot.slots.morning) && slot.slots.morning.length) {
              timeSlots = slot.slots.morning;
            }
            break;
          case 'afternoon':
            if (angular.isDefined(slot.slots.afternoon) && slot.slots.afternoon.length) {
              timeSlots = slot.slots.afternoon;
            }
            break;
          case 'evening':
            if (angular.isDefined(slot.slots.evening) && slot.slots.evening.length) {
              timeSlots = slot.slots.evening;
            }
            break;
          default:

        }
      }
      this.availableTimesByDayPart = timeSlots;
    });
  }
}
