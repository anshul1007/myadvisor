export class SessionTypeController {
  constructor(AppointmentService, $state, $analytics) {
    'ngInject';
    this.analytics = $analytics;
    this.appointmentService = AppointmentService;
    this.state = $state;
  }
  goNext(sessionType){
  	this.appointmentService.modeOfAppointment = sessionType;
    this.state.go('client.appointment.schedule.choose-topic');
  }
}
