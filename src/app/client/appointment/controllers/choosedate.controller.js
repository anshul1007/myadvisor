/*
  1) Get API response
  2) Create object of available date slots array
  3) pass dates to directive
  4) do require login change for making date disabled
*/
export class ChooseDateController {
  constructor(_, moment, $state, AppointmentService, $analytics) {
    'ngInject';
    this._ = _;
    this.moment = moment;
    this.$state = $state;
    this.appointmentService = AppointmentService;
    this.$analytics = $analytics;
    this.showSpinner = true;
    this.selectedDate = this.moment();
    this.months = [this.moment(), this.moment().add(1, 'month')];
    this.monthCount = 2;
    this.isAPIcall = false;

    this.$analytics.eventTrack('viewed type of calendar', { category: 'Client - Calendar / Scheduling', label: 'calendar view - monthly' });

    this.getAvailableSlots();
    this.dayPart = 'morning';
    this.appointmentService.dayPartName = this.dayPart;

    let weeksTopPos = 0;
    angular.element(window).bind('scroll', () => {
      const navbarHeight = angular.element('.js-navbar').outerHeight();
      let scrollTop = angular.element(window).scrollTop();
      let stepsBar = angular.element('.js-steps-bar');
      let weekNames = angular.element('.js-week-names');

      if (scrollTop >= navbarHeight) {
        stepsBar.addClass('fixed-at-top');
      } else {
        stepsBar.removeClass('fixed-at-top');
      }

      if (weeksTopPos === 0) {
        weeksTopPos = weekNames.position().top - stepsBar.outerHeight();
      }

      if (scrollTop >= weeksTopPos) {
        weekNames.addClass('fixed-at-top');
      } else {
        weekNames.removeClass('fixed-at-top');
      }

      if (!this.isAPIcall && (scrollTop + angular.element(window).height() > angular.element(document).height() - 300)) {
        this.loadMoreMonths();
      }
    });
  }

  loadMoreMonths() {
    if (this.monthCount < 12) {
      this.params.endDate = this.moment(this.params.endDate).add(60, 'days').valueOf();
      this.params.startDate = this.moment(this.params.startDate).add(60, 'days').valueOf();
      this.isAPIcall = true;
      this.appointmentService.getAvailableAppointmentsByMonth(this.params).then((response) => {
        this.isAPIcall = false;
        this.getDataByDayPart(response);
        this.months.push(this.moment().add(this.monthCount, 'month'));
        this.monthCount++;
      });
    }
  }

  setSelectedDate(selectedDate) {
    if (selectedDate) {
      this.selectedDate = selectedDate;
      this.appointmentService.appointmentDate = this.moment(selectedDate).valueOf();
      this.$analytics.eventTrack('selects appointment time', { category: 'Client - Calendar / Scheduling', label: 'appointment time' });
      angular.element(window).unbind('scroll');
      this.$state.go('client.appointment.schedule.available-times-by-date');
    }
  }

  updatedayPartString(dayPart) {
    this.dayPart = dayPart;
    this.appointmentService.dayPartName = this.dayPart;
    this.getAvailableSlotsByDayParts(dayPart);
  }

  getAvailableSlotsByDayParts(dayPart = 'morning') {
    let resultSet;
    switch (dayPart) {
      case 'afternoon':
        resultSet = this.slots.afternoon;
        break;
      case 'evening':
        resultSet = this.slots.evening;
        break;
      default:
        resultSet = this.slots.morning;
    }
    this.availableSlotsByDayPart = resultSet;
  }

  getDataByDayPart(response) {
    this.slotsObject = {};
    this.morning = this.morning || [];
    this.evening = this.evening || [];
    this.afternoon = this.afternoon || [];
    this._.each(response.days, (slot) => {
      if (angular.isDefined(slot.slots.morning)) {
        this.morning.push({ slotDate: slot.date, numberOfSlots: slot.slots.morning.length });
      }
      if (angular.isDefined(slot.slots.afternoon)) {
        this.afternoon.push({ slotDate: slot.date, numberOfSlots: slot.slots.afternoon.length });
      }
      if (angular.isDefined(slot.slots.evening)) {
        this.evening.push({ slotDate: slot.date, numberOfSlots: slot.slots.evening.length });
      }
      this.slotsObject.morning = this.morning;
      this.slotsObject.evening = this.evening;
      this.slotsObject.afternoon = this.afternoon;
      this.slots = this.slotsObject;
      this.getAvailableSlotsByDayParts();
    });
  }

  getAvailableSlots() {
    this.params = { appointmentReasonId: 'fc', startDate: this.moment().valueOf(), endDate: this.moment().add(60, 'days').valueOf(), timeZone: this.appointmentService.localTimezone };
    this.appointmentService.getAvailableAppointmentsByMonth(this.params).then((response) => {
      this.getDataByDayPart(response);
      this.showSpinner = false;
    });
  }
}
