/*jshint esversion: 6*/
export class ChooseTopicController {
  constructor(AppointmentService, $state, $analytics, $log) {
    'ngInject';
    this.appointmentService = AppointmentService;
    this.showSpinner = true;
    this.getListOfAppointmentTypes();
    this.log = $log;
    this.state = $state;
    this.analytics = $analytics;
    $analytics.eventTrack('viewed appointment reason selection', {category: 'Client - Calendar / Scheduling', label: 'appointment reason' });
  }
    /*
     *  1) set appointmentTypeId in appointment service
     *  2) Go to state availiable times
     *  @params:
     *    id => appointmentTypeId
     */
  onclick(id) {
    try {
      this.appointmentService.appointmentTypeId = id;
      this.state.go('client.appointment.schedule.available-time');
      this.analytics.eventTrack('selects appointment reason', {category: 'Client - Calendar / Scheduling', label: 'appointment reason' });
    } catch (e) {
      this.log.debug('ChooseTopicController! => '+ e);
    }
  }
  getListOfAppointmentTypes() {
    this.reasons = this.appointmentService.getListOfAppointmentTypes().$object;
    this.showSpinner = false;
  }
}
