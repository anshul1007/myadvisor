export class AvailableTimeController {
  constructor(AppointmentService, $state, moment, $analytics, dateService) {
    'ngInject';
    this.state = $state;
    this.appointmentService = AppointmentService;
    this.moment = moment;
    this.dateService = dateService;
    this.getAvailableTimesByDayParts();

    $analytics.eventTrack('viewed next available', { category: 'Client - Calendar / Scheduling', label: 'appointment time' });
  }
  onclick(timeStart, timeEnd) {
    this.appointmentService.appointmentStartTime = timeStart;
    this.appointmentService.appointmentEndTime = timeEnd;
    this.state.go('client.appointment.schedule.confirm-phone');
  }

  getDay(appointmentDateInUTC) {
    return this.dateService.getDay(appointmentDateInUTC);
  }

  getDate(timeStart) {
    return this.dateService.getDate(timeStart, 'MM/DD/YY');
  }

  getTime(timeStart) {
    return this.dateService.getTime(timeStart);
  }

  getTimeObject(timeObject) {
    let obj;
    
    if (timeObject.slots.morning) {
      obj = timeObject.slots.morning[0];
    } else if (timeObject.slots.afternoon) {
      obj = timeObject.slots.afternoon[0];
    } else if (timeObject.slots.evening) {
      obj = timeObject.slots.evening[0];
    }

    return obj;
  }

  isMorningSlot(time) {
    let status = false;
    if (time.slots.morning) {
      status = true;
    }
    return status;
  }

  isEveningSlot(time) {
    let status = false;
    if (time.slots.evening) {
      status = true;
    }
    return status;
  }

  isAfternoonSlot(time) {
    let status = false;
    if (time.slots.afternoon) {
      status = true;
    }
    return status;
  }

  getAvailableTimesByDayParts() {
    this.showSpinner = true;
    let params = { appointmentReasonId: this.appointmentService.appointmentTypeId, timeZone: this.appointmentService.localTimezone };
    if (this.appointmentService.advisorObject.id && params.timeZone && params.appointmentReasonId) {
      this.appointmentService.getAvailableAppointmentsByMonth(params).then((response) => {
        this.availableTimes = response.plain().days;
        this.showSpinner = false;
      });
    }
  }
}
