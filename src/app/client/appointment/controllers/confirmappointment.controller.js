export class ConfirmAppointmentController {
  constructor($analytics, AppointmentService, $state, moment, helpers, $filter) {
    'ngInject';
    this.analytics = $analytics;
    this.helpers = helpers;
    $analytics.eventTrack('viewed appointment confirm screen', { category: 'Client - Calendar / Scheduling', label: 'appointment confirmation' });
    this.appointmentService = AppointmentService;
    this.state = $state;
    this.$filter = $filter;
    this.showSpinner = false;
    this.moment = moment;
    this.appointmentStartTime = this.moment(this.appointmentService.appointmentStartTime).format('LT');
    this.appointmentDate = this.moment(this.appointmentService.appointmentStartTime).format('dddd') + ', ' + this.moment(this.appointmentService.appointmentStartTime).format('LL');
  }

  getModeOfAppointment(){
    return this.appointmentService.modeOfAppointment;
  }
  //(123) 456 7890
  getClientCallBackNumber(){
    return this.$filter('phone')(this.appointmentService.clientCallbackNumber);
  }
  goNext() {
    this.showSpinner = true;
    this.appointmentService.createAppointment().then((response) => {
      if (response.id) {
        this.state.go('client.appointment.upcoming-appointments');
        this.showSpinner = false;
      }
    });
    this.analytics.eventTrack('selected appointment time', {category: 'Client - Calendar / Scheduling', label: 'appointment time - '+ this.appointmentStartTime});
    this.analytics.eventTrack('selected appointment day', {category: 'Client - Calendar / Scheduling', label: 'appointment day - '+ this.appointmentDay});
    this.analytics.eventTrack('selected appointment time', { category: 'Calendar / Scheduling', label: 'appointment time - hh:mm - Monday' });
  }
  exportToCal(){
    this.analytics.eventTrack('selects to export to personal calendar', {category: 'Client - Calendar / Scheduling', label: 'appointment export'});
  }
}
