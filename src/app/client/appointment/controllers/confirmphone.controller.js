export class ConfirmPhoneController {
  constructor(AppointmentService, $state, $analytics, $scope, moment) {
    'ngInject';
    this.analytics = $analytics;
    $analytics.eventTrack('viewed appointment confirmation screen', { category: 'Client - Calendar / Scheduling', label: 'appointment confirmation' });
    this.appointmentService = AppointmentService;
    this.state = $state;
    this.scope = $scope;
    this.moment = moment;
    this.appointmentStartTime = moment(this.appointmentService.appointmentStartTime).format('LT');
    this.appointmentDate = moment(this.appointmentService.appointmentStartTime).format('dddd') + ', ' + moment(this.appointmentService.appointmentStartTime).format('LL');
  }
  goNext() {
    if (this.scope.confirmPhoneForm.$valid) {
      this.appointmentService.clientCallbackNumber = this.scope.phoneNumber;
      this.analytics.eventTrack('entered appointment phone number', { category: 'Client - Calendar / Scheduling', label: 'appointment contact info' });
      this.state.go('client.appointment.schedule.confirm-appointment');
    }
  }
}
