export class AppointmentsController {
  constructor(helpers, $scope, $state, $sessionStorage, stringConstants, AppointmentService, moment, dateService, $interval) {
    'ngInject';
    this.appointmentService = AppointmentService;
    this.moment = moment;
    this.helpers = helpers;
    this.dateService = dateService;
    this.$state = $state;
    this.stringConstants = stringConstants;
    this.isTabletDevice = $sessionStorage.isTabletDevice;
    $sessionStorage.isTabletDevice = false;
    this.$sessionStorage = $sessionStorage;
    this.missedAppointment = false;
    this.isCallAbtToStart = false;
    this.$interval = $interval;
    this.canceledMap = new Map();
    this.tabIndex = 0;

    //@Todo have to remove below mock data once API integration gets done
    this.archives = [{
      open: true,
      time: 'Yesterday at 9:00am / 25 min call',
      reason: 'Set a retirement goal plan for Marcus Alonso.',
      agenda: 'Retirement Goal 1\n Discussion point 2\n Discussion point 3',
      recommendations: 'Next step 1\n Next step 2\n Next step 3\n Next step 4',
      state: 'Open'
    }, {
      open: false,
      time: 'May 07, 2016 at 9:00am',
      reason: 'Set a retirement goal plan for Marcus Alonso.',
      agenda: 'Retirement Goal 1\n Discussion point 2\n Discussion point 3',
      recommendations: 'Next step 1\n Next step 2\n Next step 3\n Next step 4',
      state: 'Missed by Client'
    }];

    $scope.ifDesktopDevice = (confCtrl) => this.ifDesktopDevice(confCtrl);
    $scope.sendMailtoAdvisor = () => this.sendMailtoAdvisor();
    this.getClientAppointments();
    this.getClientPastAppointments();
    this.isCallAbtToStartSync();
  }

  initSession(callType){
    this.$sessionStorage.isCallFromAppointmentPage = true; 
    this.$sessionStorage.appointmentObj = {
      conferenceType: callType,
      appointmentObj: this.upcomingAppointment
    };
    this.$state.go('client.dashboard');
  }

  switchTab(index) {
    this.tabIndex = index;
  }

  getDay(appointmentDateInUTC) {
    return this.dateService.getDay(appointmentDateInUTC);
  }

  getDate(timeStart) {
    return this.dateService.getCustomDateNoYear(timeStart);
  }

  getTime(timeStart) {
    return this.dateService.getTime(timeStart);
  }

  ifDesktopDevice(confCtrl) {
    if (this.helpers.isDesktopDevice()) {
      confCtrl.initConfFromAppointments(true);
      this.$state.go('client.dashboard');
    } else if (this.helpers.isTabletDevice()) {
      this.isTabletDevice = true;
    } else {
      confCtrl.initSession(false);
    }
  }

  sendMailtoAdvisor() {
    this.helpers.openNaiveMailClient(this.stringConstants.dashboard.conference.defaultMailId);
  }

  isVideoCall(appointment) {
    if (appointment) {
      return appointment.communicationPreference === 'VIDEO' ? true : false;
    } else {
      return this.upcomingAppointment.communicationPreference === 'VIDEO' ? true : false;
    }
  }

  getClientAppointments() {
    const params = { startDate: this.moment().valueOf() };
    this.appointmentService.retrieveClientCalendar(params).then((response) => {
      this.upcomingAppointments = response.appointments;
      if (!this.upcomingAppointments.length) {
        this.noAppointments = true;
      }
    });
  }

  deleteClientAppointment(appointmentId) {
    this.showSpinner = true;
    this.canceledMap.set(appointmentId, true);
    this.appointmentService.deleteAppointment(appointmentId).then((response) => {
      this.showSpinner = false;
    });
  }

  getClientPastAppointments() {
    const params = { startDate: this.moment().subtract(11, 'months').valueOf(), endDate: this.moment().valueOf() };
    this.appointmentService.retrieveClientCalendar(params).then((response) => {
      this.pastAppointments = response.appointments;
      if (!this.pastAppointments.length) {
        this.noPastAppointments = true;
      }
    });
  }

  getFormattedAppointmentDate(startDate) {
    return this.dateService.getCustomDateNoYear(startDate) + ' ' + this.dateService.getTime(startDate);
  }

  getClientDashboardAppointments() {
    const params = { startDate: this.moment().valueOf() };
    this.appointmentService.retrieveClientCalendar(params).then((response) => {
      this.upcomingAppointment = false;
      if (response.appointments.length) {
        this.upcomingAppointment = response.appointments[0];
      }
    });
  }

  isCallAbtToStartSync() {
    this.$interval(()=> {
      if (this.upcomingAppointment) {
        const appointmentStartTime = this.upcomingAppointment.startDate;
        const appointmentEndTime = this.upcomingAppointment.endDate;
        let joinAppointmentTime = this.moment(appointmentStartTime).subtract(15, 'minutes').toDate().getTime();
        let currentTime = this.moment().toDate().getTime();

        if (currentTime >= joinAppointmentTime && currentTime <= appointmentEndTime) {
          this.isCallAbtToStart = true;
        } else {
          this.isCallAbtToStart = false;
        }
      }
      this.getClientDashboardAppointments();
    }, 1000);
  }
}
