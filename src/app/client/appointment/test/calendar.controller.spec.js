describe('Describing ChooseDateController and its configuration functions', () => {
  let scope;
  let controller;
  let scrollElement;
  let calendarElement;
  let httpBackend;
  let AppointmentService;
  let deferred;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $compile, $httpBackend, _AppointmentService_, $q) => {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    deferred = $q.defer();
    AppointmentService = _AppointmentService_;
    spyOn(AppointmentService, 'getAvailableAppointmentsByMonth').and.callFake(function() {
      deferred.resolve(true);
      return deferred.promise;
    });
    httpBackend.whenGET(/.*/).respond({});
    controller = $controller('ChooseDateController', {
      $scope: scope
    });

    scrollElement = angular.element('<div when-scroll-ends="loadMoreMonths()" threshold="0">');
    $compile(scrollElement)(scope);
    scope.$digest();

    calendarElement = angular.element('<calendar month="month" selected-date="selectedDate" on-date-select="setSelectedDate(selectedDate)"></calendar>');
    $compile(calendarElement)(scope);
    scope.$digest();

  }));

  it('Checking month object: month should have 2 length', () => {
    expect(controller.months.length).toBe(2);
  });

  it('Checking loadMoreMonths method: month should have 3 length', () => {
    controller.loadMoreMonths();
    expect(controller.months.length).toBe(2);
    controller.setSelectedDate(controller.months[0]);
  });

  it('Checking element scroll', () => {
    scrollElement.triggerHandler("scroll");
  });

  it('Checking date select', () => {
    calendarElement.find('.day:not(.disabled):eq(5)').click();
  });

});
