/**
 * @todo 
 * This test case is for ChooseTopicController.
 */
describe('controller ChooseTopicController', function() {
  let ctrl;
  let scope;
  let state;
  let appointmentService;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $state, _AppointmentService_) => {
    state = $state;
    appointmentService = _AppointmentService_;
    ctrl = $controller('ChooseTopicController', {
      $scope: scope
    });
  }));

  it('onclick function', () => {
    spyOn(state, 'go');
    ctrl.onclick('fc');
    expect(state.go).toHaveBeenCalledWith('client.appointment.schedule.available-time');
  });
});
