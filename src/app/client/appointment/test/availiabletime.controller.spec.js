/**
 * @todo 
 * This test case is for AvailableTimeController.
 */
describe('controller AvailableTimeController', function() {
  let ctrl;
  let scope;
  let state;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $state) => {
    state = $state;
    ctrl = $controller('AvailableTimeController', {
      $scope: scope
    });
  }));

  it('onclick function', () => {
    spyOn(state, 'go');
    ctrl.onclick();
    expect(state.go).toHaveBeenCalledWith('client.appointment.schedule.confirm-phone');
  });

  it('getDay function', () => {
    let day1 = ctrl.getDay('1461956400000');
    expect(day1).toEqual('Today');
  });

  it('getDate function', () => {
    let date = ctrl.getDate('1461956400000');
    expect(date).toEqual('Invalid date');
  });

  it('getTime function', () => {
    let time = ctrl.getTime('1461956400000');
    expect(time).toEqual('Invalid date');
  });

  it('isMorningSlot function', () => {
    let time = { 'slots': { morning: true } };
    let slot = ctrl.isMorningSlot(time);
    expect(slot).toEqual(true);
  });

  it('isEveningSlot function', () => {
    let time = { 'slots': { evening: true } };
    let slot = ctrl.isEveningSlot(time);
    expect(slot).toEqual(true);
  });

  it('isAfternoonSlot function', () => {
    let time = { 'slots': { afternoon: true } };
    let slot = ctrl.isAfternoonSlot(time);
    expect(slot).toEqual(true);
  });

  it('getAvailableTimesByDayParts function', () => {
    ctrl.appointmentService = { 'advisorObject': { id: 1 } };
    let availableSlots = ctrl.getAvailableTimesByDayParts();
    expect(availableSlots).toBeUndefined();
  });
});
