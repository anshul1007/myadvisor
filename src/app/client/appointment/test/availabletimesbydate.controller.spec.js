/**
 * @todo 
 * This test case is for AvailableTimesByDateController.
 */
describe('controller AvailableTimesByDateController', function() {
  let ctrl;
  let scope;
  let state;
  let AppointmentService;
  let httpBackend;
  let deferred;

  beforeAll(() => {
    window.onbeforeunload = () => 'reload!';
  });

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $state, _AppointmentService_, $httpBackend, $q) => {
    scope = $rootScope.$new();
    state = $state;
    httpBackend = $httpBackend;
    deferred = $q.defer();
    AppointmentService = _AppointmentService_;
    spyOn(AppointmentService, 'getAvailableAppointmentsByMonth').and.callFake(function() {
      deferred.resolve(false);
      return deferred.promise;
    });
    httpBackend.whenGET('/api/v1/calendar/advisors/1').respond({});
    ctrl = $controller('AvailableTimesByDateController', {
      $scope: scope
    });
  }));

  it('goNext function', () => {
    spyOn(state, 'go');
    ctrl.goNext(1464554349625, 1464554349625);
    expect(state.go).toHaveBeenCalledWith('client.appointment.schedule.confirm-phone');
  });

});
