/**
 * @todo 
 * This test case is for ConfirmAppointmentController.
 */
describe('controller ConfirmAppointmentController', function() {
  let ctrl;
  let scope;
  let state;
  let AppointmentService;
  let httpBackend;
  let deferred;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $state, _AppointmentService_, $httpBackend, $q) => {
    state = $state;
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    deferred = $q.defer();
    AppointmentService = _AppointmentService_;
    spyOn(AppointmentService, 'createAppointment').and.callFake(function() {
      deferred.resolve(false);
      return deferred.promise;
    });
    httpBackend.whenPOST('/api/v1/calendar/appointments').respond({});
    ctrl = $controller('ConfirmAppointmentController', {
      $scope: scope,
    });
  }));

  // it('goNext function', () => {
  //   ctrl.goNext();
  //   spyOn(state, 'go');
  //   expect(state.go).toHaveBeenCalledWith('client.appointments');
  // });
});
