/**
 * @todo  In Progree not completed
 * This test case is for AppointmentService.
 */
describe('controller Appointment Service', function() {
  let appointmentService;
  let restangular;
  let session;
  let deferred;
  let httpBackend;
  let scope;

  beforeEach(angular.mock.module('MainApp'));
  beforeEach(inject(($rootScope, _AppointmentService_, _Restangular_, _Session_, $httpBackend, $q) => {
    appointmentService = _AppointmentService_;
    restangular = _Restangular_;
    session = _Session_;
    httpBackend = $httpBackend;
    deferred = $q.defer();
    scope = $rootScope.$new();
    httpBackend.whenGET(/.*/).respond({});

  }));

  it('advisorObject', () => {
    let advisor = { 'advisorObject': { id: 1 } };
    appointmentService.advisorObject = advisor;
    expect(appointmentService.advisorObject).toBe(advisor);
  });
  it('appointmentTypeId', () => {
    appointmentService.appointmentTypeId = 1;
    expect(appointmentService.appointmentTypeId).toBe(1);
  });

  it('loggedInClientId', () => {
    appointmentService.loggedInClientId = 1;
    expect(appointmentService.loggedInClientId).toBe(1);
  });

  it('loggedInClientId', () => {
    appointmentService.loggedInClientId = 1;
    expect(appointmentService.loggedInClientId).toBe(1);
  });

  it('dayPartName', () => {
    appointmentService.dayPartName = 'morning';
    expect(appointmentService.dayPartName).toBe('morning');
  });

  it('localTimezone', () => {
    appointmentService.localTimezone = 'UTC';
    expect(appointmentService.localTimezone).toBe('UTC');
  });

  it('appointmentDate', () => {
    appointmentService.appointmentDate = '12345678912';
    expect(appointmentService.appointmentDate).toBe('12345678912');
  });

  it('appointmentStartTime', () => {
    appointmentService.appointmentStartTime = '12345678912';
    expect(appointmentService.appointmentStartTime).toBe('12345678912');
  });

  it('appointmentEndTime', () => {
    appointmentService.appointmentEndTime = '12345678912';
    expect(appointmentService.appointmentEndTime).toBe('12345678912');
  });

  it('clientCallbackNumber', () => {
    appointmentService.clientCallbackNumber = '12345678912';
    expect(appointmentService.clientCallbackNumber).toBe('12345678912');
  });

  it('getListOfAppointmentTypes', () => {
    let advisorId = 1;
    let requestObject = {};
    let retVal = '';
    deferred.resolve('resolveData');
    spyOn(appointmentService, 'getListOfAppointmentTypes').and.returnValue(deferred.promise);
    appointmentService.getListOfAppointmentTypes(advisorId, requestObject)
      .then(function(result) {
        retVal = result;
      });
    scope.$digest();
    expect(retVal).toBe('resolveData');
  });

  it('getAvailableAppointmentsByMonth', () => {
    let params = {};
    let retVal = '';
    deferred.resolve('resolveData');
    spyOn(appointmentService, 'getAvailableAppointmentsByMonth').and.returnValue(deferred.promise);
    appointmentService.getAvailableAppointmentsByMonth(params)
      .then(function(result) {
        retVal = result;
      });
    scope.$digest();
    expect(retVal).toBe('resolveData');
  });

  it('createAppointment', () => {
    let params = {};
    let retVal = '';
    deferred.resolve('resolveData');
    spyOn(appointmentService, 'createAppointment').and.returnValue(deferred.promise);
    appointmentService.createAppointment(params)
      .then(function(result) {
        retVal = result;
      });
    scope.$digest();
    expect(retVal).toBe('resolveData');
  });


  // it('getAvailableTimesByDayParts', () => {
  //   let advisorId = 1;
  //   let requestObject = {};
  //   expect(appointmentService.getAvailableTimesByDayParts(advisorId, requestObject)).toBeDefined();
  // });

});
