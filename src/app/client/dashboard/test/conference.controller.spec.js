describe('Describing ConferenceController and its configuration functions', function() {
  let scope;
  let controller;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $compile, $window) => {
    scope = $rootScope.$new();
    $window = { location: { href: jasmine.createSpy() } };
    controller = $controller('ConferenceController', {
      $scope: scope
    });
  }));

  // it('Initiating constructor and validating scope values...', () => {
  //   expect(controller.isAvailableAfterMissed).toBe(false);
  // });

  // it('Calling isAdvisorAccepted', () => {
  //   expect(controller.isAdvisorAccepted()).toBe(false);
  // });

  // it('Calling isJoinMeetingEnabled', () => {
  //   expect(controller.isJoinMeetingEnabled()).toBe(true);
  // });

  it('Calling isSessionProgess', () => {
    expect(controller.isSessionProgess()).toBe(false);
  });

  it('Calling isConferenceStart', () => {
    expect(controller.isConferenceStart()).toBe(false);
  });

  it('Calling isWaitingRoomEnabled', () => {
    expect(controller.isWaitingRoomEnabled()).toBe(false);
  });

  it('Calling isAdvisorDecline', () => {
    expect(controller.isAdvisorDecline()).toBe(false);
  });

  // it('Calling initConferencePlayer', () => {
  //   expect(controller.initConferencePlayer()).toBeUndefined();
  // });

  // it('Calling initConf', () => {
  //   expect(controller.initConf(true)).toBeUndefined();
  // });

  // it('Calling initConferenceSync', () => {
  //   controller.$sessionStorage.startConference = true;
  //   expect(controller.initConferenceSync()).toBeUndefined();
  // });

  // it('Calling initSession', () => {
  //   controller.helpers.isDesktopDevice = function () {
  //     return true;
  //   }
  //   expect(controller.initSession(true)).toBeUndefined();
  // });

  // it('Calling initSession', () => {
  //   controller.helpers.isDesktopDevice = function () {
  //     return false;
  //   }
  //   controller.helpers.isTabletDevice = function () {
  //     return true;
  //   }
  //   controller.state.go = function(data) {
  //     return data;
  //   }
  //   expect(controller.initSession(true)).toBeUndefined();
  // });

  // it('Calling sendMailtoAdvisor', () => {
  //   controller.helpers.openNaiveMailClient = function (data) {
  //     return true;
  //   }
  //   expect(controller.sendMailtoAdvisor()).toBeUndefined();
  // });

  // it('Calling initSession', () => {
  //   controller.helpers.isDesktopDevice = function () {
  //     return false;
  //   }
  //   controller.helpers.isTabletDevice = function () {
  //     return false;
  //   }
  //   controller.window.location.href = function(data) {
  //     return data;
  //   }
  //   expect(controller.initSession(true)).toBeUndefined();
  // });

  // it('Calling startSession', () => {
  //   expect(controller.startSession(true, true)).toBeUndefined();
  // });

  // it('Calling startSession', () => {
  //   expect(controller.startSession(false, false)).toBeUndefined();
  // });
});