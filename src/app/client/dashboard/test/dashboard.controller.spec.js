describe('Describing DashboardController and its configuration functions', function() {
  let scope;
  let controller;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $compile) => {
    scope = $rootScope.$new();

    controller = $controller('DashboardController', {
      $scope: scope
    });
  }));
});