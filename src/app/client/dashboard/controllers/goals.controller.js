export class GoalsController {
  constructor($scope, GoalsService, UserService) {
    'ngInject';

    this.$scope = $scope;
    this.GoalsService = GoalsService;
    this.UserService = UserService;

    this.getClientGoals();

    this.goals = [{
      name: 'Retirement',
      chart: this.getRetirementChartOptions(),
      data: {
        details: [{
          key: 'Timeframe',
          value: '22 Years'
        }, {
          key: 'Goal Importance',
          value: 'Very Important'
        }, {
          key: 'Investor Profile',
          value: 'Balanced',
          tooltip: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut arcu sit amet nibh sollicitudin iaculis. Morbi efficitur metus in auctor vehicula.'

        }],
        funds: [{
          key: 'Current',
          value: '500000',
          note: 'Investible assets'
        }, {
          key: 'Target',
          value: '2500000',
          note: '$3,735 / month in retirement'
        }, {
          key: 'Projected',
          value: '2000000',
          tooltip: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ut arcu sit amet nibh sollicitudin iaculis. Morbi efficitur metus in auctor vehicula.',
          note: '$2,513 / month in retirement'
        }],
        accounts: {
          headings: ['ACCOUNT TYPE', 'INSTITUTION', 'OWENERSHIP', 'BALANCE'],
          data: [
            ['RRSP*', 'RBC', 'Single', '400000'],
            ['TFSA*', 'RBC', 'Single', '400000'],
            ['RRSP*', 'RBC', 'Single', '400000']
          ]
        }
      }
    }];

    this.$scope.$watch(() => {
      return this.GoalsService.currentGoalType;
    }, () => {
      this.changeGoal(this.GoalsService.currentGoalType);
    });
  }

  getClientGoals() {
    const params = {
      clientId: this.UserService.clientDetails.clientId
    };

    this.GoalsService.getGoals(params).then((response) => {
      this.GoalsService.allGoals = response.plain();
      this.allGoals = angular.copy(this.GoalsService.allGoals);
      this.listGoals = this.GoalsService.createGoalsList();
      this.changeGoal(this.listGoals[0].type);
    });
  }

  changeGoal(goalType) {
    this.currentGoalType = goalType;
    this.GoalsService.currentGoalType = goalType;
  }

  getRetirementChartOptions() {
    return {
      credits: {
        enabled: false
      },
      exporting: {
        enabled: false
      },
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        width: '275',
        height: '250',
        spacing: 0
      },
      title: {
        text: 'Retirement',
        align: 'center',
        verticalAlign: 'middle',
        style: {
          fontSize: '16px',
          color: '#444'
        },
        y: 0
      },
      subtitle: {
        useHTML: true,
        text: '<div style="text-align:center; font-size: 16px; color: #bb0016;"><i class="fa fa-exclamation-circle" style="font-size: 22px;"></i><br>Needs<br>Attention</div>',
        align: 'center',
        verticalAlign: 'middle',
        y: 60
      },
      tooltip: false,
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            y: -15,
            distance: 5,
            padding: 0,
            style: {
              fontSize: '12px',
              fontWeight: 'normal',
              color: '#999',
              textShadow: 'none'
            },
            formatter: function() {
              return this.point.name + '<br> ' + this.y + ' MM';
            }
          },
          minSize: 150,
          startAngle: -150,
          endAngle: 150,
          center: ['50%', '50%']
        }
      },
      series: [{
        type: 'pie',
        innerSize: '70%',
        data: [{
          y: 3,
          name: 'Current',
          value: '$500k',
          color: '#2b4460'
        }, {
          y: 1.75,
          name: 'Projected',
          value: '$2.5m',
          color: '#ede2cb'
        }, {
          y: 1.75,
          name: 'Target',
          value: '$2.5m',
          color: '#bb0016'
        }, {
          y: 1.5,
          color: '#f5f5f5',
          dataLabels: false
        }],
        states: {
          hover: {
            enabled: false
          }
        }
      }]
    };
  }
}
