export class NetWorthController {
  constructor($scope, $interval, DashboardService, $timeout) {
    'ngInject';

    this.isLoading = true;
    this.isExpanded = false;
    this.timeout = $timeout;
    this.chartOptions = {};
    this.toggleData = true;
    this.dashboardService = DashboardService;

    $scope.loadNetWorthChart = () => this.loadNetWorthChart();
    $timeout(() => {
      this.loadNetWorthChart();
      this.isLoading = false;
    }, 100);

    $scope.$on('loadNetWorth', (event, args) => {
      $timeout(() => {
        this.isExpanded = false;
        this.loadNetWorthChart();
        this.isLoading = false;
      }, 100);
      this.isLoading = true;
      // if(!this.chartOptions.isReload) {
      //   this.chartOptions.isReload = true;
      // }
      // else {
      //   this.loadNetWorthChart();
      // }
    });

    $scope.$on('updateSandBoxChart', (event, args) => {
      this.changeData();
    });
    this.getNetWorthData();
  }

  legendsStroks() {
    angular.element('.highcharts-legend-item path').attr('stroke-width', '12');
  }

  loadNetWorthChart() {
    let retirementAge = 2015;

    let investAssetData = [{ age1: 45, age2: 43, x: 2010, y: 700000 }, { age1: 46, age2: 44, x: 2011, y: 1200000 }, { age1: 47, age2: 45, x: 2012, y: 1290000 }, { age1: 48, age2: 46, x: 2013, y: 1378000 }, { age1: 49, age2: 47, x: 2014, y: 1435000 }, { age1: 50, age2: 48, x: 2015, y: 1350000 }, { age1: 51, age2: 49, x: 2016, y: 1100000 }];

    let lifeStyleData = [{ age1: 45, age2: 43, x: 2010, y: 500000 }, { age1: 46, age2: 44, x: 2011, y: 567000 }, { age1: 47, age2: 45, x: 2012, y: 668000 }, { age1: 48, age2: 46, x: 2013, y: 754000 }, { age1: 49, age2: 47, x: 2014, y: 874000 }, { age1: 50, age2: 48, x: 2015, y: 1000000 }, { age1: 51, age2: 49, x: 2016, y: 1100000 }];

    let totalWorthData = [{ age1: 45, age2: 43, x: 2010, y: 650000 }, { age1: 46, age2: 44, x: 2011, y: 1200000 }, { age1: 47, age2: 45, x: 2012, y: 1290000 }, { age1: 48, age2: 46, x: 2013, y: 1378000 }, { age1: 49, age2: 47, x: 2014, y: 1435000 }, { age1: 50, age2: 48, x: 2015, y: 1350000 }, { age1: 51, age2: 49, x: 2016, y: 1100000 }];

    this.chartOptions = this.prepNetWorthChartOptions(retirementAge, investAssetData, lifeStyleData, totalWorthData);
  }

  prepNetWorthChartOptions(retirementAge, investAssetData, lifeStyleData, totalWorthData) {
    return {
      isReload: false,
      colName: 'js-net-worth-chart',
      chart: {
        backgroundColor: 'transparent',
        plotBackgroundColor: '#f5f5f5',
        height: '450',
        spacing: [17, 0, 10, 0],
        events: {
          load: () => {
            this.legendsStroks();
          },
          redraw: () => {
            this.legendsStroks();
          }
        }
      },

      exporting: {
        enabled: false
      },

      credits: {
        enabled: false
      },

      plotOptions: {
        series: {
          pointPlacement: 'on',
          states: {
            hover: {
              enabled: true,
              halo: {
                size: 0
              }
            }
          },
          events: {
            mouseOver: function() {
              angular.element('.expand-collapse-chart').css('z-index', '1');
            },
            mouseOut: function() {
              angular.element('.expand-collapse-chart').css('z-index', '2');
            }
          }
        }
      },

      title: {
        text: ''
      },

      legend: {
        verticalAlign: 'top',
        itemDistance: 10,
        align: 'left',
        symbolWidth: 12,
        symbolHeight: 12,
        itemStyle: {
          fontSize: '12px',
          fontWeight: 'normal',
          color: '#1eb7fa'
        },
        x: 52
      },

      xAxis: {
        tickmarkPlacement: 'on',
        // labels: {
        //   formatter: function() {
        //     if (this.isFirst) {
        //       return 'Today';
        //     } else if (this.isLast) {
        //       return 2016;
        //     }
        //   },
        //   y: 20
        // },
        tickLength: 0,
        plotLines: [{
          color: '#dbdbdb',
          width: 1,
          value: retirementAge,
          zIndex: 5,
          label: {
            text: 'retirement',
            align: 'right',
            x: -13,
            y: -5,
            verticalAlign: 'bottom',
            style: {
              color: '#ffffff'
            }
          }
        }, {
          color: '#dbdbdb',
          width: 1,
          value: retirementAge - 1,
          zIndex: 5,
          label: {
            text: 'Spouse\'s retirement',
            align: 'right',
            x: -13,
            y: -5,
            verticalAlign: 'bottom',
            style: {
              color: '#ffffff'
            }
          }
        }]
      },

      yAxis: {
        title: false,
        labels: {
          formatter: function() {
            return '$' + this.axis.defaultLabelFormatter.call(this);
          }
        }
      },

      lang: {
        noData: 'No Data to Display'
      },

      tooltip: {
        enabled: true,
        shared: true,
        crosshairs: {
          width: 2,
          color: '#00c7c6',
          dashStyle: 'solid',
          zIndex: 5
        },
        useHTML: true,
        backgroundColor: '#fdfdfd',
        borderColor: '#fdfdfd',
        hideDelay: 0,
        style: {
          color: '#000000',
          fontWeight: 'normal',
          fontSize: '10px'
        },
        positioner: function(labelWidth, labelHeight, point) {
          let tooltipX;
          if (point.plotX + labelWidth > this.chart.plotWidth) {
            tooltipX = point.plotX + this.chart.plotLeft - labelWidth;
            tooltipX = tooltipX + (labelWidth / 2);
          } else {
            tooltipX = point.plotX + this.chart.plotLeft;
            tooltipX = tooltipX - (labelWidth / 2);
          }
          // tooltipY = point.plotY + this.chart.plotTop - 20;
          return {
            x: tooltipX,
            y: -190
          };
        },
        formatter: function() {
          let toolTipHtml = '<div class="tooltip-format">';
          let innerRows = '';
          let x = this.x;
          let age1 = this.points[0].point.age1;
          let age2 = this.points[0].point.age2;
          let series = this.points[0].point.series.chart.series;
          toolTipHtml = toolTipHtml + '<div><b>' + x + '</b></div><div class="age-col"><span>Your Age: ' + age1 + '</span>&nbsp;&nbsp;<span>Spouse\'s Age: ' + age2 + '</span></div><hr/>';
          angular.element.each(series, function(i, s) {
            angular.element.each(s.data, function(j, d) {
              if (d.x === x) {
                innerRows = innerRows + '<div class="detail-container"><div class="fl-left">' + s.name + ': </div><div class="fl-right">$' + d.y + '</div></div>';
              }
            });
          });
          innerRows = innerRows + '<div class="detail-container"><div class="fl-left">Cash: </div><div class="fl-right">$500000</div></div><div class="detail-container"><div class="fl-left">Liabilities: </div><div class="fl-right">$500000</div></div><div class="detail-container no-border"><div class="fl-left">Education Expense: </div><div class="fl-right">$500000</div></div>';

          toolTipHtml = toolTipHtml + innerRows + '</div>';

          return toolTipHtml;
        }
      },

      series: [{
        name: 'Investable Assets',
        color: '#ede2cb',
        type: 'area',
        fillColor: '#ede2cb',
        lineWidth: 0,
        legendIndex: 2,
        marker: {
          symbol: 'circle',
          enabled: false,
          states: {
            hover: {
              enabled: false
            }
          }
        },
        data: investAssetData
      }, {
        name: 'Lifestyle Assets',
        type: 'area',
        fillColor: '#2b4460',
        legendIndex: 3,
        color: '#2b4460',
        lineWidth: 0,
        marker: {
          symbol: 'circle',
          enabled: false,
          states: {
            hover: {
              enabled: false
            }
          }
        },
        data: lifeStyleData
      }, {
        name: 'Total Net Worth',
        shared: true,
        type: 'line',
        color: '#f1c302',
        legendIndex: 1,
        marker: {
          symbol: 'circle',
          enabled: false,
          fillColor: '#00c7c6',
          lineColor: '#00c7c6',
          radius: 2
        },
        data: totalWorthData
      }]
    };
  }

  changeData() {
    if (this.toggleData) {
      this.chartOptions.series[0].data = [];
    } else {
      this.loadNetWorthChart();
    }
    this.toggleData = !this.toggleData;
  }

  expandCollapse() {
    this.isExpanded = !this.isExpanded;
  }

  getNetWorthData() {
    this.dashboardService.getNetWorthData().then((response) => {
      this.netWorthData = response.plain();
    });
  }

  dataDisplayConditionsForName(item, parent) {
    let result = null;
    switch (parent) {
      case 'investments':
        result = item.name;
        break;
      case 'banking':
        result = item.name;
        break;
      case 'liabilities':
        result = item.name;
        break;
      case 'lifestyleAssets':
        result = item.name;
        break;

      default:
    }
    return result;
  }
  dataDisplayConditionsForAmount(item, parent) {
    let result = null;
    switch (parent) {
      case 'investments':
        result = item.currentBalance;
        break;
      case 'banking':
        result = item.currentBalance;
        break;
      case 'liabilities':
        result = item.bookValue;
        break;
      case 'lifestyleAssets':
        result = item.marketValue;
        break;
      default:
    }
    return result;
  }
}
