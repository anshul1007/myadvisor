class ModalInstanceCtrl {
  constructor($uibModalInstance) {
    'ngInject';
    this.uibModalInstance = $uibModalInstance;
  }
  ok() {
    this.uibModalInstance.close();
  }
  cancel() {
    this.uibModalInstance.dismiss();
  }
}
export class SandBoxController {
  constructor($scope, stringConstants, helpers, $timeout, $window, $uibModal, $state, $analytics, $rootScope, OTService) {
    'ngInject';
    this.isSandBox = false;
    this.analytics = $analytics;
    this.$timeout = $timeout;
    this.$window = $window;
    this.isDesktop = helpers.isDesktopDevice();
    this.OTService = OTService;

    this.retirementAge = {
      options: {}
    };
    this.annualGoalContrib = {
      options: {}
    };
    this.annualGoalContribTFSA = {
      options: {}
    };
    this.retirementExp = {
      options: {}
    };
    this.annualCost = {
      options: {}
    };
    this.amount = {
      options: {}
    };
    this.purchaseYear = {
      options: {}
    };

    $rootScope.$on('co-browsing:updateChart', (event, args) => {
      console.log('Client', args);
    });

    $scope.getContent = (type) => this.getContent(type);

    this.getGoalList();
    this.getInvestorProfileList();
    this.uibModal = $uibModal;
    this.state = $state;

    this.$timeout(() => {
      this.hidePopover();
    }, 5000);

    $scope.sliders = { retirementAge: 60, spouseretirementAge: 60, annualGoalContrib: 500, annualGoalContribTFSA: 10000, retirementExp: 0 };

    this.scope = $scope;
    this.tabIndex = 1;
    this.isEditGoal = false;
  }

  setTab(tabIndex) {
    this.getContent(tabIndex);
    this.tabIndex = tabIndex;
  }

  activateSandBox() {
    this.getContent(this.tabIndex);
    this.isSandBox = true;
  }

  deactivateSandBox() {
    this.isSandBox = false;
    this.getContent(this.tabIndex);
  }

  isSandBoxTab() {
    return this.tabIndex === 1 || this.tabIndex === 2 ? true : false;
  }

  isCashFlowTab() {
    return this.tabIndex === 2 ? true : false;
  }

  isGoalsTab() {
    return this.tabIndex === 3 ? true : false;
  }

  editable() {
    this.isEditable = true;
  }

  updateGoal() {
    this.closeEditGoal();
  }

  closeEditable() {
    this.isEditable = false;
  }

  getContent(type) {
    if (!this.isSandBox) {
      if (type === 2) {
        this.scope.$broadcast('loadCashFlow', {});
      } else if (type === 1) {
        this.scope.$broadcast('loadNetWorth', {});
      }
    }
    this.$timeout(() => {
      this.$window.dispatchEvent(new Event('resize'));
    }, 5);
  }

  sendSignal() {
    let signalPayload = [{
      eduExp: 1000
    }];

    let signalErrorFunc = function() {
      // TODO
    };

    if (this.OTService.OTSession.streams.length) {
      this.OTService.sendSignalToOTSession('updateChart', signalPayload, signalErrorFunc);
    }
  }

  getGoalList() {
    this.selectGoalList = ['Retirement', 'Education', 'Major Expense'];
    this.selectedGoal = this.selectGoalList[0];
    this.initSliders();
  }

  getInvestorProfileList() {
    this.selectedInvestorProfileList = [
      ['Secure', 'You’re equally measured when investing, demonstrating an average risk tolerance and a longer capacity to see any returns.'],
      ['Very Conservative', 'Though you play the long game better you’re still stiff when investing and exercise a very low tolerance for risk — some investments are safeguarded more than others.'],
      ['Conservative', 'You’re willing to ride it out when it comes to investing since you’re more stable and sensible with in your risk tolerance— reasonably safe with your investments.'],
      ['Growth', 'You are more proactive around investing since your risk tolerance is above average — growing your investments is priority.'],
      ['Balanced', 'You’re equally measured when investing, demonstrating an average risk tolerance and a longer capacity to see any returns.'],
      ['Aggressive Growth', 'You strike hard and fast but can play the long game since you have a high risk tolerance — maximizing capital growth of your investments are crucial.']
    ];

    this.selectedInvestorProfile = this.selectedInvestorProfileList[0][0];
    this.selectedInvestorProfileDesc = this.selectedInvestorProfileList[0][1];
  }

  resetSandboxCtrls() {
    this.getGoalList();
    this.getInvestorProfileList();

    this.scope.sliders = { retirementAge: 60, spouseretirementAge: 60, annualGoalContrib: 500, annualGoalContribTFSA: 10000, retirementExp: 0 };
  }

  getToolTip(prefix, suffix, value, isMoneyRange = 0) {
    let tipVal;
    if (isMoneyRange) {
      if (value >= 1000) {
        tipVal = (value / 1000);
        tipVal = prefix + tipVal + suffix;
      } else {
        tipVal = prefix + value;
      }
    } else {
      tipVal = prefix + value + suffix;
    }

    return tipVal;
  }

  initSliders() {
    this.loadRetirementAgeSlider();
    this.loadAnnualGoalContribSlider();
    this.loadAnnualGoalContribTFSASlider();
    this.loadRetirementExpSlider();
    this.loadAnnualCostSlider();
    this.loadAmountSlider();
    this.loadPurchaseYearSlider();
  }

  saveSandBox() {
    let uibModalInstance = this.uibModal.open({
      animation: true,
      templateUrl: 'app/client/dashboard/views/_sandbox-save-confirm.html',
      // size: 'sm',
      scope: this.scope,
      controller: ModalInstanceCtrl,
      controllerAs: 'mCtrl',
      bindToController: true,
      backdrop: 'static',
      keyboard: false
    });

    uibModalInstance.result.then(() => {
      // TODO
      this.analytics.eventTrack('viewed sandbox schedule appointment CTA', { category: 'Client - MVFP', label: 'appointment schedule - No' });
    }, () => {
      this.analytics.eventTrack('viewed sandbox schedule appointment CTA', { category: 'Client - MVFP', label: 'appointment schedule - Yes' });
      this.state.go('client.appointment.choosetopic');
    });
  }

  hidePopover() {
    angular.element(document).trigger('click');
  }

  createSlider(valObj) {
    return {
      min: valObj.min,
      max: valObj.max,
      step: valObj.step,
      precision: 2,
      orientation: 'horizontal',
      handle: 'round',
      tooltip: 'show',
      tooltipseparator: ':',
      tooltipsplit: false,
      enabled: true,
      naturalarrowkeys: false,
      range: false,
      ngDisabled: false,
      reversed: false,
      ticks: valObj.ticks,
      ticksLabels: valObj.ticksLabels
    };
  }

  loadRetirementAgeSlider() {
    const valObj = {
      min: 60,
      max: 80,
      step: 1,
      ticks: [60, 70, 80],
      ticksLabels: ['60', '80']
    };
    this.retirementAge.options = this.createSlider(valObj);
  }

  loadAnnualGoalContribSlider() {
    const valObj = {
      min: 500,
      max: 1000,
      step: 10,
      ticks: [500, 800, 1000],
      ticksLabels: ['$500', '$1K']
    };
    this.annualGoalContrib.options = this.createSlider(valObj);
  }

  loadAnnualGoalContribTFSASlider() {
    const valObj = {
      min: 10000,
      max: 50000,
      step: 100,
      ticks: [10000, 32000, 50000],
      ticksLabels: ['$10K', '$50K']
    };
    this.annualGoalContribTFSA.options = this.createSlider(valObj);
  }

  loadRetirementExpSlider() {
    const valObj = {
      min: 0,
      max: 10000,
      step: 100,
      ticks: [0, 6000, 10000],
      ticksLabels: ['$0K/mo', '$10K/mo']
    };
    this.retirementExp.options = this.createSlider(valObj);
  }

  loadAnnualCostSlider() {
    const valObj = {
      min: 0,
      max: 20000,
      step: 100,
      ticks: [0, 14000, 20000],
      ticksLabels: ['$0K', '$20K']
    };
    this.annualCost.options = this.createSlider(valObj);
  }

  loadAmountSlider() {
    const valObj = {
      min: 0,
      max: 100000,
      step: 0,
      ticks: [0, 60000, 100000],
      ticksLabels: ['$0K', '$100K']
    };
    this.amount.options = this.createSlider(valObj);
  }

  loadPurchaseYearSlider() {
    const valObj = {
      min: 2016,
      max: 2096,
      step: 1,
      ticks: [2016, 2056, 2096],
      ticksLabels: ['2016', '2096']
    };
    this.purchaseYear.options = this.createSlider(valObj);
  }

  updateSandBox(filterType, val, desc = '') {
    // angular.element('.tooltip.tooltip-main.top').addClass('in');
    switch (filterType) {
    case 'selectedGoal':
      this.sendSignal();
      this.selectedGoal = val;
      this.analytics.eventTrack('selected sandbox goal type', { category: 'Client - MVFP', label: 'goal type_sandbox - ' + val });
      break;
    case 'selectedInvestorProfile':
      this.selectedInvestorProfile = val;
      this.selectedInvestorProfileDesc = desc;
      this.analytics.eventTrack('adjusted sandbox', { category: 'Client - MVFP', label: 'lever type_sandbox - ' + val });
      break;
    default:
      this.scope.$broadcast('updateSandBoxChart', {});
      break;
    }
  }
}
