class ConfirmDelinkAccountController {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }

  YesDelinkAccount() {
    this.$uibModalInstance.close();
  }

  closeDelinkPopup() {
    this.$uibModalInstance.dismiss();
  }
}

export class EditGoalController {
  constructor($scope, moment, GoalsService, UserService, helpers, $uibModal) {
    'ngInject';

    this.$scope = $scope;
    this.moment = moment;
    this.GoalsService = GoalsService;
    this.UserService = UserService;
    this.helpers = helpers;
    this.$uibModal = $uibModal;

    this.isEditGoal = false;

    this.monthsDropdown = this.helpers.createArrayForSelect(1, 12);
    this.datesDropdown = this.helpers.createArrayForSelect(1, 31);
    this.yearsDropdown = this.helpers.createArrayForSelect(1950, this.moment().year());
    this.educationStartDropdown = this.helpers.createArrayForSelect(10, 50);
    this.educationYearsDropdown = this.helpers.createArrayForSelect(1, 10);

    this.$scope.$watch(() => {
      return this.GoalsService.currentGoalType;
    }, () => {
      this.changeGoal(this.GoalsService.currentGoalType);
    });
  }

  editGoal() {
    this.currentGoalType = this.GoalsService.currentGoalType;
    this.listGoals = this.GoalsService.createGoalsList();
    this.prefillGoals();
    this.isEditGoal = true;
  }

  closeEdit() {
    this.isEditGoal = false;
  }

  changeGoal(goalType) {
    this.currentGoalType = goalType;
    this.GoalsService.currentGoalType = goalType;
  }

  editDependent(dependent) {
    this.editDependentInfo = dependent;

    const dateOfBirth = this.moment(this.editDependentInfo.dateOfBirth);

    this.dobMonth = this.monthsDropdown.find((month) => {
      return month.value === dateOfBirth.month() + 1;
    });

    this.dobDate = this.datesDropdown.find((date) => {
      return date.value === dateOfBirth.date();
    });

    this.dobYear = this.yearsDropdown.find((year) => {
      return year.value === dateOfBirth.year();
    });

    this.educationStartAge = this.educationStartDropdown.find((year) => {
      return year.value === this.editDependentInfo.startAge;
    });

    this.educationNumberOfYears = this.educationYearsDropdown.find((year) => {
      return year.value === this.editDependentInfo.numberOfYears;
    });

    this.isEditDependent = true;
  }

  saveEditDependent() {
    let costPerYear = 0;

    this.editDependentInfo.dateOfBirth = this.moment(this.dobMonth.value + '-' + this.dobDate.value + '-' + this.dobYear.value, 'MM-DD-YYYY').valueOf();
    this.dependentEducationGoal.educationInfos.forEach((info) => {
      costPerYear = Number(costPerYear) + Number(info.costPerYear);
    });

    this.dependentEducationGoal.costPerYear = costPerYear;
    this.closeEditDependent();
  }

  closeEditDependent() {
    this.isEditDependent = false;
  }

  setEducationStartAge() {
    this.editDependentInfo.startAge = this.educationStartAge.value;
  }

  setEducationNumberOfYears() {
    this.editDependentInfo.numberOfYears = this.educationNumberOfYears.value;
  }

  updateGoal() {
    let params;

    if (this.currentGoalType === 'RETIREMENT') {
      params = angular.copy(this.retirementGoal);
    } else if (this.currentGoalType === 'EDUCATION_SELF') {
      params = angular.copy(this.selfEducationGoal);
    } else if (this.currentGoalType === 'EDUCATION_DEPENDENT') {
      params = angular.copy(this.dependentEducationGoal);
    } else if (this.currentGoalType === 'PURCHASE') {
      params = angular.copy(this.purchaseGoal);
    }

    delete params.accounts;

    this.GoalsService.updageGoal(params).then((response) => {
      response = response.plain();
      this.closeEdit();
    });
  }

  deleteGoal() {
    /*this.GoalsService.updageGoal(goal).then((response) => {
      // console.log(response);
    });*/
    this.closeEdit();
  }

  prefillGoals() {
    this.allGoals = this.GoalsService.allGoals;

    this.allGoals.forEach((goal) => {
      if (goal.type === 'RETIREMENT') {
        this.prefillRetirementGoal(goal);
      } else if (goal.type === 'EDUCATION') {
        this.prefillEducationGoal(goal);
      } else if (goal.type === 'PURCHASE') {
        this.prefillPurchaseGoal(goal);
      }
    });
  }

  prefillRetirementGoal(goal) {
    this.retirementGoal = goal;
    this.clientAge = this.moment().year() - this.moment(this.UserService.clientDetails.dob).year();
    this.isRetired = this.clientAge > this.retirementGoal.myself.retirementAge ? true : false;

    this.retirementSlider = this.createSlider({
      min: 50,
      max: 80,
      step: 1,
      ticks: [50, 80],
      ticksLabels: [50, 80]
    });

    this.lifeExpectancySlider = this.createSlider({
      min: this.clientAge,
      max: 120,
      step: 1,
      ticks: [this.clientAge, 120],
      ticksLabels: [this.clientAge, 120]
    });

    if (this.retirementGoal.spouse) {
      this.spouseAge = this.moment().year() - this.moment(this.UserService.clientDetails.spouse.dob).year();
      this.isSpouseRetired = this.spouseAge > this.retirementGoal.spouse.retirementAge ? true : false;

      this.spouseRetirementSlider = this.createSlider({
        min: 50,
        max: 80,
        step: 1,
        ticks: [50, 80],
        ticksLabels: [50, 80]
      });

      this.spouseLifeExpectancySlider = this.createSlider({
        min: this.spouseAge,
        max: 120,
        step: 1,
        ticks: [this.spouseAge, 120],
        ticksLabels: [this.spouseAge, 120]
      });
    }
  }

  prefillEducationGoal(goal) {
    if (goal.educationInfos[0].educationGoalType === 'SELF') {
      this.selfEducationGoal = goal;
    } else {
      this.dependentEducationGoal = goal;
    }
  }

  prefillPurchaseGoal(goal) {
    this.purchaseGoal = goal;
  }

  createSlider(valObj) {
    return {
      min: valObj.min,
      max: valObj.max,
      step: valObj.step,
      precision: 2,
      orientation: 'horizontal',
      handle: 'round',
      tooltip: 'show',
      tooltipseparator: ':',
      tooltipsplit: false,
      enabled: true,
      naturalarrowkeys: false,
      range: false,
      ngDisabled: false,
      reversed: false,
      ticks: valObj.ticks,
      ticksLabels: valObj.ticksLabels
    };
  }

  getToolTip(prefix, suffix, value, isMoneyRange = 0) {
    let tipVal;
    if (isMoneyRange) {
      if (value >= 1000) {
        tipVal = (value / 1000);
        tipVal = prefix + tipVal + suffix;
      } else {
        tipVal = prefix + value;
      }
    } else {
      tipVal = prefix + value + suffix;
    }

    return tipVal;
  }

  hasAccounts(goal) {
    return (goal && goal.accounts.length > 0) ? true : false;
  }

  confirmDelinkAccount(goalId, accountId) {
    let modalInstance = this.$uibModal.open({
      templateUrl: 'app/client/dashboard/views/_delink-account.html',
      controller: ConfirmDelinkAccountController,
      controllerAs: 'daCtrl',
      bindToController: true
    });

    modalInstance.result.then(() => {
      let params = {
        accountIds: [accountId]
      }

      this.GoalsService.delinkAccount(goalId, params).then((response) => {
        response = response.plain();
      });
    });
  }
}
