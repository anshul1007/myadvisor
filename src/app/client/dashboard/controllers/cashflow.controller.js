export class CashFlowController {
  constructor($scope, $interval, DashboardService, $timeout) {
    'ngInject';

    this.isLoading = true;
    this.isExpanded = false;
    this.timeout = $timeout;
    this.chartOptions = {};
    this.toggleData = true;

    $scope.loadCashFlowChart = () => this.loadCashFlowChart();

    $timeout(() => {
      this.loadCashFlowChart();
      this.isLoading = false;
    }, 100);

    $scope.$on('loadCashFlow', (event, args) => {
      $timeout(() => {
        this.isExpanded = false;
        this.loadCashFlowChart();
        this.isLoading = false;
      }, 100);
      this.isLoading = true;
      // if(!this.chartOptions.isReload) {
      //   this.chartOptions.isReload = true;
      // }
      // else {
      //   this.loadCashFlowChart();
      // }
    });

    $scope.$on('updateSandBoxChart', (event, args) => {
      this.changeData();
    });

    this.cashData = [{
      title: 'Inflows',
      amount: 174000,
      legendColor: 'green',
      items: [{
        title: 'Employment',
        amount: 150000,
        items: [{
          title: 'Client',
          amount: 50000
        }, {
          title: 'Spouse (if applicable)',
          amount: 100000
        }]
      }, {
        title: 'Other',
        amount: 24000,
        items: [{
          title: 'Other Income 1',
          amount: 20000
        }, {
          title: 'Other Income 3',
          amount: 4000
        }]
      }]
    }, {
      title: 'Outflows',
      amount: -7322,
      legendColor: 'brown',
      items: [{
        title: 'Mortgage',
        amount: 1350
      }, {
        title: 'Personal Care',
        amount: 966
      }, {
        title: 'Shopping',
        amount: 913
      }, {
        title: 'Healthcare',
        amount: 614
      }, {
        title: 'Online Services',
        amount: 563
      }, {
        title: 'Home Improvements',
        amount: 361
      }, {
        title: 'Restaurents',
        amount: 332
      }, {
        title: 'Bills &amp; Utilities',
        amount: 307
      }, {
        title: 'Groceries',
        amount: 279
      }, {
        title: 'Gifts',
        amount: 150
      }]
    }, {
      title: 'Savings',
      amount: 500,
      legendColor: 'orange'
    }];
  }

  legendsStroks() {
    angular.element('.highcharts-legend-item path').attr('stroke-width', '12');
  }

  loadCashFlowChart() {
    let retirementAge = 2015;

    let expenses = [{ age1: 45, age2: 43, x: 2010, y: 500 }, { age1: 46, age2: 44, x: 2011, y: 1000 }, { age1: 47, age2: 45, x: 2012, y: 1200 }, { age1: 48, age2: 46, x: 2013, y: 1350 }, { age1: 49, age2: 47, x: 2014, y: 1520 }, { age1: 50, age2: 48, x: 2015, y: 1775 }, { age1: 51, age2: 49, x: 2016, y: 3375 }];

    let income = [{ age1: 45, age2: 43, x: 2010, y: 800 }, { age1: 46, age2: 44, x: 2011, y: 1200 }, { age1: 47, age2: 45, x: 2012, y: 1400 }, { age1: 48, age2: 46, x: 2013, y: 1500 }, { age1: 49, age2: 47, x: 2014, y: 1700 }, { age1: 50, age2: 48, x: 2015, y: 1900 }, { age1: 51, age2: 49, x: 2016, y: 3500 }];

    let cashOnHand = [{ age1: 45, age2: 43, x: 2010, y: 1100 }, { age1: 46, age2: 44, x: 2011, y: 600 }, { age1: 47, age2: 45, x: 2012, y: 800 }, { age1: 48, age2: 46, x: 2013, y: 1200 }, { age1: 49, age2: 47, x: 2014, y: 1800 }, { age1: 50, age2: 48, x: 2015, y: 3200 }, { age1: 51, age2: 49, x: 2016, y: 3700 }];

    let savings = [{ age1: 45, age2: 43, x: 2010, y: 200 }, { age1: 46, age2: 44, x: 2011, y: 300 }, { age1: 47, age2: 45, x: 2012, y: 400 }, { age1: 48, age2: 46, x: 2013, y: 500 }, { age1: 49, age2: 47, x: 2014, y: 600 }, { age1: 50, age2: 48, x: 2015, y: 700 }, { age1: 51, age2: 49, x: 2016, y: 800 }];

    this.chartOptions = this.prepCashFlowChartOptions(retirementAge, expenses, income, cashOnHand, savings);
  }

  prepCashFlowChartOptions(retirementAge, expenses, income, cashOnHand) {
    return {
      isReload: false,
      colName: 'js-cash-flow-chart',
      chart: {
        backgroundColor: 'transparent',
        plotBackgroundColor: '#f5f5f5',
        height: '450',
        spacing: [17, 0, 10, 0],
        events: {
          load: () => {
            this.legendsStroks();
          },
          redraw: () => {
            this.legendsStroks();
          }
        }
      },

      credits: {
        enabled: false
      },

      title: {
        text: ''
      },

      exporting: {
        enabled: false
      },

      legend: {
        verticalAlign: 'top',
        itemDistance: 10,
        align: 'left',
        symbolWidth: 12,
        symbolHeight: 12,
        itemStyle: {
          fontSize: '12px',
          fontWeight: 'normal',
          color: '#1eb7fa'
        },
        x: 27
      },

      xAxis: {
        tickmarkPlacement: 'on',
        // labels: {
        //   formatter: function() {
        //     if (this.isFirst) {
        //       return 'Today';
        //     } else if (this.isLast) {
        //       return 2016;
        //     }
        //   },
        //   y: 20
        // },
        tickLength: 0,
        plotLines: [{
          color: '#dbdbdb',
          width: 1,
          value: retirementAge,
          zIndex: 5,
          dashStyle: 'solid',
          label: {
            text: 'retirement',
            align: 'right',
            x: -13,
            y: -5,
            verticalAlign: 'bottom',
            style: {
              color: '#ffffff'
            }
          }
        }, {
          color: '#dbdbdb',
          width: 1,
          value: retirementAge - 1,
          zIndex: 5,
          label: {
            text: 'Spouse\'s retirement',
            align: 'right',
            x: -13,
            y: -5,
            verticalAlign: 'bottom',
            style: {
              color: '#ffffff'
            }
          }
        }]
      },

      tooltip: {
        enabled: true,
        shared: true,
        crosshairs: {
          width: 2,
          color: '#00c7c6',
          dashStyle: 'solid'
        },
        useHTML: true,
        backgroundColor: '#fdfdfd',
        borderColor: '#fdfdfd',
        hideDelay: 0,
        style: {
          color: '#000000',
          fontWeight: 'normal',
          fontSize: '10px'
        },
        positioner: function(labelWidth, labelHeight, point) {
          let tooltipX;
          if (point.plotX + labelWidth > this.chart.plotWidth) {
            tooltipX = point.plotX + this.chart.plotLeft - labelWidth;
            tooltipX = tooltipX + (labelWidth / 2);
          } else {
            tooltipX = point.plotX + this.chart.plotLeft;
            tooltipX = tooltipX - (labelWidth / 2);
          }
          // tooltipY = point.plotY + this.chart.plotTop - 20;
          return {
            x: tooltipX,
            y: -160
          };
        },
        formatter: function() {
          let toolTipHtml = '<div class="tooltip-format">';
          let innerRows = '';
          let x = this.x;
          let age1 = this.points[0].point.age1;
          let age2 = this.points[0].point.age2;
          let series = this.points[0].point.series.chart.series;
          toolTipHtml = toolTipHtml + '<div><b>' + x + '</b></div><div class="age-col"><span>Your Age: ' + age1 + '</span>&nbsp;&nbsp;<span>Spouse\'s Age: ' + age2 + '</span></div><hr/>';
          angular.element.each(series, function(i, s) {
            angular.element.each(s.data, function(j, d) {
              if (d.x === x) {
                innerRows = innerRows + '<div class="detail-container"><div class="fl-left">' + s.name + ': </div><div class="fl-right">$' + d.y + '</div></div>';
              }
            });
          });
          innerRows = innerRows + '<div class="detail-container no-border"><div class="fl-left">Education Expense: </div><div class="fl-right">$5000</div></div>';
          toolTipHtml = toolTipHtml + innerRows + '</div>';

          return toolTipHtml;
        }
      },

      yAxis: {
        title: false,
        labels: {
          formatter: function() {
            return '$' + this.axis.defaultLabelFormatter.call(this);
          }
        }
      },

      plotOptions: {
        series: {
          pointPadding: 0,
          pointPlacement: null,
          states: {
            hover: {
              enabled: true,
              halo: {
                size: 0
              }
            }
          },
          events: {
            mouseOver: function() {
              angular.element('.expand-collapse-chart').css('z-index', '1');
            },
            mouseOut: function() {
              angular.element('.expand-collapse-chart').css('z-index', '2');
            }
          }
        }
      },

      lang: {
        noData: 'No Data to Display'
      },

      series: [{
        name: 'Expenses',
        type: 'column',
        fillColor: '#2b4460',
        legendIndex: 3,
        color: '#2b4460',
        lineWidth: 0,
        borderColor: '#2b4460',
        data: expenses
      }, {
        name: 'Income',
        type: 'column',
        fillColor: '#ede2cb',
        legendIndex: 2,
        color: '#ede2cb',
        lineWidth: 0,
        borderColor: '#ede2cb',
        data: income
      }, {
        name: 'Cash on Hand',
        type: 'line',
        color: '#f1c302',
        legendIndex: 1,
        marker: {
          symbol: 'circle',
          enabled: false,
          fillColor: '#00c7c6',
          lineColor: '#00c7c6',
          radius: 2
        },
        data: cashOnHand
      }
      // {
      //   name: 'Savings',
      //   shared: true,
      //   color: '#ff8e00',
      //   type: 'line',
      //   fillColor: '#ff8e00',
      //   legendIndex: 4,
      //   marker: {
      //     symbol: 'circle'
      //   },
      //   data: savings
      // }
      ]
    };
  }

  changeData() {
    if (this.toggleData) {
      this.chartOptions.series[0].data = [];
    } else {
      this.loadCashFlowChart();
    }

    this.toggleData = !this.toggleData;
  }

  expandCollapse() {
    this.isExpanded = !this.isExpanded;
  }
}
