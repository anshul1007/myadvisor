export class ClientConferenceController {
  constructor($scope, helpers, $timeout, $sessionStorage, $state, OTSession, OTService, $rootScope, MayDayService, $filter, AppointmentService, $uibModal) {
    'ngInject';

    this.helpers = helpers;
    this.$state = $state;
    this.$timeout = $timeout;
    this.$uibModal = $uibModal;
    this.OTService = OTService;
    this.$rootScope = $rootScope;
    this.OTService.OTSession = OTSession;
    this.$sessionStorage = $sessionStorage;
    this.isDesktopDevice = helpers.isDesktopDevice();
    this.maydayService = MayDayService;
    this.$filter = $filter;
    this.appointmentService = AppointmentService;

    this.isSessionStart = false;
    this.waitingRoomEnabled = false;
    this.isMeetingAccepted = false;
    this.isAdhocMeeting = false;
    this.elapsed = '00:00:00';
    this.isExpand = false;
    this.muteVideo = false;
    this.muteAudio = false;
    this.micOff = false;
    this.advisorName = 'an advisor';

    $scope.logoutMeeting = () => this.logoutMeeting();
    $scope.cancelAdHocCallSync = () => this.cancelAdHocCallSync();
    $scope.isConferenceStart = () => this.isConferenceStart();
    $scope.videoSessionLogout = () => this.videoSessionLogout();
    
    this.logoutMeeting();
    this.$scope = $scope;

    if(this.$sessionStorage.isCallFromAppointmentPage) {
      const appointmentObj = this.$sessionStorage.appointmentObj;
      if(appointmentObj === 'AD_HOC') {
        this.initSession(true);
      } else {
        this.initSession(false, appointmentObj.appointmentObj);
      }
      
      this.$sessionStorage.isCallFromAppointmentPage = false;
      this.$sessionStorage.appointmentObj = {};
    }
  }

  getSessionTypeList() {
    if (!this.OTService.isBrowserSupported() || !this.helpers.isDesktopDevice()) {
      this.sessionTypeList = ['Phone Call'];
    } else {
      this.sessionTypeList = ['Phone Call', 'Video Call'];
    }
    this.selectedSession = this.sessionTypeList[0];
    this.resetSessionTypeDetailsFilters();
    this.isPhoneCallRequested = true;
  }

  getReasonList() {
    this.reasonTypeList = this.appointmentService.getListOfAppointmentTypes().$object;
    this.selectedReason = this.reasonTypeList[0];
  }

  videoSessionLogout() {
    this.OTService.logoutSession(this.$scope);
  }

  logoutMeeting() {
    this.maydayService.doConferenceHangUp('client');
  }

  expandCollapseScreen() {
    this.maydayService.expandCollapseScreen(this);
  }

  resetSessionTypeDetailsFilters() {
    this.phoneCallScheduled = false;
    this.isCallSetup = false;
    this.isRequestDone = true;
    this.phoneNumber = '';
    this.getReasonList();
    this.isSessionStart = false;
    this.waitingRoomEnabled = false;
    this.isMeetingAccepted = false;
    this.phoneError = false;
  }

  updateDetailFilters(filterType, val) {
    switch (filterType) {
    case 'selectedSession':
      this.selectedSession = val;
      if (val === 'Phone Call') {
        this.isPhoneCallRequested = true;
      } else {
        this.isPhoneCallRequested = false;
      }
      this.resetSessionTypeDetailsFilters();
      break;
    case 'selectedReason':
      this.selectedReason = val;
      break;
    default:
      break;
    }
  }

  toggleVideoControls(isShow, duration = 0) {
    this.maydayService.toggleVideoControls(isShow, duration);
  }

  cancelAdHocCallReq() {
    this.isAdhocMeeting = false;
  }

  timeOutScheuledCallReq() {
    this.isSessionStart = false;
    this.waitingRoomEnabled = false;
    this.isMeetingAccepted = false;
    this.maydayService.cancelClientPolling();
    this.maydayService.setMeetingDeclined();
    this.maydayService.isWatchingPageChanged(false, this.$scope);
    this.maydayService.startStopCallTimer(false, this);
  }

  cancelAdHocCallSync() {
    this.isAdhocMeeting = false;
    this.isSessionStart = false;
    this.waitingRoomEnabled = false;
    this.isMeetingAccepted = false;
    this.maydayService.cancelClientPolling();
    this.maydayService.setMeetingDeclined();
    this.maydayService.isWatchingPageChanged(false, this.$scope);
    this.maydayService.startStopCallTimer(false, this);
  }

  getMeetingDetailsFromClient() {
    if (typeof this.phoneNumber === 'undefined') {
      this.phoneError = true;
      return false;
    }

    this.isRequestDone = false;
    if (this.selectedSession === 'Phone Call') {
      let data = {
        phoneNumber: this.$filter('phone')(this.phoneNumber),
        reasonId: (!this.selectedReason ? this.reasonTypeList[0].id : this.selectedReason.id)
      };
      this.maydayService.sendPhoneCallRequest(data);
      this.$timeout(() => {
        this.isRequestDone = true;
        this.phoneCallScheduled = true;
        this.isCallSetup = true;
        this.phoneNumber = this.$filter('phone')(this.phoneNumber);
      }, 1000);
    } else {
      this.isCallSetup = true;
      let data = {
        phoneNumber: '(111)-111-1111',
        reasonId: (!this.selectedReason ? this.reasonTypeList[0].id : this.selectedReason.id)
      };

      this.maydayService.isWatchingPageChanged(true, this.$scope);
      this.startClientVideoConf(data, 'AD_HOC');
    }
  }

  initAdHocClientCall() {
    this.getSessionTypeList();
    this.isAdhocMeeting = true;
  }

  initScheduledCall(appointmentObj) {
    this.advisorName = 'Jone Chan';
    this.maydayService.advisorObj.firstName = 'Jone';
    this.maydayService.advisorObj.lastName = 'Chan';
    this.maydayService.setAdvisorId('40288030556fb0f801556fb110210045');
    let data = {
      conferenceId: appointmentObj.id,
      advisorId: this.maydayService.getAdvisorId()
    };

    this.maydayService.isWatchingPageChanged(true, this.$scope);
    this.startClientVideoConf(data, 'SCHEDULED');
  }

  initConfFromAppointments(data) {
    this.$sessionStorage.startConference = data;
  }

  initConferenceSync() {
    this.maydayService.initClientMeetingSync(this);
  }

  startClientVideoConf(data, callType) {
    this.maydayService.setMeetingStarted(data, callType);
    this.initConferenceSync();
    this.maydayService.checkDeviceSupport(this);
  }

  initSession(meetingType, appointmentObj = {}) {
    if (meetingType) {
      this.maydayService.getAdvisorStatus(this).then((response) => {
        if(response.businessHours) {
          if (response.dedicatedFree) {
            this.initAdHocClientCall();
          } else if (!response.otherFree && !response.dedicatedFree) {
            this.isAdhocMeeting = false;
            const uibModalInstance = this.maydayService.openModelPopup('app/client/dashboard/views/_no-advisor-avail-modal.html', {}, this);
            uibModalInstance.result.then(() => {
              this.$state.go('client.appointment.schedule.session-type');
            }, () => {
              //TODO
            });
          } else if (response.otherFree && !response.dedicatedFree) {
            const uibModalInstance = this.maydayService.openModelPopup('app/client/dashboard/views/_other-advisor-avail-modal.html', {}, this);
            uibModalInstance.result.then(() => {
              this.initAdHocClientCall('an advisor');
            }, (data) => {
              if (data) {
                this.$state.go('client.appointment.schedule.session-type');
              }
            });
          }
        } else {
          this.isAdhocMeeting = false;
          let data = { message: response.businessHoursMessage };
          this.maydayService.openModelPopup('app/client/dashboard/views/_out-of-business-hours-modal.html', data, this);
        }
      });
    } else {
      this.isAdhocMeeting = false;
      if (this.helpers.isDesktopDevice()) {
        if (!this.OTService.isBrowserSupported()) {
          this.maydayService.openModelPopup('app/client/dashboard/views/_no-mayday-supported-browser-modal.html', {}, this);
        } else {
          this.initScheduledCall(appointmentObj);
        }
      } else {
        this.maydayService.openModelPopup('app/client/dashboard/views/_no-mayday-supported-devices-modal.html', {}, this);
      }
    }
  }

  startSession(data) {
    if (data) {
      this.OTService.initOTConnection(this.$scope, this.$rootScope);
    }
  }

  isSessionProgess() {
    return this.isSessionStart;
  }

  isConferenceStart() {
    return this.isSessionStart && this.waitingRoomEnabled && this.isMeetingAccepted && this.isDesktopDevice;
  }

  isWaitingRoomEnabled() {
    return this.isSessionStart && this.waitingRoomEnabled && !this.isMeetingAccepted;
  }

  isAdvisorDecline() {
    return this.isSessionStart && !this.waitingRoomEnabled && !this.isMeetingAccepted;
  }
}
