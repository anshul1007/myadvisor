export class GoalsService {
  constructor(Restangular) {
    'ngInject';
    this.goalsAPI = Restangular.all('client-manager');
    this.clientId = '40288030556fb0f801556fb110cf0001';
  }

  get currentGoalType() {
    return this.currentOpenGoal;
  }

  set currentGoalType(goalType) {
    this.currentOpenGoal = goalType;
  }

  createGoalsList() {
    let listGoals = [];

    this.allGoals.forEach((goal) => {
      let goalType = goal.type;

      if (goal.type === 'EDUCATION') {
        goalType = (goal.educationInfos[0].educationGoalType === 'SELF') ? 'EDUCATION_SELF' : 'EDUCATION_DEPENDENT';
      }

      listGoals.push({
        type: goalType,
        name: goal.type === 'RETIREMENT' ? 'Retirement Goal' : goal.name
      });
    });

    return listGoals;
  }

  getGoals(params) {
    return this.goalsAPI.all('goals').getList(params);
  }

  updageGoal(params) {
    return this.goalsAPI.one('goals', params.id).customPUT(params);
  }

  delinkAccount(goalId, params) {
    // return this.goalsAPI.all('goals').one(goalId, 'accounts').customDELETE(params);
  }

  get loggedInClientId() {
    return this.addGoalRequestObject.clientId;
  }

  set loggedInClientId(clientId) {
    this.addGoalRequestObject.clientId = clientId;
  }

  get goalType() {
    return this.addGoalReqObj.type;
  }
  set goalType(goalType) {
    this.addGoalRequestObject = {};
    this.addGoalReqObj.type = goalType;
  }

  get investmentObjective() {
    return this.addGoalReqObj.investmentObjective;
  }

  set investmentObjective(objective) {
    this.addGoalReqObj.investmentObjective = objective;
  }

  get investmentTimeframe() {
    return this.addGoalReqObj.investmentTimeframe;
  }
  set investmentTimeframe(timeFrame) {
    this.addGoalReqObj.investmentTimeframe = timeFrame;
  }

  get riskTolerance() {
    return this.addGoalReqObj.riskTolerance;
  }
  set riskTolerance(riskTolerance) {
    this.addGoalReqObj.riskTolerance = riskTolerance;
  }

  get amount() {
    return this.addGoalReqObj.amount;
  }
  set amount(amount) {
    this.addGoalReqObj.amount = amount;
  }

  get name() {
    return this.addGoalReqObj.name;
  }

  set name(name) {
    this.addGoalReqObj.name = name;
  }

  get purchaseType() {
    return this.addGoalReqObj.purchaseType;
  }
  set purchaseType(purchaseType) {
    this.addGoalReqObj.purchaseType = purchaseType;
  }

  get frequency() {
    return this.addGoalReqObj.frequency;
  }
  set frequency(frequency) {
    this.addGoalReqObj.frequency = frequency;
  }

  get importance() {
    return this.addGoalReqObj.importance;
  }
  set importance(importance) {
    this.addGoalReqObj.importance = importance;
  }

  get startingAge() {
    return this.addGoalReqObj.startingAge;
  }
  set startingAge(startingAge) {
    this.addGoalReqObj.startingAge = startingAge;
  }

  get addGoalRequestObject() {
    return this.addGoalReqObj;
  }
  set addGoalRequestObject(addGoalReqObj) {
    this.addGoalReqObj = addGoalReqObj;
  }

  get endingAge() {
    return this.addGoalReqObj.endingAge;
  }
  set endingAge(endingAge) {
    this.addGoalReqObj.endingAge = endingAge;
  }
  get startingAt() {
    return this.addGoalReqObj.startingAt;
  }
  set startingAt(startingAt) {
    this.addGoalReqObj.startingAt = startingAt;
  }
  get endingAt() {
    return this.addGoalReqObj.endingAt;
  }
  set endingAt(endingAt) {
    this.addGoalReqObj.endingAt = endingAt;
  }
  get numberOfYears() {
    return this.addGoalReqObj.numberOfYears;
  }
  set numberOfYears(numberOfYears) {
    this.addGoalReqObj.numberOfYears = numberOfYears;
  }
  get planToFinance() {
    return this.addGoalReqObj.planToFinance;
  }
  set planToFinance(planToFinance) {
    this.addGoalReqObj.planToFinance = planToFinance;
  }

  /**
   * @URI : POST /client-manager/goals
   * Creates the new goal for the given client
   * @Todo : investmentTimeframe and startingAge is not coming from selection
   */
  createMajorPurchaseGoal() {
    this.startingAge = 50;
    this.loggedInClientId = this.clientId;
    return this.goalsAPI.one('goals').customPOST(this.addGoalReqObj);
  }
}
