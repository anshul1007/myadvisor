export class DashboardService {
  constructor(Restangular, $q, helpers, $interval) {
    'ngInject';
    this.interval = $interval;
    this.helpers = helpers;
    this.restangular = Restangular;
    this.dashboardRest = Restangular.one('onboarding');
    this.q = $q;
    this.clientManagerRoute = Restangular.one('client-manager');

    this.clientCallerId = 999999999;
    this.clientCardNumber = 4519031436276282;

  }

  getDataToVisualize() {
    let deferred = this.q.defer();
    let testData = [];

    deferred.resolve(testData);
    // return deferred.promise;
    // return this.dashboardRest.all('dashboard').get('data'); // TODO mock api call here
  }

  get clientCardNumber() {
    return this.clientCard;
  }

  set clientCardNumber(clientCard) {
    this.clientCard = clientCard;
  }

  get clientCallerId() {
    return this.callerId;
  }

  set clientCallerId(callerId) {
      this.callerId = callerId;
    }
    /**
     * URL GET /client-manager/accounts
     * @params : ClientCardNumber, CallerId
     * 
     */
  getNetWorthData() {
    const headers = { ClientCardNumber: this.clientCardNumber, CallerId: this.clientCallerId };
    this.restangular.setDefaultHeaders(headers);
    return this.clientManagerRoute.all('accounts').one('summary').customGET();
  }
}
