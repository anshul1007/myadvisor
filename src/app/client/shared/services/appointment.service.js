/*
 * req : /calendar/reasons
 *  2) Get latest available appointments
 *    req : /calendar/advisors/{advisorId}
 *  3)
 */
export class AppointmentService {
  constructor(Restangular, Session, _, moment) {
    'ngInject';
    this.moment = moment;
    //stating calendar route
    this.calendarRoute = Restangular.one('calendar');
    this.session = Session;
    this.localTimezone = 'UTC' + this.moment.tz(this.moment(), this.moment.tz.guess()).format('Z');
    this._ = _;
    this.advisorObject = {
      id: '40288030556fb0f801556fb110210000'
    };
    this.loggedInClientId = '40288030556fb0f801556fb110cf0001';
  }
  get modeOfAppointment() {
    if (angular.isDefined(this.session.request.appointmentMode)) {
      this.appointmentMode = this.session.request.appointmentMode;
    }
    return this.appointmentMode;
  }
  set modeOfAppointment(appointmentMode) {
    this.appointmentMode = appointmentMode;
    this.session.request.appointmentMode = appointmentMode;
  }
  get advisorObject() {
    if (angular.isDefined(this.session.request.advisorObject) && !angular.equals({}, this.session.request.advisorObject)) {
      this.advisor = this.session.request.advisorObject;
    }
    return this.advisor;
  }
  set advisorObject(advisor) {
    this.advisor = advisor;
    this.session.request.advisorObject = advisor;
  }
  get appointmentTypeId() {
    if (angular.isDefined(this.session.request.appointmentTypeId) && !angular.equals({}, this.session.request.appointmentTypeId)) {
      this.typeId = this.session.request.appointmentTypeId;
    }
    return this.typeId;
  }
  set appointmentTypeId(id) {
    this.typeId = id;
    this.session.request.appointmentTypeId = id;
  }
  get loggedInClientId() {
    if (angular.isDefined(this.session.request.clientId)) {
      this.clientId = this.session.request.clientId;
    }
    return this.clientId;
  }
  set loggedInClientId(clientId) {
    this.clientId = clientId;
    this.session.request.clientId = clientId;
  }
  get dayPartName() {
    if (angular.isDefined(this.session.request.dayPart)) {
      this.dayPart = this.session.request.dayPart;
    }
    return this.dayPart;
  }
  set dayPartName(dayPart) {
    this.dayPart = dayPart;
    this.session.request.dayPart = dayPart;
  }
  get localTimezone() {
    if (angular.isDefined(this.session.request.localTz)) {
      this.localTz = this.session.request.localTz;
    }
    return this.localTz;
  }
  set localTimezone(localTimezone) {
    this.localTz = localTimezone;
    this.session.request.localTz = localTimezone;
  }
  get appointmentDate() {
    if (angular.isDefined(this.session.request.appointmentDateUTC)) {
      this.appointmentDateUTC = this.session.request.appointmentDateUTC;
    }
    return this.appointmentDateUTC;
  }
  set appointmentDate(date) {
    this.appointmentDateUTC = date;
    this.session.request.appointmentDateUTC = date;
  }

  get appointmentStartTime() {
    if (angular.isDefined(this.session.request.timeStart)) {
      this.timeStart = this.session.request.timeStart;
    }
    return this.timeStart;
  }
  set appointmentStartTime(timeStart) {
    this.timeStart = timeStart;
    this.session.request.timeStart = timeStart;
  }

  get appointmentEndTime() {
    if (angular.isDefined(this.session.request.timeEnd)) {
      this.timeEnd = this.session.request.timeEnd;
    }
    return this.timeEnd;
  }
  set appointmentEndTime(timeEnd) {
    this.timeEnd = timeEnd;
    this.session.request.timeEnd = timeEnd;
  }
  get clientCallbackNumber() {
    if (angular.isDefined(this.session.request.callbackNumber)) {
      this.callbackNumber = this.session.request.callbackNumber;
    }
    return this.callbackNumber;
  }
  set clientCallbackNumber(callbackNumber) {
    this.callbackNumber = callbackNumber;
    this.session.request.callbackNumber = callbackNumber;
  }

  /*
   * Get available reasons of appointments slots
   * @req : GET /calendar/reasons
   * @params : none
   */
  getListOfAppointmentTypes() {
    return this.calendarRoute.all('reasons').getList();
  }

  /*
   * 1 Get latest available appointments slots
   *  @req : GET /calendar/advisors/{advisorId}
   *  @params : timezone (UTC)
   *      : timeSlotType (id of the selected reason of appointment)
   */
  getAvailableTimesByDayParts(advisorId, requestObject) {
    return this.calendarRoute.all('advisors').one(advisorId).customGET('available', requestObject);
  }

  /*
   * 2 Get available appointments slots by month
   *  @req : GET /calendar/advisors/{advisorId}
   *  @params : timezone (UTC)
   *      : timeSlotType (id of the selected reason of appointment)
   *      : from - Start timestamp
   *      : to - End timestamp
   *      : advisorId - Advisor ID
   */
  getAvailableAppointmentsByMonth(params) {
    const advisorId = this.advisorObject.id;
    return this.calendarRoute.all('advisors').one(advisorId).customGET('available', params);
  }

  /*
   * Create new appointment
   * @req : POST /calendar/appointments
   * @params : clientId (id of client)
   *     : advisorId (id of advisor)
   *     : timeSlotType (id of the selected reason of appointment)
   *     : timeStart (timestamp)
   *     : timeEnd  (timestamp)
   */
  createAppointment() {
    //const params = { callbackNumber: this.clientCallbackNumber, clientId: this.loggedInClientId, advisorId: this.advisorObject.id, appointmentReasonId: this.appointmentTypeId, startDate: this.appointmentStartTime, endDate: this.appointmentEndTime };
    const params = { callbackNumber: this.clientCallbackNumber, clientId: this.loggedInClientId, advisorId: this.advisorObject.id, communicationPreference: this.modeOfAppointment, appointmentReasonId: this.appointmentTypeId, startDate: this.appointmentStartTime, endDate: this.appointmentEndTime, subject: 'test', description: 'test' };
    return this.calendarRoute.one('appointments').customPOST(params);
  }

  /*
   * Retrieve Client calendar
   * @req : URL GET  /calendar/clients/{clientId}
   * @params : clientId : Client ID
   *           startDate : Calendar interval start date
   *           endDate : Calendar interval end date
   */

  retrieveClientCalendar(params) {
    const clientId = this.loggedInClientId;
    return this.calendarRoute.all('clients').customGET(clientId, params);
  }

  /*
   * Delete an appointment
   * @req : DELETE  /calendar/appointments/{appointmentId}
   * @params : advisorId (id of advisor)
   *      : clientId (id of client)
   *
   */
  deleteAppointment(appointmentId) {
    const params = { actionInitiator: 'CLIENT' };
    return this.calendarRoute.one('appointments').customDELETE(appointmentId, params);
  }
}
