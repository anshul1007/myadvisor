export class GoalTypesController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
    this.showCurrentView = ($state.current.name === 'add-goal')?false:true;
  }
  goNext(goalType) {
    this.showCurrentView = true;
    this.goalsService.goalType = goalType;
    if (goalType === 'EDUCATION') {
      this.$state.go('add-goal.education-goal');
    } else if (goalType === 'PURCHASE') {
      this.$state.go('add-goal.major-expense-goal');
    }
  }
}
