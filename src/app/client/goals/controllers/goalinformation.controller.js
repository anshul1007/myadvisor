export class GoalInformationController {
  constructor($state, $scope, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.$scope = $scope;
    this.goalsService = GoalsService;
    this.timeframeObj = [
      { name: 'With in 2 years', value: 'WITHIN_2_YEARS' },
      { name: 'From 2 to 4 years', value: 'FROM_2_TO_4_YEARS' },
      { name: 'From 5 to 9 years', value: 'FROM_5_TO_9_YEARS' },
      { name: 'More than 9 years', value: 'MORE_THAN_9_YEARS' }
    ];
  }
  updateSelection(goalImportance) {
    angular.forEach(this.goalImportanceObj, function(importance) {
      importance.checked = false;
    });
    goalImportance.checked = true;
  }
  goNext() {
    if (this.$scope.goalInformation.$valid) {
      this.goalsService.name = this.name;
      this.goalsService.amount = this.amount;
      this.goalsService.planToFinance = this.planToFinance;
      this.goalsService.investmentTimeframe = this.investmentTimeframe;
      this.$state.go('add-goal.major-expense-goal.goal-confirmation');
    }
  }
}
