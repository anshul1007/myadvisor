export class InvestorProfileController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
  }
  goNext(investorprofile) {
    this.$state.go('add-goal.major-expense-goal.risk-tolerance');
  }
}
