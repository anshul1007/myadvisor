export class GoalConfirmationController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
  }
  getGoalData(){
    return this.goalsService.addGoalRequestObject;
  }
  goNext() {
    this.showSpinner = true;
    this.goalsService.createMajorPurchaseGoal().then((response) => {
      if (response.id) {
        this.showSpinner = false;
        this.$state.go('client.dashboard');
      }
    });
  }
}
