export class RiskToleranceController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
    this.riskToleranceObject = [
      { name: 'Very Low', value: 'VERY_LOW' },
      { name: 'Low', value: 'LOW' },
      { name: 'Below Average', value: 'BELOW_AVERAGE' },
      { name: 'Average', value: 'AVERAGE' }
    ];
  }
  goNext(riskTolerance) {
    this.goalsService.riskTolerance = riskTolerance;
    this.$state.go('add-goal.major-expense-goal.investment-objective');
  }
}
