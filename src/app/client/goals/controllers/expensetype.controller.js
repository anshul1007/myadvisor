export class ExpenseTypeController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
  }
  goNext(purchaseType) {
    this.goalsService.purchaseType = purchaseType;
    this.$state.go('add-goal.major-expense-goal.expense-investor-profile');
  }
}
