export class GoalImportanceController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
    this.goalImportanceObj = [{
      label: 'Need to have',
      value: 'NEED_TO_HAVE',
      checked: true
    }, {
      label: 'Nice to have',
      value: 'NICE_TO_HAVE',
      checked: false
    }, {
      label: 'Aspirational',
      value: 'ASPIRATIONAL',
      checked: false
    }];
  }
  updateSelection(goalImportance){
  	angular.forEach(this.goalImportanceObj, function(importance) {
            importance.checked = false;
        });
        goalImportance.checked = true;
  }
  getSelectedValue(){
    let goalImportance;
  	angular.forEach(this.goalImportanceObj, function(importance) {
            if(importance.checked){
              goalImportance = importance.value;
              return 0;
            }
        });
    return goalImportance;
  }
  goNext() {
    this.goalsService.importance = this.getSelectedValue();
    this.$state.go('add-goal.major-expense-goal.goal-information');
  }
}
