export class InvestorObjectiveController {
  constructor($state, GoalsService) {
    'ngInject';
    this.$state = $state;
    this.goalsService = GoalsService;
  }
  goNext(investmentObjective) {
    this.goalsService.investmentObjective = investmentObjective;
    this.$state.go('add-goal.major-expense-goal.select-account');
  }
}
