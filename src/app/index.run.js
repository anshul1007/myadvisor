export function runConfig($rootScope, stringConstants, $log, envConfig, helpers, Idle) {
  'ngInject';
  let stateChange = $rootScope.$on('$stateChangeSuccess', function (event, current, previous) {
    $rootScope.title = current.title || stringConstants.common.pageTitle;
    $rootScope.description = current.description || stringConstants.common.description;
    $rootScope.keywords = current.keywords || stringConstants.common.keywords;
  });
  // start watching when the app runs. also starts the Keepalive service by default.
  Idle.watch();
  $rootScope.ensightenDevUrl = helpers.trustSrc(envConfig.ensightenDevUrl);
  $rootScope.$on('$destroy', stateChange);

  let gaRbcDomains = ['royalbank.com', 'rbcadvicecentre.com', 'rbc.com', 'rbcroyalbank.com', 'rbcrewards.com'];
  (function(i, s, o, g, r, a, m) {
    i.GoogleAnalyticsObject=r;
    i[r]=i[r]||function() {
      (i[r].q=i[r].q||[]).push(arguments);
    };
    i[r].l=1*new Date();
    a=s.createElement(o);
    m=s.getElementsByTagName(o)[0];
    a.async=1;
    a.src=g;
    m.parentNode.insertBefore(a, m);
  })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-76420157-1', 'royalbank.com', {allowLinker: true });

  // load the cross-domain linker plugin
  ga('require', 'linker');

  // set the domains defined in the previously declared gaRbcDomains array to autoLink
  ga('linker:autoLink', gaRbcDomains, false, true);
}
