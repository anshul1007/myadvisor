export class AdvisorController {
  constructor(stringConstants, $scope, $state, helpers) {
    'ngInject';

    this.stringConstants = stringConstants;
    this.isNavCollapsed = true;
    this.helpers = helpers;
    this.$state = $state;
    $scope.currentStatus = 'Available';
    $scope.getAutomationId = (elmName, elmType, elmIndex) => this.getAutomationId(elmName, elmType, elmIndex);
    this.toggleNavigation = function() {
      this.isNavCollapsed = !this.isNavCollapsed;
    };
    $scope.getStatusList = () => this.getStatusList();
    $scope.changeStatus = (status) => this.changeStatus(status);
    
    this.$scope = $scope;
  }
  
  getAutomationId(elmName, elmType, elmIndex = null) {
    const elementObject = {
      elementState: this.$state.current.name,
      elementName: elmName,
      elementType: elmType,
      elementIndex: elmIndex
    };
    return this.helpers.generateAutomationID(elementObject);
  }

  getStatusList() {
    const statusList = [
      'Available',
      'Pre Call Prep',
      'Appt - Video',
      'Appt - Phone',
      'Ad hoc - Video',
      'Ad hoc - Callback',
      'Ad hoc - 1800 Phone',
      'Unavailable - General',
      'Email work',
      'Break', 
      'Lunch',
      'Coaching/mentorship',
      'Team Meeting',
      'After Call Work',
      'Offline'
    ];
    return statusList;
  }

  changeStatus(status) {
    angular.element(document).trigger('click');
    this.$scope.currentStatus = status;
  }
}
