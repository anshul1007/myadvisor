export class ConferenceController {
  constructor($scope, helpers, $timeout, $state, OTSession, OTService, $rootScope, $uibModal, MayDayService) {
    'ngInject';

    this.helpers = helpers;
    this.$state = $state;
    this.$timeout = $timeout;
    this.OTService = OTService;
    this.$rootScope = $rootScope;
    this.OTService.OTSession = OTSession;
    this.isDesktopDevice = helpers.isDesktopDevice();
    this.$uibModal = $uibModal;
    this.maydayService = MayDayService;

    this.isSessionStart = false;
    this.waitingRoomEnabled = false;
    this.isMeetingAccepted = false;
    this.elapsed = '00:00:00';
    this.isExpand = false;
    this.muteVideo = false;
    this.muteAudio = false;
    this.micOff = false;
    this.pollCtr = true;
    this.isPhoneModalOpened = false;
    this.isCancelModalOpened = false;
    this.isUpcomingAppointment = false;
    this.upcomingAppointmentId = null;
    this.advisorName = 'an advisor';

    $scope.logoutMeeting = () => this.logoutMeeting();
    $scope.isConferenceStart = () => this.isConferenceStart();
    $scope.videoSessionLogout = () => this.videoSessionLogout();

    this.initConferenceSync();
    this.$scope = $scope;
  }

  callRequestDismiss() {
    this.appointmentObj = false;
    angular.element('.js-conf-toast-container').fadeOut();
    this.isUpcomingAppointment = false;
  }

  videoDismiss() {
    this.pollCtr = true;
    angular.element('.js-conf-toast-container').fadeOut();
    this.maydayService.setMeetingDismissed();
    this.elapsed = '00:00:00';
    this.maydayService.startStopCallTimer(false, this);
  }

  videoAccept() {
    this.elapsed = '00:00:00';
    angular.element('.js-conf-toast-container').fadeOut();
    this.maydayService.checkDeviceSupport(this);

    this.maydayService.getAdvisorOTTokens().then((responseObj) => {
      // this.$state.go('advisor.dashboard');
      this.startSession(true, true);
      this.waitingRoomEnabled = true;
      this.isMeetingAccepted = true;
      this.pollCtr = true;
      this.maydayService.isWatchingPageChanged(true, this.$scope);
      this.$timeout(() => {
        this.toggleVideoControls(false);
      }, 2000);
    });
  }

  openModelPopup(templateUrl, data = {}) {
    return this.maydayService.openModelPopup(templateUrl, data, this);
  }

  setMeetingCanceled(uibModalInstance) {
    if (uibModalInstance) {
      uibModalInstance.dismiss();
    }
    this.elapsed = '00:00:00';
    angular.element('.js-conf-toast-container').fadeOut();
    this.maydayService.startStopCallTimer(false, this);
  }

  initConferenceSync() {
    this.logoutMeeting();
    this.maydayService.initAdvisorMeetingSync(this);
  }

  logoutMeeting() {
    this.maydayService.doConferenceHangUp('advisor');
  }

  toggleVideoControls(isShow, duration = 0) {
    this.maydayService.toggleVideoControls(isShow, duration);
  }

  videoSessionLogout() {
    this.OTService.logoutSession(this.$scope);
  }

  muteAudioControl() {
    angular.element('ot-subscriber button.OT_edge-bar-item.OT_mute').click();
    this.muteAudio = !this.muteAudio;
  }

  expandCollapseScreen() {
    this.maydayService.expandCollapseScreen(this);
  }

  startSession(data) {
    if (data) {
      this.OTService.initOTConnection(this.$scope, this.$rootScope);
    }
  }

  isSessionProgess() {
    return this.isSessionStart;
  }

  isConferenceStart() {
    return this.isSessionStart && this.waitingRoomEnabled && this.isMeetingAccepted && this.isDesktopDevice;
  }

  isWaitingRoomEnabled() {
    return this.isSessionStart && this.waitingRoomEnabled && !this.isMeetingAccepted;
  }

  isAdvisorDecline() {
    return this.isSessionStart && !this.waitingRoomEnabled && !this.isMeetingAccepted;
  }
}
