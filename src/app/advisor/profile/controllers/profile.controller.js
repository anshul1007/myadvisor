export class AdvisorProfileController {
  constructor($analytics) {
    'ngInject';
    this.analytics = $analytics;
    this.bioContent = 'Catherine O’Hara is an American author, financial advisor, motivational speaker, and television host. O’Hara was born in Chicago and pursued a degree in social work. She worked as a financial advisor for Merrill Lynch. In 1983 she became the vice-president of investments at Prudential Bache Securities. In 1987, she joined Royal Bank of Canada.';
    this.contentLength = this.bioContent.length;

    this.analytics.eventTrack('viewed advisor profile', { category: 'Advisor - Profile', label: '' });
  }
  get bio() {
    return this.bioContent;
  }
  set bio(bioContent) {
    this.bioContent = bioContent;
    this.analytics.eventTrack('edited advisor bio', { category: 'Advisor - Profile', label: '' });
  }

  updatedContent (data) {
    this.contentLength = data.length;
    return this.contentLength;
  }
}
