export function routerConfig(stringConstants, $stateProvider, $urlRouterProvider, $locationProvider) {
  'ngInject';
  $stateProvider
    .state('advisor', {
      title: stringConstants.advisor.pageTitle,
      description: stringConstants.advisor.description,
      keywords: stringConstants.advisor.keywords,
      url: '/advisor',
      templateUrl: 'app/advisor/main/views/index.html',
      controller: 'AdvisorController',
      controllerAs: 'AdvisorCtrl'
    })
    .state('advisor.dashboard', {
      title: stringConstants.advisor.dashboard.pageTitle,
      description: stringConstants.advisor.dashboard.description,
      keywords: stringConstants.advisor.dashboard.keywords,
      url: '/dashboard',
      templateUrl: 'app/advisor/dashboard/views/dashboard.html',
      controller: 'AdvisorDashboardController',
      controllerAs: 'DashboardCtrl'
    })
    .state('advisor.performance', {
      title: stringConstants.advisor.performance.pageTitle,
      description: stringConstants.advisor.performance.description,
      keywords: stringConstants.advisor.performance.keywords,
      url: '/performance',
      templateUrl: 'app/advisor/performance/views/performance.html',
      controller: 'AdvisorDashboardController',
      controllerAs: 'DashboardCtrl'
    })
    .state('advisor.notifications', {
      title: stringConstants.advisor.notifications.pageTitle,
      description: stringConstants.advisor.notifications.description,
      keywords: stringConstants.advisor.notifications.keywords,
      url: '/notifications',
      templateUrl: 'app/advisor/notifications/views/notifications.html',
      controller: 'AdvisorDashboardController',
      controllerAs: 'DashboardCtrl'
    })
    .state('advisor.clientbook', {
      title: stringConstants.advisor.clientbook.pageTitle,
      description: stringConstants.advisor.clientbook.description,
      keywords: stringConstants.advisor.clientbook.keywords,
      url: '/clientbook',
      templateUrl: 'app/advisor/clientbook/views/clientbook.html'
    })
    .state('advisor.clientbook.information', {
      title: stringConstants.advisor.clientbook.pageTitle,
      description: stringConstants.advisor.clientbook.description,
      keywords: stringConstants.advisor.clientbook.keywords,
      url: '/information',
      templateUrl: 'app/advisor/clientbook/views/_client-information.html',
      controller: 'AdvisorClientBookController',
      controllerAs: 'clientBookController'
    })
    .state('advisor.clientbook.search', {
      title: stringConstants.advisor.clientbook.pageTitle,
      description: stringConstants.advisor.clientbook.description,
      keywords: stringConstants.advisor.clientbook.keywords,
      url: '/search',
      templateUrl: 'app/advisor/clientbook/views/_search.html',
      controller: 'SearchController',
      controllerAs: 'searchCtrl'
    })
    .state('advisor.profile', {
      title: stringConstants.advisor.profile.pageTitle,
      description: stringConstants.advisor.profile.description,
      keywords: stringConstants.advisor.profile.keywords,
      url: '/profile',
      templateUrl: 'app/advisor/profile/views/profile.html',
      controller: 'AdvisorProfileController',
      controllerAs: 'profileCtrl'
    });

  $locationProvider.html5Mode(true);
}
