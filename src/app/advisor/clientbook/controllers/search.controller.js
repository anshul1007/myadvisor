export class SearchController {
  constructor($analytics, helpers) {
      'ngInject';
      this.tabs = [{ title: 'My Clients', active: true }, {
        title: 'All Clients',
        active: false
      }];
      this.helpers = helpers;
      this.analytics = $analytics;
      //@Todo My Clients mocked object 
      this.myClients = [
        { fname: 'Aitken', lname: 'Max', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Antonucci', lname: 'Danny', address: '1235 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Beaton', lname: 'Kate', address: '1236 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 11, 1971' },
        { fname: 'Black', lname: 'Conrad', address: '1237 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 12, 1972' },
        { fname: 'Campeau', lname: 'Robert', address: '1238 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 13, 1973' },
        { fname: 'Cooke', lname: 'Jack Kent', address: '1239 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 14, 1974' },
        { fname: 'Dom', lname: 'Nick', address: '12310 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 15, 1975' },
        { fname: 'David', lname: 'Kent', address: '12311 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 16, 1976' }
      ];
      //@Todo All Clients mocked object
      this.allClients = [
        { fname: 'Aitken', lname: 'Max', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Antonucci', lname: 'Danny', address: '1235 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Alok', lname: 'Jain', address: '1245 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Anil', lname: 'Agarwal', address: '1246 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Beaton', lname: 'Kate', address: '1236 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 11, 1971' },
        { fname: 'Black', lname: 'Conrad', address: '1227 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 12, 1972' },
        { fname: 'Bacchus', lname: 'Make', address: '1247 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 12, 1972' },
        { fname: 'Bahman', lname: 'Jack', address: '1267 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 12, 1972' },
        { fname: 'Bronfman', lname: 'Samuel', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Campeau', lname: 'Robert', address: '1238 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 13, 1973' },
        { fname: 'Cade', lname: 'Beat', address: '1258 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 13, 1973' },
        { fname: 'Cooke', lname: 'Jack Kent', address: '1239 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 14, 1974' },
        { fname: 'Cunard', lname: 'Samuel', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'Dom', lname: 'Nick', address: '12310 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 15, 1975' },
        { fname: 'Daewon', lname: 'Famous', address: '12310 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 15, 1975' },
        { fname: 'David', lname: 'Kent', address: '12311 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 16, 1976' },
        { fname: 'Kassam', lname: 'Moe', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' },
        { fname: 'McLaughlin', lname: 'Samuel', address: '1234 Sesame St, Toronto, ON M5J 2J5, Canada', dob: 'May 10, 1970' }
      ];
      this.analytics.eventTrack('launched clientbook', { category: 'Advisor - Search', label: '' });
    }
    /**
     * This method is responsible to add full name property in client json respose
     * @params clientObject: api json response
     * @return array of objects
     */
  clientsWithFullName(clientObject) {
      let newObject = [];
      for (let client of clientObject) {
        client.fullName = client.fname + ' ' + client.lname;
        newObject.push(client);
      }
      return newObject;
    }
    /**
     * This method is responsible for splitting of given string as per first character 
     * @inputString : String
     * @return: first letter of string
     */
  firstLetterOfString(inputString) {
    return this.helpers.firstLetterOfString(inputString);
  }

  /**
   * This method is reponsilbe for switching object list over tabs
   */
  getSelectedTabObject(title) {
    const clientObject = (title === 'My Clients') ? this.clientsWithFullName(this.myClients) : this.clientsWithFullName(this.allClients);
    return clientObject;
  }
}
