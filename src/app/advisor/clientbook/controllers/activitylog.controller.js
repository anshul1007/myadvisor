export class ActivityLogController {
  constructor($analytics, Restangular, ClientBookService, moment) {
    'ngInject';

    this.analytics = $analytics;
    this.pageNumbers = 5;
    this.pageSize = 4;
    this.currentPage = 1;
    this.ClientBookService = ClientBookService;
    this.moment = moment;
    this.getEmailLogDetails();
    this.totalPages = Math.ceil(this.totalLogs / this.pageSize);
  }
  getEmailLogDetails() {
    this.ClientBookService.getEmailLog().then((response) => {
       this.allLogs = response.log; 
       this.totalLogs = this.allLogs.length;
       this.logs = this.getLogs();
       this.convertDate();
    });
  }

  toggleClass($event, className) {
    angular.element($event.currentTarget).addClass(className);
  }

  convertDate() {
    for(let i = 0; i < this.allLogs.length; i++){
       this.allLogs[i].createdDate = this.moment(this.allLogs[i].createdDate).format("LL");
    }
  }

  pageChanged() {
    this.logs = this.getLogs();
  }

  getLogs() {
    const start = (this.currentPage - 1) * this.pageSize;
    const end = (this.currentPage) * this.pageSize;
    this.analytics.eventTrack('viewed activity log', { category: 'Advisor - Tools', label: '' });
    return this.allLogs.slice(start, end);
  }
}
