export class AdvisorAppointmentsController {
  constructor(stringConstants, moment, $analytics) {
    'ngInject';
    this.stringConstants = stringConstants;
    this.moment = moment;
    this.name = 'AdvisorAppointmentsController';
    this.appointmentTime = this.moment().format('dddd, MMMM Do YYYY, h:mm:ss a');
    this.agendaContentLength = 0;
    this.recommendationsContentLength = 0;
    this.reason = "fc";
    this.haveUpComingAppointments = true;
    this.showAgenda = true;
    this.showStatus = true;
    this.showAppointmentRecommendation = true;
    this.isAdHocDisabled = false;
    this.analytics = $analytics;

    this.statuses = [
      { id: 'open', name: 'Open' },
      { id: 'completed', name: 'Completed' },
      { id: 'missedbyclient', name: 'Missed by Client' },
      { id: 'missedbyadvisor', name: 'Missed by Advisor' }
    ];
    this.selectedStatus = this.statuses[0];
    this.reasons = [
      { id: 'fc', text: 'Create my financial picture' },
      { id: 'umofpag', text: 'Review my financial picture' },
      { id: 'oania', text: 'Open a new investment account' },
      { id: 'mani', text: 'Make a contribution' },
      { id: 'Other', text: 'Other' }
    ];
    this.recommendations = [{
      open: true
    }];
    this.archives = [{
      open: true,
      time: 'Yesterday at 9:00am / 25 min call',
      reason: 'Set a retirement goal plan for Marcus Alonso.',
      agenda: 'Retirement Goal 1\n Discussion point 2\n Discussion point 3',
      recommendations: 'Next step 1\n Next step 2\n Next step 3\n Next step 4',
      state: 'Open'
    }, {
      open: false,
      time: 'May 07, 2016 at 9:00am',
      reason: 'Set a retirement goal plan for Marcus Alonso.',
      agenda: 'Retirement Goal 1\n Discussion point 2\n Discussion point 3',
      recommendations: 'Next step 1\n Next step 2\n Next step 3\n Next step 4',
      state: 'Missed by Client'
    }];
  }
  onPopUpClose() {
    this.haveUpComingAppointments = false;
  }
  setAction(action) {
    this.selectedStatus = action;
    this.submit();
  }
  submit() {}
  updatedAgendaContent(data) {
    this.agendaContentLength = data.length;
  }
  updatedRecommendationsContent(data) {
    this.recommendationsContentLength = data.length;
  }
  createAdHocAppointment() {
    this.isAdHocDisabled = true;
    this.showAgenda = false;
    this.showStatus = false;
    this.setAppointmentType = "adhoc";
    this.sentRecap = false;
    this.haveUpComingAppointments = true;
    this.showAdhocRecommendation = true;
    this.showAppointmentRecommendation = false;
    this.appointment.recommendations = '';
    this.analytics.eventTrack('created new ad hoc record', { category: 'Advisor - Calendar / Scheduling', label: '' });
  }
  resetCreateNewAdHoc() {
    this.isAdHocDisabled = false;
    this.showAgenda = true;
    this.showStatus = true;
    this.setAppointmentType = "";
  }
  sendAgenda() {
    this.sentAgenda = true;
    this.analytics.eventTrack('sent agenda', { category: 'Advisor - Calendar / Scheduling', label: '' });
  }
  sendRecap() {
    this.sentRecap = true;
    this.analytics.eventTrack('sent recap', { category: 'Advisor - Calendar / Scheduling', label: '' });
  }
}
