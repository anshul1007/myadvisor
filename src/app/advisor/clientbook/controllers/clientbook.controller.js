export class AdvisorClientBookController {
  constructor(stringConstants) {
    'ngInject';
    this.stringConstants = stringConstants;
    this.tabs = this.stringConstants.clientbooktabs;
  }
  getTabTemplateUrl(tab) {
    return "'"+'app/advisor/clientbook/views/'+tab.templateName+"'";
  }
}
