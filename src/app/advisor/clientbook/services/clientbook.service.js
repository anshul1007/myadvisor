/*
   * Service to get email logs for client
   * req : GET email-processor/log/clients
*/
export class ClientBookService {
  constructor(Restangular, Session) {
    'ngInject';
    this.session = Session;
    this.loggedInClientId = this.session.request.clientId;
    this.emailRoute = Restangular.one('email-processor/log');
  }
  
  /*
   * Get email logs for client
   * @req : GET email-processor/log/clients
   * @params : loggedInClientId
   */
  getEmailLog() {
    return this.emailRoute.all('clients').get(this.loggedInClientId);
  }

}
