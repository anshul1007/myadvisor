describe('Describing ActivityLogController and its functions', () => {
  let scope;
  let controller;
  let scrollElement;
  let calendarElement;
  let httpBackend;
  let ClientBookService;
  let deferred;
  let moment;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $controller, $compile, $httpBackend, _ClientBookService_, _moment_, $q) => {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    deferred = $q.defer();
    moment = _moment_;
    ClientBookService = _ClientBookService_;
    spyOn(ClientBookService, 'getEmailLog').and.callFake(function() {
      deferred.resolve(true);
      return deferred.promise;
    });
    httpBackend.whenGET(/.*/).respond({});
    controller = $controller('ActivityLogController', {
      $scope: scope
    });

  }));

  it('Checking page number', () => {
    expect(controller.pageNumbers).toBe(5);
  });

  it('Checking page size', () => {
    expect(controller.pageSize).toBe(4);
  });

  it('Checking currentPage', () => {
    expect(controller.currentPage).toBe(1);
  });

  it('Checking getEmailLog from ClientBookService', () => {
    expect(ClientBookService.getEmailLog()).toBeDefined();
  });

  it('Chekcing getLogs function', () => {
    controller.allLogs = [{data: 'data'}];
    expect(controller.getLogs()).toBeDefined();
  });

  it('Chekcing pageChanged function', () => {
    controller.allLogs = [{data: 'data'}];
    controller.pageChanged();
    expect(controller.logs[0].data).toBe(controller.getLogs()[0].data);
  });

  it('Chekcing convertDate function', () => {
    controller.allLogs = [{createdDate: 1467844424}];
    controller.convertDate();
    expect(controller.allLogs[0].createdDate).toBe(moment(1467844424).format('LL'));
  })

});
