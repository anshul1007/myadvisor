/**
 * @todo  In Progree not completed
 * This test case is for clientbook service.
 */
describe('Clientbook Service', function() {
  let restangular;
  let session;
  let deferred;
  let httpBackend;
  let clientbookservice;
  let scope;

  beforeEach(angular.mock.module('MainApp'));
  beforeEach(inject(($rootScope, _ClientBookService_, _Restangular_, _Session_, $httpBackend, $q) => {
    clientbookservice = _ClientBookService_;
    restangular = _Restangular_;
    session = _Session_;
    httpBackend = $httpBackend;
    deferred = $q.defer();
    scope = $rootScope.$new();
    httpBackend.whenGET(/.*/).respond({});

  }));
  
  it('should test getEmailLog', () => {
    let retVal = '';
    deferred.resolve('resolveData');
    spyOn(clientbookservice, 'getEmailLog').and.returnValue(deferred.promise);
    clientbookservice.getEmailLog()
      .then(function(result) {
        retVal = result;
      });
    scope.$digest();
    expect(retVal).toBe('resolveData');
  });
});
