describe('Describing AdvisorDashboardController and its configuration functions', function() {
  let scope;
  let controller;
  let element;

  beforeEach(angular.mock.module('MainApp'));

  beforeEach(inject(($rootScope, $compile, $controller) => {
    scope = $rootScope.$new();
    controller = $controller('AdvisorDashboardController', {
      $scope: scope
    });
    scope.calendarConfig = controller.calendarConfig;
    scope.eventSources = controller.eventSources;
    element = angular.element('<div ui-calendar="calendarConfig" ng-model="eventSources"></div>');
    $compile(element)(scope);
    scope.$digest();
  }));

  it('Checking fc-toolbar: It should be 1 if calendar has been rendered.', () => {
    expect(element.find('.fc-toolbar').length).toBe(1);
  });
  
  it('Checking customView method.', () => {
    element = angular.element('<div class="fc-content"><div class="fc-title"></div></div>');
    scope.eventSources = [{eventId:1, preTitle: 'Test', title: 'Test', description: 'Test', disabled: 'Test'}];
    controller.customView(scope.eventSources, element);
    expect(element.find('[custom-view-event]').length).toBe(1);
    controller.customView(scope.eventSources, element);
  });
});
