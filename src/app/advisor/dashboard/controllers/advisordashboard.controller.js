class ClientProfileCardController {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }
  closeClientProfileCard() {
    this.$uibModalInstance.dismiss();
  }
}

export class AdvisorDashboardController {
  constructor($scope, moment, $compile, $uibModal, $rootScope, OTService, $analytics, $timeout, AdvisorAppointmentService, helpers, $state) {
    'ngInject';
    this.$scope = $scope;
    this.moment = moment;
    this.$compile = $compile;
    this.$uibModal = $uibModal;
    this.OTService = OTService;
    this.analytics = $analytics;
    this.rendering = true;
    this.test = 'test';
    this.$state = $state;
    this.helpers =helpers;
    this.appointmentService = AdvisorAppointmentService;
    $rootScope.$on('co-browsing:updateChart', (event, args) => {
      //@Todo should delete
      console.log('Advisor', args);
    });

    this.sendSignal = () => {
      let signalPayload = [{
        eduExp: 1000
      }];
      let signalErrorFunc = function() {
        // TODO
      };
      if (this.OTService.OTSession.streams.length) {
        this.OTService.sendSignalToOTSession('updateChart', signalPayload, signalErrorFunc);
      }
    };
    
    this.getAutomationId = (elmName) => {
      return this.helpers.generateAutomationID({elementState: this.$state.current.name, elementName: elmName, elementType: 'button'});
    };

    this.customView = (event, element) => {
      var el = this.$compile('<div custom-view-event automation-id="getAutomationId(elmName,elmType)" event-id="{{' + event.eventId + '}}" pre-title="' + event.preTitle + '" title="' + event.title + '" description="' + event.description + '" disabled="' + event.disabled + '"></div>')(this.$scope);
      element.find('.fc-title').replaceWith(el);
      angular.element('.fc-agendaWeek-button').attr('data-elm-id', this.getAutomationId('calendar-week'));
      angular.element('.fc-agendaDay-button').attr('data-elm-id', this.getAutomationId('calendar-day'));
      angular.element('.fc-today-button').attr('data-elm-id', this.getAutomationId('calendar-today'));
      angular.element('.fc-prev-button').attr('data-elm-id', this.getAutomationId('calendar-previous'));
      angular.element('.fc-next-button').attr('data-elm-id', this.getAutomationId('calendar-next'));    
    };

    this.showClientProfileCard = () => {
      this.sendSignal();
      this.$uibModal.open({
        templateUrl: 'app/advisor/dashboard/views/clientprofilecard.html',
        size: 'lg',
        controller: ClientProfileCardController,
        controllerAs: 'cpCtrl',
        scope: this.$scope,
        bindToController: true
      });
      this.analytics.eventTrack('viewed appointment shortcut', { category: 'Advisor - Calendar / Scheduling', label: '' });
    };

    this.afterViewRenderEvents = (view) => {
      this.getAdvisorAppointments();
      if (view.type === 'agendaWeek') {
        this.analytics.eventTrack('viewed calendar', { category: 'Advisor - Calendar / Scheduling', label: 'calendar view - week' });
      } else if (view.type === 'agendaDay') {
        this.analytics.eventTrack('viewed calendar', { category: 'Advisor - Calendar / Scheduling', label: 'calendar view - day' });
      }
    };
    this.events = [];
    this.eventSources = [this.events];
    this.calendarConfig = {
      height: 'auto',
      header: {
        left: 'agendaWeek,agendaDay',
        center: 'title',
        right: 'today prev,next'
      },
      defaultView: 'agendaDay',
      allDaySlot: false,
      minTime: '08:00:00',
      maxTime: '18:00:00',
      eventRender: this.customView,
      eventClick: this.showClientProfileCard,
      viewRender: this.afterViewRenderEvents
    };
  }

  /**
   * This method is responsible for response tranformation to required format as UI Calendar supports only
   * @params : id, title, description, start, end and allDay ...
   * reference : http://fullcalendar.io/docs/
   */
  transformResponse(response) {
    for (let index in response.appointments) {
      let event = response.appointments[index];
      let newEvent = () => {
        for (let obj of this.events) {
          if (obj.id === event.id) {
            return false;
          }
        }
        return true;
      };
      if (newEvent()) {
        this.events.push({
          id: event.id,
          eventId: index + 1,
          title: event.clientFirstName + ' ' + event.clientLastName,
          description: event.description,
          start: this.moment(event.startDate),
          end: this.moment(event.endDate),
          allDay: false
        });
      }
    }
    this.rendering = false;
  }
  getAdvisorAppointments() {
    const params = { startDate: this.moment().valueOf(), timeZone: this.appointmentService.localTimezone, endDate: this.moment().add(11, 'months').valueOf() };
    this.appointmentService.retrieveAdvisorCalendar(params).then((response) => {
      this.transformResponse(response);
    });
  }
}
