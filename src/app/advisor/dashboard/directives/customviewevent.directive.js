class CustomViewEventDirectiveController {
  constructor($timeout) {
    'ngInject';

    this.$timeout = $timeout;
  }
}

export default class CustomViewEventDirective {
  constructor() {
    this.restrict = 'A';
    this.templateUrl = 'app/advisor/dashboard/views/customviewevent.html';
    this.scope = {
      eventId: '@',
      preTitle: '@',
      title: '@',
      description: '@',
      disabled: '@',
      automationId: '&'
    };
    this.controller = CustomViewEventDirectiveController;
    this.controllerAs = 'cveCtrl';
  }

  link(scope, element, attrs, cveCtrl) {
    scope.isGiveReason = false;
    scope.showPopover = function(event) {
      event.stopPropagation();
      angular.element(document).trigger('click');
      scope.isGiveReason = false;
    };

    scope.hidePopover = function() {
      angular.element(document).trigger('click');
      cveCtrl.$timeout(() => {
        scope.isGiveReason = false;
      }, 200);
    };

    scope.nextStep = function() {
      scope.isGiveReason = true;
    };

    scope.cancelEvent = function() {
      scope.hidePopover();
    };
  }
}
