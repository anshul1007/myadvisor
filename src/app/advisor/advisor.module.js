/* global moment:false */

import { config } from './advisor.config';
import { routerConfig } from './advisor.route';
import { runBlock } from './advisor.run';
import { AdvisorController } from './main/controllers/advisor.controller';
import { AdvisorDashboardController } from './dashboard/controllers/advisordashboard.controller';
import { AdvisorAppointmentService } from './shared/services/advisor-appointment.service';
import { AdvisorClientBookController } from './clientbook/controllers/clientbook.controller';
import { AdvisorAppointmentsController } from './clientbook/controllers/appointments.controller';
import { ActivityLogController } from './clientbook/controllers/activitylog.controller';
import { ClientBookService } from './clientbook/services/clientbook.service';
import { SearchController } from './clientbook/controllers/search.controller';
import { AdvisorProfileController } from './profile/controllers/profile.controller';
import { ConferenceController } from './main/controllers/conference.controller';
import CustomViewEventDirective from './dashboard/directives/customviewevent.directive';
import InternalNotesDirective from './shared/directives/internalnotes.directive';
import AdvisorTaskDirective from './shared/directives/advisortask.directive';

angular.module('myAdvisor', [])
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .controller('AdvisorController', AdvisorController)
  .controller('AdvisorDashboardController', AdvisorDashboardController)
  .controller('AdvisorClientBookController', AdvisorClientBookController)
  .controller('SearchController', SearchController)
  .controller('AdvisorAppointmentsController', AdvisorAppointmentsController)
  .controller('ActivityLogController', ActivityLogController)
  .controller('AdvisorProfileController', AdvisorProfileController)
  .controller('ConferenceController', ConferenceController)
  .directive('customViewEvent', () => new CustomViewEventDirective())
  .directive('internalNotes', () => new InternalNotesDirective())
  .directive('advisorTask', () => new AdvisorTaskDirective())
  .service('AdvisorAppointmentService', AdvisorAppointmentService)
  .service('ClientBookService', ClientBookService);

