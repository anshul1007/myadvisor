/*
 * Advisor appointment service
 */
export class AdvisorAppointmentService {
  constructor(Restangular, Session, moment) {
    'ngInject';
    this.moment = moment;
    //stating calendar route
    this.calendarRoute = Restangular.one('calendar');
    this.session = Session;
    this.localTimezone = 'UTC' + this.moment.tz(this.moment(), this.moment.tz.guess()).format('Z');
    this.advisorId = '40288030556fb0f801556fb110210000';
  }
  /**
    * Getter for logged in Advisor Id 
    * retrieving from session
    */
  get loggedInAdvisorId(){
    if (angular.isDefined(this.session.request.advisorId)) {
      this.advisorId = this.session.request.advisorId;
    }
    return this.advisorId;
  }
  /**
    * Setter for logged in Advisor Id 
    * retrieving from session
    */
  set loggedInAdvisorId(advisorId){
    this.advisorId = advisorId;
    this.session.request.advisorId = advisorId;
  }
  /**
    * Method for retrieve advisor calendar
    * @params
    * @return object
    */
  retrieveAdvisorCalendar(params) {
    const advisorId = this.loggedInAdvisorId;
    return this.calendarRoute.all('advisors').customGET(advisorId, params);
  }
  /**
    * Method for retrieve appointment by Id
    * @appointmentId
    * @return object 
    */
  retrieveAppointmentById(appointmentId){
    return this.calendarRoute.one('appointments').customGET(appointmentId);
  }
  /*
   * Delete an appointment
   * @req : DELETE  /calendar/appointments/{appointmentId}
   *
   */
  deleteAppointment(appointmentId) {
    const params = { actionInitiator: 'ADVISOR' };
    return this.calendarRoute.one('appointments').customDELETE(appointmentId, params);
  }
}
