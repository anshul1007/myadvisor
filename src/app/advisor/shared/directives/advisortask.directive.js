class MarkCompletePopupController {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }

  closeMarkCompletePopup() {
    this.$uibModalInstance.dismiss(false);
  }

  markTaskComplete() {
    this.$uibModalInstance.close(false);
  }
}

class AdvisorTaskDirectiveController {
  constructor(moment, $timeout, $uibModal, $analytics, $scope) {
    'ngInject';

    this.moment = moment;
    this.$timeout = $timeout;
    this.$uibModal = $uibModal;
    this.analytics = $analytics;

    this.taskFilterLabels = ['Global', 'My Clients'];
    this.filterTask(true);
    this.$scope = $scope;
    this.isEdit = false;
    this.isMarkCompleteChecked = false;
  }

  getMonths(dueDate) {
    let i;
    let months = [];

    for (i = 1; i <= 12; i++) {
      months.push({
        value: i,
        text: (i < 10 ? '0' + i : i).toString()
      });
    }

    if (dueDate) {
      this.selectedMonth = months.find((month) => {
        return month.value === this.moment(dueDate).month() + 1;
      });
    }

    return months;
  }

  getDays(dueDate) {
    let i;
    let days = [];

    for (i = 1; i <= 31; i++) {
      days.push({
        value: i,
        text: (i < 10 ? '0' + i : i).toString()
      });
    }

    if (dueDate) {
      this.selectedDay = days.find((day) => {
        return day.value === this.moment(dueDate).date();
      });
    }

    return days;
  }

  getYears(dueDate) {
    let i;
    let years = [];

    for (i = 16; i <= 17; i++) {
      years.push({
        value: i,
        text: i.toString()
      });
    }

    if (dueDate) {
      this.selectedYear = years.find((year) => {
        return year.value === Number(this.moment(dueDate).year().toString().substr(2, 2));
      });
    }

    return years;
  }

  isPastDue(dueDate) {
    return this.moment(dueDate).isBefore();
  }

  isAboutToDue(dueDate) {
    return !this.isPastDue(dueDate) && this.moment(dueDate).isBefore(this.moment().add('24', 'hours'));
  }

  getFormattedDate(date, showAsAgo) {
    let dateAsAgo = '';

    if (showAsAgo) {
      let pastHours = this.moment().diff(this.moment(date), 'hours');

      if (pastHours < 24) {
        dateAsAgo = (pastHours === 1) ? (pastHours + ' hour ago') : (pastHours + ' hours ago');
      }
    }

    return dateAsAgo ? dateAsAgo : this.moment(date).format('MM/DD/YY');
  }

  resetDate() {
    delete this.selectedMonth;
    delete this.selectedDay;
    delete this.selectedYear;
  }

  makeDueDate() {
    if (this.selectedMonth && this.selectedDay && this.selectedYear) {
      return this.moment(this.selectedYear.value + '-' + this.selectedMonth.value + '-' + this.selectedDay.value, 'YYYY-MM-DD').valueOf();
    }
  }

  isIncompleteDueDate() {
    return ((this.selectedMonth && (!this.selectedDay || !this.selectedYear)) || (this.selectedDay && (!this.selectedMonth || !this.selectedYear)) || (this.selectedYear && (!this.selectedMonth || !this.selectedDay)));
  }

  isPastDate() {
    let date = this.makeDueDate();
    if (date) {
      return this.moment(date).isBefore(this.moment());
    }
  }

  editTask(task) {
    if (task.dueDate) {
      this.months = this.getMonths(task.dueDate);
      this.days = this.getDays(task.dueDate);
      this.years = this.getYears(task.dueDate);
    }
    this.isEdit = true;
    task.tempDescription = task.description;
  }

  cancelEdit() {
    this.isEdit = false;
  }

  isEditTaskInvalid(task) {
    return !task.tempDescription.length || this.isIncompleteDueDate() || this.isPastDate();
  }

  updateTask(task) {
    if (!this.isIncompleteDueDate() && !this.isPastDate()) {
      task.description = task.tempDescription;

      if (task.dueDate) {
        task.dueDate = this.makeDueDate();
      }

      angular.element(document).trigger('click');

      this.$timeout(() => {
        this.isEdit = false;
      }, 150);
      this.analytics.eventTrack('edited advisor task', { category: 'Advisor - Tools', label: '' });
    }
  }

  checkMarkTaskComplete(task) {
    if (this.isMarkCompleteChecked) {
      let modalInstance = this.$uibModal.open({
        templateUrl: 'app/advisor/shared/views/_mark_task_complete.html',
        controller: MarkCompletePopupController,
        controllerAs: 'mtcCtrl',
        scope: this.$scope
      });

      modalInstance.result.then(() => {
        this.tasks.splice(this.tasks.indexOf(task), 1);
        this.isMarkCompleteChecked = false;
        this.analytics.eventTrack('marked advisor task complete', { category: 'Advisor - Tools', label: '' });
      }, () => {
        this.isMarkCompleteChecked = false;
      });
    }
  }

  createNewTask() {
    this.resetDate();

    this.months = this.getMonths();
    this.days = this.getDays();
    this.years = this.getYears();

    this.newTask = {
      description: ''
    };
  }

  hideNewTaskPopup() {
    angular.element(document).trigger('click');
    this.resetDate();
  }

  isNewTaskInvalid() {
    return !this.newTask.description.length || this.isIncompleteDueDate() || this.isPastDate();
  }

  saveNewTask() {
    if (!this.isNewTaskInvalid()) {
      this.newTask = {
        name: 'Scott Abbott',
        description: this.newTask.description,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().valueOf()
      };

      let dueDate = this.makeDueDate();
      if (dueDate) {
        this.newTask.dueDate = dueDate;
      }

      this.tasks.unshift(this.newTask);
      this.hideNewTaskPopup();
      this.analytics.eventTrack('created advisor task', { category: 'Advisor - Tools', label: '' });
    }
  }

  filterTask(isGlobal) {
    const desc = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in finibus sapien, ut tristique erat. Cras ultrices eros ac augue blandit semper. Integer ipsum nisl, porta sed rhoncus quis, tempor id turpis.';
    if (isGlobal) {
      this.tasks = [{
        name: 'Scott Abbott 1',
        description: desc,
        dueDate: this.moment().subtract(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 2',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 3',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 4',
        description: desc,
        dueDate: this.moment().subtract(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 5',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 6',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 7',
        description: desc,
        dueDate: this.moment().subtract(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 8',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 9',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 10',
        description: desc,
        dueDate: this.moment().subtract(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 11',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 12',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 13',
        description: desc,
        dueDate: this.moment().subtract(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 14',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 15',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }];

      this.taskFilterLabel = this.taskFilterLabels[0];
    } else {
      this.tasks = [{
        name: 'Scott Abbott 16',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 17',
        description: desc,
        dueDate: this.moment().add(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 18',
        description: desc,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(5, 'days').valueOf()
      }, {
        name: 'Scott Abbott 19',
        description: desc,
        dueDate: this.moment().add(1, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(4, 'hours').valueOf()
      }, {
        name: 'Scott Abbott 20',
        description: desc,
        dueDate: this.moment().add(3, 'days').valueOf(),
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().subtract(8, 'hours').valueOf()
      }];

      this.taskFilterLabel = this.taskFilterLabels[1];
    }

    let filterTask = (isGlobal ? 'global' : 'my clients');
    this.analytics.eventTrack('selected advisor task filter', { category: 'Advisor - Tools', label: 'task filter view - ' + filterTask });
  }
}

export default class AdvisorTaskDirective {
  constructor() {
    this.restrict = 'A';
    this.templateUrl = 'app/advisor/shared/views/_advisor_task.html';
    this.controller = AdvisorTaskDirectiveController;
    this.controllerAs = 'atdCtrl';
  }

  link(scope, element, attrs, atdCtrl) {
    atdCtrl.title = attrs.title;
    atdCtrl.isShowFilter = (angular.isUndefined(attrs.showFilter) ? false : true);
    atdCtrl.isShowNewTaskButton = (angular.isUndefined(attrs.showNewTaskButton) ? false : true);
    atdCtrl.isShowAdvisor = (angular.isUndefined(attrs.client) ? true : false);
  }
}
