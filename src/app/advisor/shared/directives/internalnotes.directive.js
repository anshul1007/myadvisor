class InternalNotesDirectiveController {
  constructor(moment, $timeout) {
    'ngInject';

    this.moment = moment;
    this.$timeout = $timeout;

    const desc = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In in finibus sapien, ut tristique erat. Cras ultrices eros ac augue blandit semper. Integer ipsum nisl, porta sed rhoncus quis, tempor id turpis.';
    this.notes = [{
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(5, 'days').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(4, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(8, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(5, 'days').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(4, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(8, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(5, 'days').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(4, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(8, 'hours').valueOf()
    }, {
      description: desc,
      advisorName: 'Advisor’s Name',
      createdDate: this.moment().subtract(5, 'days').valueOf()
    }];

    this.isEdit = false;
  }

  getFormattedDate(date, showAsAgo) {
    let dateAsAgo = '';

    if (showAsAgo) {
      let pastHours = this.moment().diff(this.moment(date), 'hours');

      if (pastHours < 24) {
        dateAsAgo = (pastHours === 1) ? (pastHours + ' hour ago') : (pastHours + ' hours ago');
      }
    }

    return dateAsAgo ? dateAsAgo : this.moment(date).format('MM/DD/YY');
  }

  editNote(note) {
    this.isEdit = true;
    note.tempDescription = note.description;
  }

  cancelEdit() {
    this.isEdit = false;
  }

  isEditNoteInvalid(note) {
    return !note.tempDescription.length;
  }

  updateNote(note) {
    note.description = note.tempDescription;

    this.hideNotePopup();

    this.$timeout(() => {
      this.isEdit = false;
    }, 150);
  }

  createNewNote() {
    this.newNote = {
      description: ''
    };
  }

  hideNotePopup() {
    angular.element(document).trigger('click');
  }

  isNewNoteInvalid() {
    return !this.newNote.description.length;
  }

  saveNewNote() {
    if (!this.isNewNoteInvalid()) {
      this.newNote = {
        description: this.newNote.description,
        advisorName: 'Advisor’s Name',
        createdDate: this.moment().valueOf()
      };

      this.notes.unshift(this.newNote);
      this.hideNotePopup();
    }
  }
}

export default class InternalNotesDirective {
  constructor() {
    this.restrict = 'A';
    this.templateUrl = 'app/advisor/shared/views/_internal_notes.html';
    this.controller = InternalNotesDirectiveController;
    this.controllerAs = 'inCtrl';
  }

  link(scope, element, attrs, atdCtrl) {
    atdCtrl.title = attrs.title;
  }
}
