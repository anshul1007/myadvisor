/*jshint esversion: 6*/
export function config(RestangularProvider, KeepaliveProvider, IdleProvider, TitleProvider) {
  'ngInject';

  RestangularProvider.setBaseUrl('/api/v1');
  RestangularProvider.setDefaultHttpFields({ cache: false });
  RestangularProvider.setMethodOverriders([]); // This overrides API call methods to POST
  RestangularProvider.setDefaultHeaders({
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Max-Age': 2520,
    'Access-Control-Allow-Headers': 'X-Requested-With',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE',
    'Content-Type': 'application/json;charset=utf-8'
  });

  // add a response interceptor
  RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
    return data;
  });
  //This method is deprecated
  // RestangularProvider.setResponseExtractor(function(response, operation) {
  //   return response.data;
  // });

  RestangularProvider.setRestangularFields({
    id: '_id',
    route: 'restangularRoute',
    selfLink: 'self.href'
  });

  // configure Idle settings
  //Time set to 15 minutes 
  TitleProvider.enabled(false);
  IdleProvider.idle(870);
  IdleProvider.timeout(30);
  KeepaliveProvider.interval(2);
}
