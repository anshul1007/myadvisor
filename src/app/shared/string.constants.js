/* eslint-disable */

// To keep all common static string constants here
exports.common = {
  pageTitle: 'Home',
  description: 'Home',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
};

// To keep all client module common static string constants here
exports.client = {
  pageTitle: 'Client Home',
  description: 'Client Home',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
};

// To keep all advisor module static string constants here
exports.advisor = {
  pageTitle: 'Advisor Dashboard',
  description: 'Advisor Dashboard',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
  dashboard: {
    pageTitle: 'Advisor Dashboard',
    description: 'Advisor Dashboard',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
    pageHeading: 'Advisor Dashboard',
    navTitle: 'Dashboard',
    navUrl: '.dashboard'
  },
  performance: {
    pageTitle: 'Advisor Performance',
    description: 'Advisor Performance',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
    pageHeading: 'Advisor Performance',
    navTitle: 'Performance',
    navUrl: '.performance'
  },
  notifications: {
    pageTitle: 'Advisor Notifications',
    description: 'Advisor Notifications',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
    pageHeading: 'Advisor Notifications',
    navTitle: 'Notifications',
    navUrl: '.notifications'
  },
  clientbook: {
    pageTitle: 'Advisor Client Book',
    description: 'Advisor Client Book',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
    pageHeading: 'Advisor Client Book',
    navTitle: 'Client Book',
    navUrl: '.clientbook.search'
  },
  profile:{
    pageTitle: 'Advisor Profile',
    description: 'Advisor Profile',
    keywords: 'Advisor Profile',
    pageHeading: 'Advisor Profile',
    navTitle: 'Advisor Profile',
    navUrl: '.profile'
  }
};

// To keep all dashboard static string constants here
exports.dashboard = {
  pageTitle: 'Client Dashboard',
  description: 'Client Dashboard',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
  pageHeading: 'Client Dashboard',
  chartEmptyLabel: 'Loading Data...',
  conference: {
    advisorStatusReady: 'Ready - for appointment',
    advisorStatusMissed: 'Missed',
    noAdvisorStatus: 'None',
    defaultTelFromMobile: '+375292771265',
    defaultMailId: 'advisor@rbc.com'
  }
};

// To keep all appointment static string constants here
exports.appointment = {
  pageTitle: 'Client Appointment',
  description: 'Client Appointment',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
  choosetopic: {
    pageTitle: 'Client Appointment - Choose Topic',
    description: 'Client Appointment  - Choose Topic',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  },
  availabletime: {
    pageTitle: 'Client Appointment - Available Time',
    description: 'Client Appointment  - Available Time',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  },
  choosedate: {
    pageTitle: 'Client Appointment - Choose Date',
    description: 'Client Appointment  - Choose Date',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  },
  availabletimesbydate: {
    pageTitle: 'Client Appointment - Available Time by Date',
    description: 'Client Appointment  - Available Time by Date',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  },
  confirmphone: {
    pageTitle: 'Client Appointment - Confirm Phone',
    description: 'Client Appointment  - Confirm Phone',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  },
  confirmappointment: {
    pageTitle: 'Client Appointment - Confirm Appointment',
    description: 'Client Appointment  - Confirm Appointment',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  }
};

// To keep all onboarding static string constants here
exports.onboarding = {
  pageTitle: 'Client Onboarding',
  description: 'Client Onboarding',
  keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding',
  contract: {
    pageTitle: 'Client Onboarding - Contract',
    description: 'Client Onboarding  - Contract',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  }
};

// To keep all onboarding static string constants here
exports.commonerrors = {
  pageNotFound: {
    pageTitle: 'Page not found.',
    description: 'Page not found.',
    keywords: 'MyAdvisory Home,Finacial leads,Appointments,Onboarding'
  }
};
//Tabs at Advisor Client Book
exports.clientbooktabs = {
  'appointments':{
    title: 'Client Appointments',
    templateName: '_appointments.html'
  },
  'client-info':{
    title: 'Client Info',
    templateName: '_clientinfo.html',
    subTabs:''
  },
  'financial-picture':{
    title: 'Financial Picture',
    templateName: '_financialpicture.html',
    subTabs:''
  },
  'activity-log':{
    title: 'Activity Log',
    templateName: '_activitylog.html',
    subTabs:''
  }
};
