export default function PhoneFormatFilter() {
  return function(phoneNumber) {
    if (!phoneNumber) {
      return '';
    }
    
    const value = phoneNumber.toString().trim().replace(/^\+/, '');
    let countryCode;
    let cityCode;
    let number;

    if (value.match(/[^0-9]/)) {
      return phoneNumber;
    }
    
    
    switch (value.length) {
    case 10:
      countryCode = 1;
      cityCode = value.slice(0, 3);
      number = value.slice(3);
      break;

    case 11:
      countryCode = value[0];
      cityCode = value.slice(1, 4);
      number = value.slice(4);
      break;

    case 12:
      countryCode = value.slice(0, 3);
      cityCode = value.slice(3, 5);
      number = value.slice(5);
      break;

    default:
      return phoneNumber;
    }

    if (countryCode === 1) {
      countryCode = '';
    }
    
    number = number.slice(0, 3) + '-' + number.slice(3);

    return (countryCode + ' (' + cityCode + ') ' + number).trim();
  };
}
