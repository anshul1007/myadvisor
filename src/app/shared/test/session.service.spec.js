
describe('Session Service', function() {
  let session;
  let rootScope;
  let scope;

  beforeEach(angular.mock.module('MainApp'));
  beforeEach(inject(($rootScope, _Session_) => {
    rootScope = $rootScope;
    session = _Session_;
    scope = $rootScope.$new();
    spyOn(rootScope, '$broadcast').and.callThrough();
    spyOn(rootScope, '$on').and.callThrough();
    spyOn(session, 'callforSecondsTimer');

  }));
  
  it('should check seconds value', () => {
    expect(rootScope.seconds).toBe(30);
  });
  
  it('should test checkForIdleState function', () => {
    rootScope.$broadcast('IdleStart');
    session.checkForIdleState();
    expect(rootScope.$on).toHaveBeenCalledWith('IdleStart', jasmine.any(Function));
    expect(session.callforSecondsTimer).toHaveBeenCalled();
  });
});
