class IdleTimeOutController {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }

  sayYes() {
    this.$uibModalInstance.close();
  }
}

/*
 * Session Storage service
 */

export class Session {
  constructor($sessionStorage, $uibModal, $rootScope, $interval) {
    'ngInject';
    this.sessionStorage = $sessionStorage;
    this.request = this.request || {};
    this.$uibModal = $uibModal;
    this.$rootscope = $rootScope;
    this.$rootscope.seconds=30;
    this.$interval = $interval;
    this.modalObj;
    this.checkForIdleState();
  }
  get request() {
    return this.sessionStorage.reqObject;
  }
  set request(reqObject) {
    this.sessionStorage.reqObject = reqObject;
  }
  unSetRequest(reqObject) {
    delete this.sessionStorage.reqObject;
  }
  checkForIdleState() {
    this.$rootscope.$on('IdleStart', () => {
      this.$rootscope.seconds=30;
      this.callforSecondsTimer();
      this.modalObj = this.$uibModal.open({
        templateUrl: 'app/shared/views/_idle-timeout.html',
        controller: IdleTimeOutController,
        controllerAs: 'idlCtrl',
        scope: this.$scope,
        bindToController: true
      });
    });
  }

  callforSecondsTimer() {
    let counter = 30;
    this.promise = this.$interval(() => {
      counter = counter +1;
      if (counter >= 60) {
        this.$rootscope.seconds = 60;
        this.modalObj.close();
        this.$interval.cancel(this.promise);
      }
      this.$rootscope.seconds = counter;
    }, 1000);
  }
}
