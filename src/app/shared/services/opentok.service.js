export class OTService {
  constructor($q, OT, $state) {
    'ngInject';
    this.OTSession = {};
    this.$q = $q;
    this.apiKey = '';
    this.sessionId = '';
    this.token = '';
    this.meetingId = '';
    this.OT = OT;
    this.apiKey = '45589582';
    this.$state = $state;
  }

  initOTSession($scope, $rootScope) {
    this.OTSession.init(this.apiKey, this.sessionId, this.token, (err, session) => {
      $scope.session = session;
      if (!err) {
        $scope.$apply(() => {
          $scope.connected = true;
          this.bindSingalEvents($rootScope);
        });
      }
    });
  }

  initOTConnection($scope, $rootScope) {
    $scope.streams = this.OTSession.streams;
    $scope.connections = this.OTSession.connections;
    $scope.connected = false;
    $scope.faceSuscriberPropsHD = this.getFaceSuscriberPropsHD(this.$state);

    this.initOTSession($scope, $rootScope);

    $scope.notMine = (stream) => {
      if (stream.connection && $scope.session.connection) {
        return stream.connection.connectionId !== $scope.session.connection.connectionId;
      }
    };

    $scope.$on('$destroy', function() {
      if ($scope.session && $scope.connected) {
        $scope.session.disconnect();
        $scope.connected = false;
      }
      $scope.session = null;
    });
  }

  logoutSession($scope) {
    $scope.session.disconnect();
    $scope.session.on('sessionDisconnected', () => {
      $scope.logoutMeeting();
      $scope.$apply(function() {
        $scope.connected = false;
      });
    });
  }

  isBrowserSupported() {
    return this.OT.checkSystemRequirements();
  }

  bindSingalEvents($scope) {
    this.OTSession.session.on({
      'signal:updateChart': (event) => {
        if (event.from.connectionId !== this.OTSession.session.connection.connectionId) {
          JSON.parse(event.data).forEach(function (data) {
            console.log(data);
            $scope.$broadcast('co-browsing:updateChart', data);
          });
        }
      }
    });

    $scope.$on('$destroy', function() {
      if ($scope.session && $scope.connected) {
        $scope.session.disconnect();
        $scope.connected = false;
      }
      $scope.session = null;
    });
  }

  getFaceSuscriberPropsHD($state) {
    return {
      name: ($state.current.name === 'advisor.dashboard' ? 'Client' : 'Advisor'),
      width: '100%',
      height: '100%',
      style: {
        nameDisplayMode: 'on'
      },
      resolution: '1280x720',
      frameRate: 30
    };
  }

  sendSignalToOTSession(signalName, signalPayload, signalErrorHandler) {
    this.OTSession.session.signal(
      {
        type: signalName,
        data: JSON.stringify(signalPayload)
      },
      signalErrorHandler
    );
  }

  setSessionToken(OTObject) {
    let deferred = this.$q.defer();
    this.sessionId = OTObject.sessionId;
    this.token = OTObject.token;
    deferred.resolve(true);
    return deferred.promise;
  }
}
