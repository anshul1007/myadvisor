export class HelperService {
  constructor($sce, deviceDetector, $window) {
    'ngInject';
    this.sce = $sce;
    this.window = $window;
    this.deviceDetector = deviceDetector;
  }

  /**
   * This method is responsible for splitting of given string as per first character
   * @inputString : String
   * @return: first letter of string
   */
  firstLetterOfString(inputString) {
    return inputString && inputString.charAt(0);
  }

  randomNumber(maxNumber) {
    return Math.floor((Math.random() * maxNumber) + 1);
  }

  trustSrc(src) {
    return this.sce.trustAsResourceUrl(src);
  }

  isDesktopDevice() {
    return this.deviceDetector.isDesktop();
  }

  isTabletDevice() {
    return this.deviceDetector.isTablet();
  }

  generateAutomationID(elementObject) {
    elementObject.elementState = elementObject.elementState.replace(/\./g, '-');
    let automationID = elementObject.elementState + '_' + elementObject.elementName + '_' + elementObject.elementType;
    if (elementObject.elementIndex) {
      automationID = elementObject.elementState + '_' + elementObject.elementName + '_' + elementObject.elementType + '_' + elementObject.elementIndex;
    }
    return automationID;
  }

  inBusinessHours() {
    let officeHours = 8;
    let startHour = 9;
    let endHours = startHour + officeHours;

    let date = new Date();
    let currentHour = date.getHours();

    return !(currentHour < startHour || currentHour >= endHours);
  }

  openNaiveMailClient(emailId) {
    this.window.location.href = 'mailto:' + emailId;
  }

  createArrayForSelect(start, end) {
    let i;
    let object = [];

    for (i = start; i <= end; i++) {
      object.push({
        value: i,
        text: (i < 10 ? '0' + i : i).toString()
      });
    }
    return object;
  }
}
