export class UserService {
  constructor(moment) {
    'ngInject';

    this.moment = moment;

    this.clientDetails = {
      clientId: '40288030556fb0f801556fb110cf0001',
      dob: this.moment("1984-02-08").valueOf(),
      spouse: {
        dob: this.moment("1986-02-08").valueOf()
      }
    };
  }
}
