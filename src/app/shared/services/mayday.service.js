class ModalInstanceCtrl {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }
  ok(response = []) {
    this.$uibModalInstance.close(response);
  }
  cancel(data = true) {
    this.$uibModalInstance.dismiss(data);
  }
}
export class MayDayService {
  constructor($http, $q, $interval, $state, $timeout, OTService, helpers, DontLeavePageService, Restangular, Session, $location, AppointmentService, moment) {
    'ngInject';
    this.$http = $http;
    this.$q = $q;
    this.$interval = $interval;
    this.$state = $state;
    this.$timeout = $timeout;
    this.OTService = OTService;
    this.helpers = helpers;
    this.appointmentService = AppointmentService;
    this.moment = moment;
    this.dontLeavePageService = DontLeavePageService;
    this.session = Session;
    this.advisorObj = {};
    this.restAngular = Restangular;
    this.loggedInClientId = this.session.request.clientId;
    this.confCallRoute = Restangular.one('conference/call');

    if(this.$state.current.name === 'advisor.dashboard') {
      const advisorId = $location.search()['advisorId'];
      this.setAdvisorId(advisorId);
    } else if(this.$state.current.name === 'client.dashboard') {
      const clientId = $location.search()['clientId'];
      this.setClientId(clientId);
    }
  }

  getMeetingStatus(participantType) {
    let deferred = this.$q.defer();
    if (participantType === 'advisor') {
      const headers = { advisorId: this.getAdvisorId(), conferenceId: this.getMeetingId() };
      this.restAngular.setDefaultHeaders(headers);

      this.confCallRoute.all('advisor/ping').customGET().then((response)=>{
        if (response.conferences.length) {
          this.setMeetingId(response.conferences[0].id);
        }
        deferred.resolve(response);
      });
    } else {
      const headers = { conferenceId: this.getMeetingId()};
      this.restAngular.setDefaultHeaders(headers);

      this.confCallRoute.all('client/ping').customGET().then((response)=>{
        deferred.resolve(response);
      });
    }

    return deferred.promise;
  }

  getAdvisorOTTokens() {
    let deferred = this.$q.defer();
    const params = { conferenceId: this.getMeetingId() };
    this.restAngular.setDefaultHeaders(params);

    this.confCallRoute.one('session').customPOST().then((response)=>{
      const responseObj = {
        token: response.advisorToken,
        sessionId: response.sessionId
      };
      this.OTService.setSessionToken(responseObj);
      deferred.resolve(responseObj);
    });

    return deferred.promise;
  }

  isWatchingPageChanged(isWatching, $scope) {
    if (isWatching) {
      const allowedSubPages = [];
      this.dontLeavePageService.startWatchingPage($scope, allowedSubPages);
    } else if (this.dontLeavePageService.eventHandler) {
      this.dontLeavePageService.stopWatchingPage();
    }
  }

  checkDeviceSupport(meetingObj) {
    meetingObj.isSessionStart = true;
    meetingObj.isMeetingAccepted = false;
    meetingObj.waitingRoomEnabled = true;
  }

  startStopCallTimer(isTimerStart, meetingObj) {
    if (isTimerStart) {
      this.$intervalObj = this.$interval(() => {
        const myTime = meetingObj.elapsed;
        const ss = myTime.split(':');
        let dt = new Date();
        dt.setHours(ss[0]);
        dt.setMinutes(ss[1]);
        dt.setSeconds(ss[2]);
        const dt2 = new Date(dt.valueOf() + 1000);
        const ts = dt2.toTimeString().split(' ')[0];
        meetingObj.elapsed = ts;
      }, 1000);
    } else {
      this.$interval.cancel(this.$intervalObj);
    }
  }

  openModelPopup(templateUrl, data = {}, meetingObj) {
    return meetingObj.$uibModal.open({
      animation: true,
      templateUrl: templateUrl,
      controller: ModalInstanceCtrl,
      controllerAs: 'mCtrl',
      bindToController: true,
      backdrop: 'static',
      keyboard: false,
      scope: meetingObj.$scope,
      resolve: {
        modalData: () => {
          return data;
        }
      }
    });
  }

  getAdvisorStatus(meetingObj) {
    let deferred = this.$q.defer();
    const headers = { clientId: this.getClientId() };
    this.restAngular.setDefaultHeaders(headers);

    this.confCallRoute.one('advisor/availability').get().then((response)=>{
      this.setAdvisorId(response.advisor.id);
      this.advisorObj = response.advisor;
      meetingObj.advisorName = response.advisor.firstName + ' ' + response.advisor.lastName;
      deferred.resolve(response);
    });

    return deferred.promise;
  }

  sendPhoneCallRequest(data) {
    const params = { clientId: this.getClientId(), advisorId: this.getAdvisorId(), conferenceType: 'AD_HOC', communicationType: 'PHONE', callbackNumber: data.phoneNumber, reasonId: data.reasonId };

    this.confCallRoute.customPOST(params).then((response)=>{
      this.setMeetingId(response.id);
    });
  }

  setMeetingStarted(data, callType) {
    if (callType === 'SCHEDULED') {
      const params = { clientId: this.getClientId(), advisorId: data.advisorId, conferenceType: 'SCHEDULED', communicationType: 'VIDEO', appointmentId: data.conferenceId };

      this.confCallRoute.customPOST(params).then((response)=>{
        this.setMeetingId(response.id);
      });
    } else {
      const params = { clientId: this.getClientId(), advisorId: this.getAdvisorId(), conferenceType: 'AD_HOC', communicationType: 'VIDEO', callbackNumber: data.phoneNumber, reasonId: data.reasonId };

      this.confCallRoute.customPOST(params).then((response)=>{
        this.setMeetingId(response.id);
      });
    }
  }

  setMeetingDeclined() {
    const params = { conferenceState: 'CANCELLED' };
    const headers = { conferenceId: this.getMeetingId() };
    this.restAngular.setDefaultHeaders(headers);

    this.confCallRoute.patch(params).then(()=>{
      this.setAdvisorId('');
      this.setMeetingId('');
    });
  }

  setMeetingDismissed() {
    const params = { conferenceState: 'DISMISSED' };
    const headers = { conferenceId: this.getMeetingId() };
    this.restAngular.setDefaultHeaders(headers);

    this.confCallRoute.patch(params).then(()=>{
      this.setMeetingId('');
    });
  }

  doConferenceHangUp(participants) {
    if (this.getMeetingId()) {
      const params = { conferenceState: participants === 'advisor' ? 'ADVISOR_HANG_UP' : 'CLIENT_HANG_UP'};
      const headers = { conferenceId: this.getMeetingId() };
      this.restAngular.setDefaultHeaders(headers);

      this.confCallRoute.patch(params).then(()=>{
        // console.log(response);
      });
    }
  }

  setMeetingId(id) {
    this.meetingId = id;
  }

  getMeetingId() {
    return this.meetingId;
  }

  setClientId(id) {
    this.clientId = id;
  }

  getClientId() {
    return this.clientId;
  }

  setAdvisorId(id) {
    this.advisorId = id;
  }

  getAdvisorId() {
    return this.advisorId;
  }

  toggleVideoControls(isShow, duration = 0) {
    if (!duration) {
      if (isShow) {
        angular.element('#bottomBar').fadeIn('slow');
      } else {
        angular.element('#bottomBar').fadeOut('slow');
      }
    } else {
      angular.element('#bottomBar').fadeOut(duration);
    }
  }

  expandCollapseScreen(meetingObj) {
    meetingObj.isExpand = !meetingObj.isExpand;
    if (meetingObj.isExpand) {
      angular.element('#session').removeClass('video-session');
      angular.element('#session').addClass('expand-video');
      angular.element('#bottomBar').addClass('expand-video-controls');
      angular.element('ot-subscriber').addClass('expand-video');
      angular.element('ot-subscriber').css('position', 'fixed');
    } else {
      angular.element('#session').addClass('video-session');
      angular.element('#session').removeClass('expand-video');
      angular.element('#bottomBar').removeClass('expand-video-controls');
      angular.element('ot-subscriber').removeClass('expand-video');
      angular.element('ot-subscriber').css('position', 'absolute');
    }
  }

  getMeetingCase(response, meetingObj, participants) {
    let meetingCase;
    switch (participants) {
    case 'client':
      if (response.sessionId && response.clientToken && response.conferenceState === 'ACCEPTED' && !meetingObj.isMeetingAccepted) {
        meetingCase = 'videoCallReqAccepted';
      } else if (response.conferenceState === 'CLIENT_HANG_UP' || response.conferenceState === 'ADVISOR_HANG_UP') {
        meetingCase = 'Logout';
      } else if (response.conferenceState === 'DISMISSED') {
        meetingCase = 'AdvisorDismissed';
      }
      break;
    case 'advisor':
      if (response.conferences.length && response.conferences[0].conferenceState === 'REQUESTED' && response.conferences[0].communicationType === 'VIDEO' && !meetingObj.isMeetingAccepted && meetingObj.pollCtr) {
        meetingCase = 'MaydayCallReqestRecv';
      } else if (response.conferences.length && response.conferences[0].conferenceState === 'REQUESTED' && response.conferences[0].communicationType === 'PHONE' && !meetingObj.isPhoneModalOpened) {
        meetingCase = 'PhoneCallRequestRecv';
      } else if (response.conferences.length && response.conferences[0].conferenceState === 'CANCELLED' && !meetingObj.isCancelModalOpened) {
        meetingCase = 'RequestCancled';
      } else if (response.conferences.length && (response.conferences[0].conferenceState === 'CLIENT_HANG_UP' || response.conferences[0].conferenceState === 'ADVISOR_HANG_UP')) {
        meetingCase = 'Logout';
      }
      break;
    default:
      break;
    }

    return meetingCase;
  }

  getUpcomingAppointment(meetingObj) {
    const params = { startDate: this.moment().valueOf() };
    this.appointmentService.retrieveClientCalendar(params).then((response) => {
      this.upcomingAppointment = false;
      if (response.appointments.length) {
        this.upcomingAppointment = response.appointments[0];
      }
    });
    if(this.upcomingAppointment && !meetingObj.isUpcomingAppointment && this.upcomingAppointment.id !== meetingObj.upcomingAppointmentId) {
      const appointmentStartTime = this.upcomingAppointment.startDate;
      let joinAppointmentTime = this.moment(appointmentStartTime).subtract(15, 'minutes').toDate().getTime();
      let currentTime = this.moment().toDate().getTime();

      if (currentTime === joinAppointmentTime) {
        meetingObj.isUpcomingAppointment = true;
        meetingObj.pollCtr = true;
        meetingObj.isPhoneModalOpened = false;
        meetingObj.isCancelModalOpened = false;
        meetingObj.appointmentObj = this.upcomingAppointment;
        meetingObj.upcomingAppointmentId = this.upcomingAppointment.id;
        angular.element('.js-conf-toast-container').fadeIn();
      }
    }
  }

  initAdvisorMeetingSync(meetingObj) {
    let uibModalInstance;
    this.$interval(() => {
      this.getUpcomingAppointment(meetingObj);
      this.getMeetingStatus('advisor').then((response) => {
        switch (this.getMeetingCase(response, meetingObj, 'advisor')) {
        case 'MaydayCallReqestRecv':
          if (uibModalInstance) {
            uibModalInstance.dismiss();
          }
          meetingObj.pollCtr = false;
          meetingObj.isUpcomingAppointment = false;
          meetingObj.isPhoneModalOpened = false;
          meetingObj.isCancelModalOpened = false;
          meetingObj.appointmentObj = false;
          meetingObj.advisorName = response.conferences[0].clientFirstName + ' ' + response.conferences[0].clientLastName;
          meetingObj.clientData = response.conferences[0];
          this.startStopCallTimer(true, meetingObj);
          angular.element('.js-conf-toast-container').fadeIn();
          break;
        case 'PhoneCallRequestRecv':
          meetingObj.isPhoneModalOpened = true;
          meetingObj.isCancelModalOpened = false;
          meetingObj.pollCtr = true;
          meetingObj.setMeetingCanceled(uibModalInstance);
          uibModalInstance = this.openModelPopup('app/advisor/dashboard/views/_phone-call-dialog.html', response.conferences[0], meetingObj);
          uibModalInstance.result.then(() => {
            this.$state.go('advisor.clientbook');
          }, () => {
            //TODO
          });
          break;
        case 'RequestCancled':
          this.setMeetingId('');
          meetingObj.setMeetingCanceled(uibModalInstance);
          meetingObj.isPhoneModalOpened = false;
          meetingObj.isCancelModalOpened = true;
          meetingObj.pollCtr = true;
          meetingObj.isSessionStart = false;
          meetingObj.waitingRoomEnabled = false;
          meetingObj.isMeetingAccepted = false;
          meetingObj.startSession(false, false);
          uibModalInstance = this.openModelPopup('app/advisor/dashboard/views/_conference-missed-dialog.html', response.conferences[0], meetingObj);
          break;
        case 'Logout':
          if (meetingObj.isSessionStart) {
            this.isWatchingPageChanged(false, meetingObj);
            meetingObj.videoSessionLogout();
          }
          this.startStopCallTimer(false, meetingObj);
          meetingObj.elapsed = '00:00:00';
          meetingObj.isSessionStart = false;
          meetingObj.isMeetingAccepted = false;
          this.setMeetingId('');
          break;
        default:
          break;
        }
      });
    }, 1000);
  }

  cancelClientPolling() {
    this.$interval.cancel(this.clientPollingObj);
  }

  initClientMeetingSync(meetingObj) {
    this.clientPollingObj = this.$interval(() => {
      this.getMeetingStatus('client').then((response) => {
        switch (this.getMeetingCase(response, meetingObj, 'client')) {
        case 'videoCallReqAccepted':
          let requestObject = {
            token: response.clientToken,
            sessionId: response.sessionId
          };

          this.OTService.setSessionToken(requestObject).then(() => {
            meetingObj.startSession(true, true);
            meetingObj.waitingRoomEnabled = true;
            meetingObj.isMeetingAccepted = true;
            meetingObj.isAdhocMeeting = false;
            this.startStopCallTimer(true, meetingObj);
            this.$timeout(() => {
              meetingObj.toggleVideoControls(false);
            }, 5000);
          });
          break;
        case 'AdvisorDismissed':
          this.setAdvisorId('');
          this.cancelClientPolling();
          meetingObj.isAdhocMeeting = false;
          meetingObj.isSessionStart = false;
          meetingObj.waitingRoomEnabled = false;
          meetingObj.isMeetingAccepted = false;
          this.isWatchingPageChanged(false, meetingObj);
          this.startStopCallTimer(false, this);
          this.openModelPopup('app/advisor/dashboard/views/_conference-missed-dialog.html', this.advisorObj, meetingObj);
          break;
        case 'Logout':
          if (meetingObj.isSessionStart && meetingObj.isMeetingAccepted) {
            meetingObj.videoSessionLogout();
          }
          this.isWatchingPageChanged(false, meetingObj);
          meetingObj.elapsed = '00:00:00';
          this.startStopCallTimer(false, meetingObj);
          meetingObj.isSessionStart = false;
          meetingObj.waitingRoomEnabled = false;
          meetingObj.isMeetingAccepted = false;
          this.setAdvisorId('');
          this.setMeetingId('');
          this.cancelClientPolling();
          break;
        case 'AdvisorMarkedMissed':
          // TODO
          break;
        default:
          break;
        }
      });
    }, 1000);
  }
}
