class DontLeavePageController {
  constructor($uibModalInstance) {
    'ngInject';
    this.$uibModalInstance = $uibModalInstance;
  }

  sayNo() {
    this.$uibModalInstance.dismiss();
  }

  sayYes() {
    this.$uibModalInstance.close();
  }
}

export class DontLeavePageService {
  constructor($state, $window, $uibModal, $timeout) {
    'ngInject';
    this.$state = $state;
    this.$window = $window;
    this.$uibModal = $uibModal;
    this.$timeout = $timeout;
  }

  startWatchingPage(ctrlScope, allowedSubPages) {
    this.isChangeState = false;
    this.eventHandler = ctrlScope.$on('$stateChangeStart', (event, toState) => {
      if (allowedSubPages && allowedSubPages.indexOf(toState.name) !== -1) {
        return;
      }

      if (this.isChangeState) {
        return;
      }

      let modalInstance = this.$uibModal.open({
        templateUrl: 'app/shared/views/_dont-leave-page.html',
        controller: DontLeavePageController,
        controllerAs: 'dlpCtrl',
        bindToController: true
      });

      modalInstance.result.then(() => {
        if(ctrlScope.isConferenceStart()){
          ctrlScope.videoSessionLogout();
        } else {
          ctrlScope.cancelAdHocCallSync();
        }
        this.$timeout(() => {
          this.isChangeState = true;
          this.$state.go(toState.name);
        }, 1000);
      }, () => {});

      event.preventDefault();
    });

    this.$window.onbeforeunload = function() {
      return 'Leaving this page will disconnect your video conference with your advisor.';
    };
  }

  stopWatchingPage() {
    this.eventHandler();
    this.eventHandler = null;
    this.$window.onbeforeunload = null;
  }
}
