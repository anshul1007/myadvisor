export class DateService {
  constructor(moment) {
    'ngInject';
    this.moment = moment;
  }

  /*
   * @params : appointmentDateInUTC UTC
   * @output : Today, May 19 at 10:00 a.m.
   *           Thursday, May 19 at 10:00 a.m.
   */
  getDay(appointmentDateInUTC) {
      let day = null;
      const appointmentFormattedDate = this.moment(this.moment(appointmentDateInUTC)).format('YYYY,MM,DD');
      const cuurentFormattedDate = this.moment().format('YYYY,MM,DD');
      let dateDiff = this.moment(new Date(appointmentFormattedDate)).diff(new Date(cuurentFormattedDate), 'days');
      if (!dateDiff) {
        day = 'Today';
      } else if (dateDiff === 1) {
        day = 'Tomorrow';
      } else if (dateDiff > 1) {
        day = this.moment(appointmentDateInUTC).format('dddd');
      }
      return day;
    }
    /*
     * @params : timeStart UTC
     *  s@output : 
     */
  getDate(timeStart, dateFormat) {
      if (angular.isDefined(timeStart)) {
        return this.moment(timeStart).format(dateFormat);
      }
    }
    /*
     * @params : timeStart UTC
     * @output : May 19
     */
    getCustomDateNoYear(appointmentDate){
      return this.moment(appointmentDate).format('dddd') + ' ' + this.moment(appointmentDate).format('LL');
    }
    /*
     * @params : timeStart UTC
     * @output : 
     */
  getTime(timeStart) {
    if (angular.isDefined(timeStart)) {
      return this.moment(timeStart).format('hh:mma');
    }
  }

}
