export default class MutePublisherAudio {
  constructor(OTSession) {
    this.restrict = 'A';
    this.OTSession = OTSession;
  }
  link(scope, element, attrs) {
    var publisher;
    scope.muted = false;
    angular.element(element).on('click', () => {
      if (!publisher) {
        publisher = this.OTSession.publishers.filter(function (el) {
          return el.id === attrs.publisherId;
        })[0];
      }
      if (publisher) {
        publisher.publishAudio(scope.muted);
        scope.muted = !scope.muted;
        scope.$apply();
      }
    });
    scope.$on('$destroy', function () {
      publisher = null;
    });
  }
}
