export default class MutePublisher {
  constructor(OTSession) {
    this.restrict = 'A';
    this.OTSession = OTSession;
  }
  link(scope, element, attrs) {
    var publisher;
    scope.vedioMuted = false;
    angular.element(element).on('click', () => {
      if (!publisher) {
        publisher = this.OTSession.publishers.filter(function (el) {
          return el.id === attrs.publisherId;
        })[0];
      }
      if (publisher) {
        publisher.publishVideo(scope.vedioMuted);
        scope.vedioMuted = !scope.vedioMuted;
        scope.$apply();
      }
    });
    scope.$on('$destroy', function () {
      publisher = null;
    });
  }
}
