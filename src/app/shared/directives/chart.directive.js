export default class ChartDirective {
  constructor() {
    this.restrict = 'E';
    this.template = '<div></div>';
    this.scope = {
      options: '=',
      isexpand: '='
    };
  }
  link(scope, element) {
    let chart = Highcharts.chart(element[0], scope.options);
    let colName = scope.options.colName;

    scope.$watch('isexpand', function(newSeries, oldSeries) {
      if (newSeries !== oldSeries) {
        if (newSeries) {
          angular.element('.' + colName + '-left .accordion-table').slideToggle('slow');
          angular.element('.' + colName + '-left').animate({ width: 'toggle' }, 'slow', function() {
            angular.element('.' + colName).removeClass('col-md-8').addClass('col-md-12');
            chart.setSize(angular.element('.' + colName).width(), chart.chartHeight, true);
          });
        } else {
          angular.element('.' + colName).removeClass('col-md-12').addClass('col-md-8');
          angular.element('.' + colName + '-left').animate({ width: 'toggle' }, 'slow');
          chart.setSize(angular.element('.' + colName).width(), chart.chartHeight, true);
          angular.element('.' + colName + '-left .accordion-table').slideToggle('slow');
        }
      }
    }, true);

    scope.$watch('options', function(newSeries, oldSeries) {
      if (newSeries !== oldSeries) {
        chart = Highcharts.chart(element[0], scope.options);
      }
    }, true);
  }
}
