export default class MuteSubscriber {
  constructor(OTSession) {
    this.restrict = 'A';
    this.OTSession = OTSession;
  }
  link(scope, element) {
    var subscriber;
    scope.muted = false;
    angular.element(element).on('click', () => {
      if (!subscriber) {
        subscriber = this.OTSession.session.getSubscribersForStream(scope.stream)[0];
      }
      if (subscriber) {
        subscriber.subscribeToVideo(scope.muted);
        scope.muted = !scope.muted;
        scope.$apply();
      }
    });
    scope.$on('$destroy', function () {
      subscriber = null;
    });
  }
}
