/*
 * This is a reusable genric Directive for Modal Version 01
 *    @Todo:

 *    @usage :
 *      <modal-directive name="Text or Button" template-dir="Modal template directory (default is app/shared/views)" modal-template="Modal template name" custom-css="Modal CSS class"></modal-directive>
 *
 *    @methods:
 *      1) open
 *      2) ok
 *      3) cancel
 */
// Directive's controller
class DirectiveModalInstanceCtrl {
  constructor($uibModal, $state) {
    'ngInject';
    this.modal = $uibModal;
    this.$state = $state;
  }
}
//Modal template controller
class ModalInstanceCtrl {
  constructor($uibModalInstance, $timeout, autoClose) {
    'ngInject';
    this.uibModalInstance = $uibModalInstance;
    this.timeout = $timeout;
    if (autoClose) {
      this.timeout(() => {
        this.cancel();
      }, 3000);
    }
  }
  ok() {
    this.uibModalInstance.close();
  }
  cancel() {
    this.uibModalInstance.dismiss();
  }
}
export default class ModalDirective {
  constructor() {
    this.template = '<a href="#" class="{{btnType}}" ng-click="open()" ng-transclude>{{name}}</a>';
    this.restrict = 'EA';
    this.transclude = true;
    this.replace = true;
    this.scope = {
      'onClose': '&'
    };
    this.controller = DirectiveModalInstanceCtrl;
    this.controllerAs = 'ctrl';
    this.bindToController = true;
  }
  link(scope, element, attrs, ctrl) {
    let sharedViews = 'app/shared/views/';
    scope.autoClose = attrs.autoClose;
    scope.scrollOffset = attrs.scrollOffset;
    scope.scrollDuration = scope.scrollDuration;
    scope.btnType = attrs.btnType;
    scope.name = attrs.name;
    scope.stateful = attrs.stateful;
    scope.customCss = attrs.customCss;
    scope.modalTemplate = attrs.modalTemplate;
    scope.templateDir = attrs.templateDir;
    attrs.templateDir = (angular.isUndefined(scope.templateDir)) ? sharedViews : attrs.templateDir;
    scope.open = function() {
      if (attrs.stateful) {
        ctrl.modalInstance = ctrl.modal.open({
          template: '<div ui-view="modal"></div>',
          controller: ModalInstanceCtrl,
          controllerAs: 'mCtrl',
          bindToController: true,
          windowTopClass: attrs.customCss,
          size: 'lg',
          resolve: {
            modalState: function() {
              if (attrs.stateful) {
                ctrl.$state.includes('add-goal');
                ctrl.currentState = ctrl.$state.current.name;
                //ctrl.$state.go('modal.goaltypes');
                ctrl.$state.transitionTo('add-goal.goal-types');
              }
            },
            autoClose: function() {
              return attrs.autoClose;
            }
          }
        });
      } else {
        ctrl.modalInstance = ctrl.modal.open({
          templateUrl: attrs.templateDir + attrs.modalTemplate + '.html',
          controller: ModalInstanceCtrl,
          controllerAs: 'mCtrl',
          bindToController: true,
          windowTopClass: attrs.customCss,
          resolve: {
            autoClose: function() {
              return attrs.autoClose;
            }
          }
        });
      }

      ctrl.modalInstance.result.then(function() {
        //'Modal success' on open
      }, function() {
        const offset = (angular.isDefined(scope.scrollOffset)) ? scope.scrollDuration : 0;
        const duration = (angular.isDefined(scope.scrollDuration)) ? scope.duration : 800;
        angular.element('body').animate({ scrollTop: offset }, duration);
        if (attrs.stateful) { ctrl.$state.go(ctrl.currentState); }
        ctrl.onClose();
      });
    };

  }
}
