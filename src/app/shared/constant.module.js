import * as stringConstants from './string.constants';

angular.module('constants', [])
  .constant('stringConstants', stringConstants.default);
