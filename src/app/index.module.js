import { HelperService } from './shared/services/helper.service';
import { DateService } from './shared/services/date.service';
import { Session } from './shared/services/session.service';
import { OTService } from './shared/services/opentok.service';
import { MayDayService } from './shared/services/mayday.service';
import { DontLeavePageService } from './shared/services/dontleavepage.service';
import './shared/constant.module';
import './env.config';
import { config } from './index.config';
import { runConfig } from './index.run';
import ModalDirective from './shared/directives/modal.directive';
import ChartDirective from './shared/directives/chart.directive';
import MuteSubscriber from './shared/directives/mayday-mute-subscriber.directive';
import MutePublisher from './shared/directives/mayday-mute-publisher.directive';
import MuteSubscriberAudio from './shared/directives/mayday-mute-subscriber-audio.directive';
import MutePublisherAudio from './shared/directives/mayday-mute-publisher-audio.directive';
import PhoneFormatFilter from './shared/filters/formatphone.filter';
import SentanceCaseFilter from './shared/filters/sentancecase.filter';
import { UserService } from './shared/services/user.service';

angular.module('MainApp', ['myClient', 'myAdvisor', 'ui.bootstrap-slider', 'restangular', 'ngStorage', 'xeditable', 'angularSpinner', 'ngSanitize', 'constants', 'underscore', 'ngTouch', 'ui.router', 'ngAnimate', 'ui.bootstrap', 'ui.calendar', 'toastr', 'ui.utils.masks', 'com.2fdevs.videogular', 'com.2fdevs.videogular.plugins.controls', 'com.2fdevs.videogular.plugins.overlayplay', 'com.2fdevs.videogular.plugins.poster', 'env.config', 'ng.deviceDetector', 'angulartics', 'angulartics.google.analytics', 'opentok','ngIdle'])
  .service('helpers', HelperService)
  .service('dateService', DateService)
  .service('Session', Session)
  .service('OTService', OTService)
  .service('MayDayService', MayDayService)
  .service('DontLeavePageService', DontLeavePageService)
  .directive('modalDirective', () => new ModalDirective())
  .directive('chartDirective', () => new ChartDirective())
  .config(config)
  .run(runConfig)
  .directive('muteSubscriber', (OTSession) => new MuteSubscriber(OTSession))
  .directive('mutePublisher', (OTSession) => new MutePublisher(OTSession))
  .directive('muteSubscriberAudio', (OTSession) => new MuteSubscriberAudio(OTSession))
  .directive('mutePublisherAudio', (OTSession) => new MutePublisherAudio(OTSession))
  .filter('phone', () => new PhoneFormatFilter())
  .filter('sentanceCase', () => new SentanceCaseFilter())
  .service('UserService', UserService);

